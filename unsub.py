# Snow Day API - Unsubscribe Script
# (c) 2019-2020 o355
# This code is licensed under the MIT License. Please see LICENSE for more information.

# This script pragmatically unsubscribes everyone from the SMS Service. Use this for when the season might be ending,
# for instance.

# This code relies on the config file. Make sure that config.ini is present in the relative directory where
# unsub.py is being stored in.

import boto3
import requests
import configparser
import time

# Configures if a dry run should be completed. If dry run is on, then the script will fully run (good for testing), but
# not actually unsubscribe anyone.
dryrun = False

# Configures the API key used to unsubscribe numbers.
apikey = ""

# Configures if the special message query value will be set to True or not.
specialmsg_val = False

# Configures the URL for the path to /api/v2/unsubscribeNumber. Include the full hostname of your server when doing this
# You'll likely want to temporarily increase the rate limit for /api/v2/unsubscribeNumber, as it is pretty low by
# default and you will run into rate limiting issues if you don't change it. Make sure this includes https:// at the
# front of the URL.
unsubscribenumber_path = ""

config = configparser.ConfigParser()
config.read("config.ini")

access_key_aws = config.get('KEYS', 'aws_access')
secret_key_aws = config.get('KEYS', 'aws_secret')
region_aws = config.get('KEYS', 'aws_region')

session = boto3.Session(aws_access_key_id=access_key_aws, aws_secret_access_key=secret_key_aws, region_name=region_aws)
db = session.resource('dynamodb')
table = db.Table('Numbers_Prod')

response = table.scan()

responselength = len(response['Items'])
counter = 0
if dryrun is True:
    print("- - - - - - - - - - - - - - - -")
    print("DRY RUN ENABLED. NO REQUESTS TO THE API WILL BE MADE.")
    time.sleep(2)
print("- - - - - - - - - - - - - - - -")
for i in response['Items']:
    counter = counter + 1
    print("Unsubscribing number %s/%s - %s" % (counter, responselength, str(i['number'])))
    payload = {"key": "f507d58ae478359acad81e7f470cb1f1dc5d69016650f0fa6d",
               "number": str(i['number']),
               "specialMsg": True}
    print("Sending request...")
    if dryrun is False:
        requestresponse = requests.post("https://snowdayapi.owenthe.dev/api/v2/unsubscribeNumber", data=payload)
        try:
            jsonreturn = requestresponse.json()
            if str(jsonreturn['code']) == "0":
                print("Request successful. Error Code: 0. Message was: %s" % str(jsonreturn['message']))
                print("Continuing onto next number.")
                print("- - - - - - - - - - - - - - - -")
            else:
                print("Request unsuccessful. Error Code: %s. Message was: %s" % (str(jsonreturn['code']),
                                                                                 str(jsonreturn['message'])))
                print("Continuing onto next number.")
                print("- - - - - - - - - - - - - - - -")
        except ValueError:
            print("Request unsuccessful. No JSON was return, response was: %s" % requestresponse.text)
            print("Continuing onto next number.")
            print("- - - - - - - - - - - - - - - -")
    elif dryrun is True:
        time.sleep(1.5)
        print("Request 'successful'. Error Code: N/A. Message was: N/A")
        print("Continuing onto next number.")
        print("- - - - - - - - - - - - - - - -")

print("No more numbers to unsubscribe. Script complete.")
