# Snow Day SMS API Service - Version 2.2.1
# Copyright (C) 2019-2020

from flask import Flask, request, jsonify, render_template, Response
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from twilio.rest import Client
import boto3
import re
import twilio
from twilio.twiml.messaging_response import MessagingResponse
from twilio.twiml.voice_response import VoiceResponse, Say
from botocore.exceptions import ClientError
import configparser
import uuid
import time
import random
from ua_parser import user_agent_parser

config = configparser.ConfigParser()
config.read("config.ini")

access_key_aws = config.get('KEYS', 'aws_access')
secret_key_aws = config.get('KEYS', 'aws_secret')
aws_region = config.get('KEYS', 'aws_region')

twilio_account_sid = config.get('KEYS', 'twilio_sid')
twilio_account_token = config.get('KEYS', 'twilio_token')

# CORS is now always on, however, CORS protection has been improved.
cors_url_frontend = config.get('CORS', 'cors_frontend')
cors_url_dashend = config.get('CORS', 'cors_dashend')
cors_url_maintenance = config.get('CORS', 'cors_maintenance')
cors_url = config.get('CORS', 'cors')

# Manual API key define
api_keys = config.get('APIKEYS', 'keys').split(", ")

api_keys_registernumber = config.get('APIKEYS', 'keys_registernumber').split(", ")

api_keys_predictioninfo = config.get('APIKEYS', 'keys_predictioninfo').split(", ")

api_keys_numbercapacity = config.get('APIKEYS', 'keys_numbercapacity').split(", ")

api_keys_requestnewsession = config.get('APIKEYS', 'keys_requestnewsession').split(", ")

api_keys_bannumber = config.get('APIKEYS', 'keys_bannumber').split(", ")

api_keys_unbannumber = config.get('APIKEYS', 'keys_unbannumber').split(", ")

# API version define
apiversion = config.get('VERSIONING', 'apiver')
apiversion_nov = config.get('VERSIONING', 'apiver_nov')

api_homepage_statusurl = config.get('HOMEPAGE', 'statuspage_url')
api_homepage_docsurl = config.get('HOMEPAGE', 'docs_url')
api_homepage_sourceurl = config.get('HOMEPAGE', 'source_url')

# Define special messages for unsubscribe or register number.
api_registernumber_specialmsg_text = config.get('SPECIALMSG', 'registernumber_specialtext')

api_unsubscribenumber_specialmsg_text = config.get('SPECIALMSG', 'unsubscribenumber_specialtext')

# Yes, this is a full list from Wikipedia of valid US area codes with potential overlays included.
# Yes, I really do waste this much time.
valid_codes = [201, 202, 203, 205, 206, 207, 208, 209, 210, 212, 213, 214, 215, 216, 217, 218, 219, 220, 223, 224,
               225, 227, 228, 229, 231, 234, 239, 240, 248, 251, 252, 253, 254, 256, 260, 262, 267, 269, 270, 272,
               274, 276, 279, 281, 283, 301, 302, 303, 304, 305, 307, 308, 309, 310, 312, 313, 314, 315, 316, 317,
               318, 319, 320, 321, 323, 325, 326, 327, 330, 331, 332, 334, 336, 337, 339, 341, 346, 347, 351, 352,
               360, 361, 364, 380, 385, 386, 401, 402, 404, 405, 406, 407, 408, 409, 410, 412, 413, 414, 415, 417,
               419, 423, 424, 425, 430, 432, 434, 435, 440, 442, 443, 445, 447, 458, 463, 464, 469, 470, 475, 478,
               479, 480, 484, 501, 502, 503, 504, 505, 507, 508, 509, 510, 512, 513, 515, 516, 517, 518, 520, 530,
               531, 534, 539, 540, 541, 551, 559, 561, 562, 563, 564, 567, 570, 571, 573, 574, 575, 579, 580, 585,
               586, 601, 602, 603, 605, 606, 607, 608, 609, 610, 612, 614, 615, 616, 617, 618, 619, 620, 623, 626,
               628, 629, 630, 631, 636, 640, 641, 646, 650, 651, 657, 659, 660, 661, 662, 667, 669, 678, 679, 680,
               681, 682, 689, 701, 702, 703, 704, 706, 707, 708, 712, 713, 714, 715, 716, 717, 718, 719, 720, 724,
               725, 726, 727, 730, 731, 732, 734, 737, 740, 743, 747, 754, 757, 760, 762, 763, 765, 769, 770, 772,
               773, 774, 775, 779, 781, 785, 786, 801, 802, 803, 804, 805, 806, 808, 810, 812, 813, 814, 815, 816,
               817, 818, 820, 828, 830, 831, 832, 838, 839, 840, 843, 845, 847, 848, 850, 854, 856, 857, 858, 859,
               860, 862, 863, 864, 865, 870, 872, 878, 901, 903, 904, 905, 906, 907, 908, 909, 910, 912, 913, 914,
               915, 916, 917, 918, 919, 920, 925, 927, 928, 929, 930, 931, 934, 936, 937, 938, 940, 941, 947, 949,
               951, 952, 954, 956, 959, 970, 971, 972, 973, 978, 979, 980, 984, 985, 986, 989]

# API closed has been updated so that it only covers maintenance.
apiclosed = config.getboolean('MAIN', 'closed')

# Configure options to limit API registrations if a specified number is greater than what is expected here.
# Turned off by default.
api_limitregistrations = config.getboolean('MAIN', 'limitregistrations')
api_limitregistrations_number = config.getint('MAIN', 'limitregistrations_number')

# Configure groups
api_groups = config.get('MAIN', 'groups').split(", ")
api_defaultgroup = config.get('MAIN', 'defaultgroup')

# Configure numbers
api_numbers = config.get('MAIN', 'numbers').split(", ")
api_numbers_count = len(api_numbers)

# Configure SMS related stuff
api_sms_maxprice = config.get('SMS', 'maxprice')
api_sms_outgoingcallback = config.get('SMS', 'outgoingsms_callback')
api_sms_registercallback = config.get('SMS', 'registernumber_callback')

# Configure hold limit
api_holdlimit = config.getboolean('MAIN', 'holdlimit')
api_holdlimit_count = config.getint('MAIN', 'holdlimit_days')

# Load all the rate limits in from the config file
api_ratelimits_enabled = config.getboolean('RATELIMITS', 'enable_ratelimits')
api_limit_global = config.get('RATELIMITS', 'limit_global')
api_limit_registernumber = config.get('RATELIMITS', 'limit_registernumber')
api_limit_inboundvoice = config.get('RATELIMITS', 'limit_inboundvoice')
api_limit_inboundsms = config.get('RATELIMITS', 'limit_inboundsms')
api_limit_updateusergroup = config.get('RATELIMITS', 'limit_updateusergroup')
api_limit_newmessage = config.get('RATELIMITS', 'limit_newmessage')
api_limit_outgoingsmsstatus = config.get('RATELIMITS', 'limit_outgoingsmsstatus')
api_limit_disablemessagedeliveryconfirmation = config.get('RATELIMITS', 'limit_disablemessagedeliveryconfirmation')
api_limit_confirmmessageerrorcode = config.get('RATELIMITS', 'limit_confirmmessageerrorcode')
api_limit_subscribenumber = config.get('RATELIMITS', 'limit_subscribenumber')
api_limit_unsubscribenumber = config.get('RATELIMITS', 'limit_unsubscribenumber')
api_limit_terminatesession = config.get('RATELIMITS', 'limit_terminatesession')
api_limit_updateusersettings = config.get('RATELIMITS', 'limit_updateusersettings')
api_limit_getsessionstats = config.get('RATELIMITS', 'limit_getsessionstats')
api_limit_terminateallsessions = config.get('RATELIMITS', 'limit_terminateallsessions')
api_limit_predictioninfo = config.get('RATELIMITS', 'limit_predictioninfo')
api_limit_messageerrors = config.get('RATELIMITS', 'limit_messageerrors')
api_limit_getusersettings = config.get('RATELIMITS', 'limit_getusersettings')
api_limit_verifypreauthsessionstatus = config.get('RATELIMITS', 'limit_verifypreauthsessionstatus')
api_limit_verifysessionstatus = config.get('RATELIMITS', 'limit_verifysessionstatus')
api_limit_verifynewsession = config.get('RATELIMITS', 'limit_verifynewsession')
api_limit_requestnewauthcode = config.get('RATELIMITS', 'limit_requestnewauthcode')
api_limit_requestnewsession = config.get('RATELIMITS', 'limit_requestnewsession')
api_limit_numbercapacity = config.get('RATELIMITS', 'limit_numbercapacity')
api_limit_bannumber = config.get('RATELIMITS', 'limit_bannumber')
api_limit_unbannumber = config.get('RATELIMITS', 'limit_unbannumber')
api_limit_ping = config.get('RATELIMITS', 'limit_ping')
api_limit_docs = config.get('RATELIMITS', 'limit_docs')
api_limit_index = config.get('RATELIMITS', 'limit_index')

api_limiting_db = config.get('RLDB', 'db_url')


client = Client(twilio_account_sid, twilio_account_token)
session = boto3.Session(aws_access_key_id=access_key_aws, aws_secret_access_key=secret_key_aws, region_name=aws_region)
db = session.resource('dynamodb')
table = db.Table('Numbers_Prod')
table2 = db.Table('PendingNumbers_Prod')
table3 = db.Table('ResponseNumbers_Prod')
table4 = db.Table('ErrorNumbers_Prod')
table5 = db.Table('SIDStatus_Prod')
table6 = db.Table('Sessions_Prod')
table7 = db.Table('SessionsActual_Prod')
table8 = db.Table('PredictionInfo')
table9 = db.Table('NumCapacity_Prod')
table10 = db.Table('BannedNumbers_Prod')


def apiclosed_check():
    if apiclosed is True:
        response = {"apiversion": apiversion, "code": 59, "data": {},
                    "message": "The API is temporarily offline for maintenance at this time. Please try again later."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 59)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add('Access-Control-Allow-Origin', cors_url_maintenance)
        return response


app = Flask(__name__)
limiter = Limiter(app, key_func=get_remote_address,
                  default_limits=api_limit_global.split(";"), headers_enabled=True, storage_uri="redis://localhost:6379",
                  storage_options={})
limiter.init_app(app)

shared_limiter = limiter.shared_limit(api_limit_global, scope="/")


@app.errorhandler(429)
def ratelimit_handle(e):
    response = {"apiversion": apiversion, "code": 68, "data": {"ratelimit": e.description},
                "message": "You are making too many requests. Please wait some time before making another request."}
    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 68)
    response.headers.add("Access-Control-Allow-Credentials", "true")
    response.headers.add("Access-Control-Allow-Origin", cors_url_maintenance)
    return response, 429


@app.route("/api/v2/registerNumber", methods=['POST'])
@limiter.limit(api_limit_registernumber)
@shared_limiter
def registernumber():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    key = request.values.get('key', "None")

    if (key not in api_keys) and (key not in api_keys_registernumber):
        response = {"apiversion": apiversion, "code": 54, "data": {},
                    "message": "The API key defined is invalid. Please use a valid API key to use this API."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
        return response, 403

    data = request.values.get('number', None)
    group = request.values.get('group', None)
    outboundNum = request.values.get('outboundNum', None)
    specialmessage = request.values.get('specialMsg', None)

    if group is not None:
        if group not in api_groups:
            response = {"apiversion": apiversion, "code": 90, "data": {},
                        "message": "The group defined in the request is not a valid group."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 90)
            response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
            return response, 422
    else:
        group = api_defaultgroup

    if data == "" or data is None:
        response = {"apiversion": apiversion, "code": 39, "data": {},
                    "message": "The number defined has a length of 0 characters. Please enter a valid number."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 39)
        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
        return response, 422

    numcapacity_response = table9.scan()

    if outboundNum is not None:
        if outboundNum not in api_numbers:
            response = {"apiversion": apiversion, "code": 92, "data": {},
                        "message": "The outbound number defined in the request is not a valid number."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 92)
            response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
            return response, 422

        if api_limitregistrations is True:
            for i in numcapacity_response['Items']:
                if str(i['number']) == outboundNum:
                    if int(i['capacity']) >= api_limitregistrations_number / api_numbers_count:
                        response = {"apiversion": apiversion, "code": 93, "data": {},
                                    "message": "The number defined in the request has reached capacity. Please "
                                               "choose a different number."}
                        response = jsonify(response)
                        response.headers.add("X-SnowDayAPI-Version", apiversion)
                        response.headers.add("X-SnowDayAPI-Code", 93)
                        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
                        return response, 422
    else:
        lowestcap_count = 99999999
        lowestcap_number = ""
        for i in numcapacity_response['Items']:
            if int(i['capacity']) < lowestcap_count:
                lowestcap_count = int(i['capacity'])
                lowestcap_number = str(i['number'])

        outboundNum = lowestcap_number

    data = str(data)

    data = data.replace("-", "").replace("(", "").replace(")", "").replace(" ", "").replace("+", "").replace('\n', "")
    data = data.replace('\r', "").replace('\t', "").replace("  ", "")

    if data[0] == "1":
        data = data[1:]

    data = "+1" + data

    if len(data) != 12:
        response = {"apiversion": apiversion, "code": 5, "data": {},
                    "message": "The number defined is not of proper length. Make sure an area code is included."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 5)
        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
        return response, 422

    tempdata = data[:5]
    tempdata = tempdata[2:]

    try:
        tempdata = int(tempdata)
    except ValueError:
        response = {"apiversion": apiversion, "code": 58, "data": {},
                    "message": "The number defined is invalid. Make sure that the phone number defined has "
                               "actual numbers, other than (, ), -, a space, and +."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 58)
        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
        return response, 422

    if tempdata not in valid_codes:
        response = {"apiversion": apiversion, "code": 62, "data": {},
                    "message": "The number defined is invalid as it does not have an area code based in the "
                               "mainland US, Alaska, or Hawaii."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 62)
        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
        return response, 422

    try:
        response = table10.query(
            KeyConditionExpression="#num = :num",
            ExpressionAttributeValues={
                ":num": data
            },
            ExpressionAttributeNames={
                "#num": "number"
            }
        )
    except ClientError:
        response = {"apiversion": apiversion, "code": 37, "data": {},
                    "message": "A database error has prevented confirmation of the number defined."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 37)
        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
        return response, 503

    if len(response['Items']) == 1:
        reason = response['Items'][0]['reason']
        response = {"apiversion": apiversion, "code": 60, "data": {"reason": reason},
                    "message": "The number defined is banned from the SMS service."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 60)
        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
        return response, 401

    try:
        response = table.scan()
    except ClientError:
        response = {"apiversion": apiversion, "code": 37, "data": {},
                    "message": "A database error has prevented confirmation of the number defined."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 37)
        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
        return response, 503

    for i in response['Items']:
        if str(i['number']) == str(data):
            response = {"apiversion": apiversion, "code": 4, "data": {},
                        "message": "The number defined is already subscribed to the service."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 4)
            response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
            return response, 400

    if api_limitregistrations is True:
        if len(response['Items']) > api_limitregistrations_number:
            response = {"apiversion": apiversion, "code": 33, "data": {},
                        "message": "The maximum number of numbers registered has been reached, the number defined "
                                   "could not be registered. Please try again when some users have unsubscribed."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 33)
            response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
            return response, 503

    if len(data) != 12:
        response = {"apiversion": apiversion, "code": 3, "data": {},
                    "message": "The number defined has an invalid length. Please try again later."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 3)
        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
        return response, 422

    try:
        response = table2.scan()
    except ClientError:
        response = {"apiversion": apiversion, "code": 37, "data": {},
                    "message": "A database error has prevented confirmation of the number defined."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 37)
        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
        return response, 503

    for i in response['Items']:
        if str(i['number']) == str(data):
            response = {"apiversion": apiversion, "code": 8, "data": {},
                        "message": "The number defined is pending verification."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 8)
            response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
            return response, 400

    # Send message
    try:
        if str(specialmessage).lower() == "true":
            message = client.messages.create(
                body=api_registernumber_specialmsg_text,
                from_=outboundNum,
                status_callback=api_sms_registercallback,
                max_price=api_sms_maxprice,
                to=data
            )
        else:
            message = client.messages.create(
                body="(Snow Day Predictor): Thanks for signing up for the Snow Day SMS Service. To "
                     "confirm registration, reply with Y. If not, reply with N.",
                from_=outboundNum,
                status_callback=api_sms_registercallback,
                max_price=api_sms_maxprice,
                to=data
            )

    except twilio.base.exceptions.TwilioRestException:
        response = {"apiversion": apiversion, "code": 6, "data": {},
                    "message": "The number defined is invalid. This may be because the service is blocked on the "
                               "number defined."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 6)
        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
        return response, 400

    # Wait a second for message delivery
    sid = message.sid

    # Although it may not make sense since deleting SID entries really aren't useful, it can forewarn about database
    # issues.
    try:
        response = table5.scan()
    except ClientError:
        response = {"apiversion": apiversion, "code": 37, "data": {},
                    "message": "A database error has prevented confirmation of the number defined."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 37)
        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
        return response, 503

    # Potentially delete any duplicate SID entries
    for i in response['Items']:
        if str(i['sid']) == str(sid):
            table5.delete_item(
                Key={
                    'sid': sid
                }
            )

    # Put in the initial SID code
    try:
        table5.put_item(
            Item={
                'sid': sid
            }
        )
    except ClientError:
        response = {"apiversion": apiversion, "code": 37, "data": {},
                    "message": "A database error has prevented confirmation of the number defined."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 37)
        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
        return response, 503

    for i in range(1, 61):
        try:
            response = table5.scan()
        except ClientError:
            response = {"apiversion": apiversion, "code": 37, "data": {},
                        "message": "A database error has prevented confirmation of the number defined."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 37)
            response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
            return response, 503

        for i in response['Items']:
            if str(i['sid']) == str(sid):
                try:
                    code = i['code']
                except:
                    # If we can't find a code, sleep for 1 second and keep looping
                    time.sleep(1)
                    continue

                # If we did find a code, do some checking to see if we have a code 0 or an error code.

                code = str(code)

                # For all status codes, if we can't delete the SID code then just skip over - it's fine

                if code == "30003":
                    try:
                        table5.delete_item(
                            Key={
                                'sid': sid
                            }
                        )
                    except Exception:
                        pass

                    response = {"apiversion": apiversion, "code": 13, "data": {},
                                "message": "The number defined is not reachable, make sure the number defined "
                                           "is reachable."}
                    response = jsonify(response)
                    response.headers.add("X-SnowDayAPI-Version", apiversion)
                    response.headers.add("X-SnowDayAPI-Code", 13)
                    response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
                    return response, 400
                elif code == "30004":
                    try:
                        table5.delete_item(
                            Key={
                                'sid': sid
                            }
                        )
                    except Exception:
                        pass
                    response = {"apiversion": apiversion, "code": 14, "data": {},
                                "message": "The number defined blocked the confirmation message. Make sure the "
                                           "service is unblocked on the number defined."}
                    response = jsonify(response)
                    response.headers.add("X-SnowDayAPI-Version", apiversion)
                    response.headers.add("X-SnowDayAPI-Code", 14)
                    response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
                    return response, 400
                elif code == "30005":
                    try:
                        table5.delete_item(
                            Key={
                                'sid': sid
                            }
                        )
                    except Exception:
                        pass
                    response = {"apiversion": apiversion, "code": 15, "data": {},
                                "message": "The number defined returned an unknown destination error. "
                                           "Please try again later."}
                    response = jsonify(response)
                    response.headers.add("X-SnowDayAPI-Version", apiversion)
                    response.headers.add("X-SnowDayAPI-Code", 15)
                    response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
                    return response, 400
                elif code == "30006":
                    try:
                        table5.delete_item(
                            Key={
                                'sid': sid
                            }
                        )
                    except Exception:
                        pass
                    response = {"apiversion": apiversion, "code": 16, "data": {},
                                "message": "The number defined is a landline number. Please try again and define a "
                                           "mobile number instead."}
                    response = jsonify(response)
                    response.headers.add("X-SnowDayAPI-Version", apiversion)
                    response.headers.add("X-SnowDayAPI-Code", 16)
                    response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
                    return response, 400
                elif code == "30007":
                    try:
                        table5.delete_item(
                            Key={
                                'sid': sid
                            }
                        )
                    except Exception:
                        pass
                    response = {"apiversion": apiversion, "code": 17, "data": {},
                                "message": "The number defined cannot receive the confirmation text due to "
                                           "carrier spam filtering. Please try again later."}
                    response = jsonify(response)
                    response.headers.add("X-SnowDayAPI-Version", apiversion)
                    response.headers.add("X-SnowDayAPI-Code", 17)
                    response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
                    return response, 400
                elif code == "30008":
                    try:
                        table5.delete_item(
                            Key={
                                'sid': sid
                            }
                        )
                    except Exception:
                        pass
                    response = {"apiversion": apiversion, "code": 18, "data": {},
                                "message": "The number defined returned a generic error. Please try again later."}
                    response = jsonify(response)
                    response.headers.add("X-SnowDayAPI-Version", apiversion)
                    response.headers.add("X-SnowDayAPI-Code", 18)
                    response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
                    return response, 400
                elif code == "30010":
                    try:
                        table5.delete_item(
                            Key={
                                'sid': sid
                            }
                        )
                    except Exception:
                        pass

                    response = {"apiversion": apiversion, "code": 20, "data": {},
                                "message": "The number defined is not compatible with the Snow Day SMS Service due to "
                                           "carrier fees."}
                    response = jsonify(response)
                    response.headers.add("X-SnowDayAPI-Version", apiversion)
                    response.headers.add("X-SnowDayAPI-Code", 20)
                    response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
                    return response, 400
                elif code != "0":
                    try:
                        table5.delete_item(
                            Key={
                                'sid': sid
                            }
                        )
                    except Exception:
                        pass
                    response = {"apiversion": apiversion, "code": 19, "data": {},
                                "message": "An unknown error occurred. Please try again later."}
                    response = jsonify(response)
                    response.headers.add("X-SnowDayAPI-Version", apiversion)
                    response.headers.add("X-SnowDayAPI-Code", 19)
                    response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
                    return response, 500

                try:
                    table5.delete_item(
                        Key={
                            'sid': sid
                        }
                    )
                except Exception:
                    pass

                # Even though we pass over exceptions with entering SID data, for putting items into pending numbers
                # we will always return code 37

                try:
                    unixtime = int(time.time())
                    table2.put_item(
                        Item={
                            'number': data,
                            'group': group,
                            'outboundNum': outboundNum,
                            'created': int(unixtime)
                        }
                    )
                except ClientError:
                    response = {"apiversion": apiversion, "code": 37, "data": {},
                                "message": "A database error has prevented confirmation of the number defined."}
                    response = jsonify(response)
                    response.headers.add("X-SnowDayAPI-Version", apiversion)
                    response.headers.add("X-SnowDayAPI-Code", 37)
                    response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
                    return response, 503

                response = {"apiversion": apiversion, "code": 0, "data": {},
                            "message": "The action completed successfully."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 0)
                response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
                return response
            else:
                continue
        time.sleep(1)
        continue

    # Code for when we get a timed out request
    response = {"apiversion": apiversion, "code": 22, "data": {},
                "message": "A timeout error occurred when validating the number defined. Please try again later."}
    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 22)
    response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
    return response, 408


@app.route("/api/v2/inboundVoice", methods=['POST'])
@limiter.limit(api_limit_inboundvoice)
@shared_limiter
def inboundvoice():
    resp = VoiceResponse()
    say = Say(voice='Polly.Joanna')

    if apiclosed is True:
        # Another special response for when the API is under maintenance.
        say.append('The Snow Day Predictor is not taking calls at this time due to backend system maintenance. Please '
                   'visit status dot owen the dot dev for more information.')
        resp.append(say)
        resp.hangup()
        response = Response(str(resp))
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 59)
        response.headers.add("Access-Control-Allow-Origin", cors_url)
        return response

    key = request.values.get('key', "None")
    if key not in api_keys:
        say.append('Error Code 54 - Please contact Snow Day Support.')
        resp.append(say)
        resp.hangup()
        response = Response(str(resp))
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response

    response = table8.scan()
    for i in response['Items']:
        lastupdated_line = str(i['lu-info'])
        line1 = str(i['pt1-info'])
        line2 = str(i['pt2-info'])
        line3 = str(i['pt3-info'])
        line4 = str(i['pt4-info'])
        line5 = str(i['pt5-info'])
        line6 = str(i['pt6-info'])
        line7 = str(i['pt7-info'])

        say.append(lastupdated_line)
        say.ssml_break(strength='x-strong', time='1s')
        first_block = line1
        if line2 != "None":
            first_block = first_block + " " + line2
        if line3 != "None":
            first_block = first_block + " " + line3
        say.append(first_block + ".")
        say.ssml_break(strength='weak', time='150ms')
        second_block = ""
        if line4 != "None":
            second_block = line4 + "."
        if line5 != "None":
            if len(second_block) == 0:
                second_block = line5 + "."
            else:
                second_block = second_block + " " + line5 + "."
        if line6 != "None":
            if len(second_block) == 0:
                second_block = line6 + "."
            else:
                second_block = second_block + " " + line6 + "."
        if line7 != "None":
            if len(second_block) == 0:
                second_block = line7 + "."
            else:
                second_block = second_block + " " + line7 + "."
        say.append(second_block)
        say.ssml_break(strength='weak', time='100ms')
        say.append("Full details are available at owen the dot dev slash snowday.")
        say.ssml_break(strength='weak', time='100ms')
        say.append("Goodbye!")
        resp.append(say)
        resp.hangup()
        response = Response(str(resp))
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 0)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response


@app.route("/api/v2/inboundSMS", methods=['POST'])
@limiter.limit(api_limit_inboundsms)
@shared_limiter
def inboundSMS():
    resp = MessagingResponse()

    unixtime = int(time.time())

    message_body = request.values.get('Body', None)
    message_from = request.values.get('From', None)
    key = request.values.get('key', "None")

    # Filter out iMessage sent with (very, very, very edge case)
    # Non-comprehensive list that needs more stuff
    message_body = message_body.replace(" (sent with Echo)", "")
    message_body = message_body.replace(" (sent with Spotlight)", "")
    message_body = message_body.replace(" (sent with Balloons)", "")
    message_body = message_body.replace(" (sent with Loud Effect)", "")
    message_body = message_body.replace(" (sent with Lasers)", "")
    message_body = message_body.replace(" (sent with Fireworks)", "")
    message_body = message_body.replace(" (sent with Reveal Effect)", "")

    if apiclosed is True:
        # Special response for API under maintenance.
        resp.message("Sorry, I couldn't understand your response since the backend systems are offline for maintenance. Check "
                     "https://status.owenthe.dev for more info.")
        response = Response(str(resp))
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 59)
        response.headers.add("Access-Control-Allow-Origin", cors_url)
        return response

    if key not in api_keys:
        resp.message("Error Code 54 - Inbound SMS API Key not set properly. Contact webmaster, "
                     "then try again later.")
        response = Response(str(resp))
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response

    response = table3.scan()
    for i in response['Items']:
        if str(i['number']) == str(message_from):
            response = response = Response('<?xml version="1.0" encoding="UTF-8"?><Response></Response>')
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 29)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response

    try:
        table3.put_item(
            Item={
                'number': message_from
            }
        )
    except Exception:
        pass

    if message_body == 'Y':
        try:
            response = table.scan()
        except ClientError:
            resp.message("(Snow Day Predictor): A database error prevented us from confirming your decision. Please "
                         "try again later.")
            response = Response(str(resp))
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 26)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response

        if len(response['Items']) >= api_limitregistrations_number:
            resp.message("(Snow Day Predictor): The Snow Day SMS Service has reached capacity. Your registration "
                         "request cannot be completed, please try again when there is capacity.")
            response = Response(str(resp))
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 48)
            response.headers.add("Access-Control-Allow-Origin", cors_url)
            return response

        try:
            response = table2.scan()
        except ClientError:
            resp.message("(Snow Day Predictor): A database error prevented us from confirming your decision. Please "
                         "try again later.")
            response = Response(str(resp))
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 26)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response

        for i in response['Items']:
            if str(i['number']) == str(message_from):
                resp.message("(Snow Day Predictor): Number verified! To customize messages, text DASH. To see what's "
                             "new, text WHATSNEW. For more commands, text CMDS. Msg rates may apply.")
                group = i['group']
                outboundnum = i['outboundNum']
                table2.delete_item(
                    Key={
                        'number': message_from
                    }
                )
                table.put_item(
                    Item={
                        'number': message_from,
                        'group': group,
                        'joindate': int(unixtime),
                        'message_hold': 0,
                        '24_hr_prefix': False,
                        'no_prefix': False,
                        'timezone': False,
                        'name': message_from,
                        'predictionde': True,
                        'predictioninfo': True,
                        'settingslinks': False,
                        'systemstatus': True,
                        'accountmanagement': True,
                        'outboundNum': outboundnum
                    }
                )

                table9.update_item(
                    Key={
                        'number': outboundnum
                    },
                    UpdateExpression='SET #cap = #cap + :n',
                    ExpressionAttributeValues={
                        ":n": 1
                    },
                    ExpressionAttributeNames={
                        "#cap": "capacity"
                    }
                )

                table4.put_item(
                    Item={
                        'number': message_from,
                        'errors': 0
                    }
                )

                response = Response(str(resp))
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 0)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response

        resp.message("(Snow Day Predictor): Your number isn't pending verification at this time. To resubscribe, "
                     "visit https://owenthe.dev/snowday")
        response = Response(str(resp))
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 0)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response

    elif message_body == 'N':
        try:
            response = table2.scan()
        except ClientError:
            resp.message("(Snow Day Predictor): A database error prevented us from confirming your decision. Please "
                         "try again later.")
            response = Response(str(resp))
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 26)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response

        for i in response['Items']:
            if str(i['number']) == str(message_from):
                resp.message("(Snow Day Predictor): Got it! You won't be sent messages. If you'd like to resubscribe, "
                             "visit https://owenthe.dev/snowday")
                table2.delete_item(
                    Key={
                        'number': message_from
                    }
                )

                response = Response(str(resp))
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 0)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response

        resp.message("(Snow Day Predictor): Your number isn't pending verification at this time. To subscribe, "
                     "visit https://owenthe.dev/snowday")
        response = Response(str(resp))
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 0)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response

    elif message_body == 'UNSUB' or message_body == 'STOP' or message_body == 'UNSUBSCRIBE':
        # If a user ends up requesting a full STOP code, we do get the response back, so just unsubscribe them from
        # the database.

        # Scan the numbers database to see if the number that is texting back is actually subscribed.
        # There is a potential edge case where somebody not subscribed will text back, and have the capacity count
        # improperly updated.

        response = table.query(
            KeyConditionExpression='#num = :num',
            ExpressionAttributeValues={
                ":num": message_from
            },
            ExpressionAttributeNames={
                "#num": "number"
            }
        )

        if len(response['Items']) == 1:
            for i in response['Items']:
                outboundNum = str(i['outboundNum'])

            table9.update_item(
                Key={
                    'number': str(response['Items'][0]['outboundNum'])
                },
                UpdateExpression='SET #cap = #cap - :n',
                ExpressionAttributeValues={
                    ":n": 1
                },
                ExpressionAttributeNames={
                    "#cap": "capacity"
                }
            )
        try:
            table2.delete_item(
                Key={
                    'number': message_from
                }
            )
            table.delete_item(
                Key={
                    'number': message_from
                }
            )
            try:
                table3.delete_item(
                    Key={
                        'number': message_from
                    }
                )
            except Exception:
                pass

            try:
                table4.delete_item(
                    Key={
                        'number': message_from
                    }
                )
            except Exception:
                pass

            try:
                response = table7.scan(
                    FilterExpression="#num = :num",
                    ExpressionAttributeValues={
                        ":num": message_from
                    },
                    ExpressionAttributeNames={
                        "#num": "number"
                    }
                )

                for i in response['Items']:
                    table7.delete_item(
                        Key={
                            "session": str(i['session'])
                        }
                    )
            except Exception:
                pass

            try:
                response = table6.scan(
                    FilterExpression="#num = :num",
                    ExpressionAttributeValues={
                        ":num": message_from
                    },
                    ExpressionAttributeNames={
                        "#num": "number"
                    }
                )

                for i in response['Items']:
                    table6.delete_item(
                        Key={
                            "session": str(i['session'])
                        }
                    )
            except Exception:
                pass

            resp.message("(Snow Day Predictor): Your number has been successfully unsubscribed. Thank you for using "
                         "the Snow Day SMS Service.")

            response = Response(str(resp))

            if message_body == 'UNSUB':
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 0)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response
            else:
                # If we receive a stop code, don't send anything back as it'll cost money, and just return empty handed
                response = Response('<?xml version="1.0" encoding="UTF-8"?><Response></Response>')
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 24)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response

        except Exception:
            resp.message(
                "(Snow Day Predictor): A database error prevented us from unsubscribing your number. Please try again "
                "later.")
            response = Response(str(resp))
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 26)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response

    elif message_body == 'START':
        response = Response('<?xml version="1.0" encoding="UTF-8"?><Response></Response>')
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 24)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response

    elif message_body == 'DASH':
        resp.message("(Snow Day Predictor): Customize your Snow Day SMS Service experience on the Snow Day Dashboard: "
                     "https://owenthe.dev/snowday/dash")
        response = Response(str(resp))
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 0)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response
    elif message_body == 'ABOUT':
        resp.message("(Snow Day Predictor): SMS Service to give you the latest snow day predictions. Version %s"
                     % apiversion_nov)
        response = Response(str(resp))
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 0)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response

    elif message_body == 'SUPPORT':
        resp.message("(Snow Day Predictor): For help with the Snow Day SMS Service, visit the support center: "
                     "https://owenthe.dev/snowday/support")
        response = Response(str(resp))
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 0)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response

    elif message_body == 'WHATSNEW':
        resp.message("(Snow Day Predictor): Here's what's new this season: https://owenthe.dev/snowday/whatsnew.html")
        response = Response(str(resp))
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 0)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response

    elif message_body == 'COMMANDS' or message_body == 'CMDS':
        resp.message("(Snow Day Predictor): \nSUPPORT - Help \nUNSUB - Unsubscribe\nERRORS - Show # of "
                     "errors for your number\nDASH - Get dashboard link\nWHATSNEW - Changelog this year")
        response = Response(str(resp))
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 0)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response

    elif message_body == 'ERRORS':
        try:
            response = table4.scan()
        except ClientError:
            resp.message("(Snow Day Predictor): A database error prevented us from retrieving your message delivery "
                         "error count. Please try again later.")
            response = Response(str(resp))
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 26)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response

        for i in response['Items']:
            if str(i['number']) == str(message_from):
                errors = str(i['errors'])

                resp.message("(Snow Day Predictor): Your number currently has %s/4 message delivery errors. More info "
                             "on the dash (https://owenthe.dev/snowday/dash)" % errors)
                response = Response(str(resp))
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 0)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response

        resp.message("(Snow Day Predictor): Your number currently has 0/4 message delivery errors. More info "
                     "on the dash (https://owenthe.ninja/snowday/dash)")
        response = Response(str(resp))
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 0)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response

    # Filter out iMessage reactions - This is an incomplete list and needs to be improved
    elif ('Liked “' in message_body) or ('Laughed at “' in message_body) or ('Disliked “' in message_body) \
            or ('Loved “' in message_body) or ('Emphasized “' in message_body) or ('Questioned “' in message_body):
        response = Response('<?xml version="1.0" encoding="UTF-8"?><Response></Response>')
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 24)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response

    else:
        # response = table3.scan()
        # for i in response['Items']:
        #     if str(i['number']) == str(message_from):
        #         response = response = Response('<?xml version="1.0" encoding="UTF-8"?><Response></Response>')
        #         response.headers.add("X-SnowDayAPI-Version", apiversion)
        #         response.headers.add("X-SnowDayAPI-Code", 29)
        #         response.headers.add('Access-Control-Allow-Origin', cors_url)
        #         return response

        # Send back a message when a user hasn't gotten the "this is a bot" message.
        # As of v2.2.0-rc.1 this is disabled to prevent confusion.
        # try:
        #     table3.put_item(
        #         Item={
        #             'number': message_from
        #         }
        #     )
        # except Exception:
        #     pass

        resp.message("(Snow Day Predictor): Sorry, I couldn't understand what you said. If you are trying to text a "
                     "command, check your spelling & make sure you are using all caps.")
        response = Response(str(resp))
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 0)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response


@app.route("/api/v2/updateUserGroup", methods=['POST'])
@limiter.limit(api_limit_updateusergroup)
@shared_limiter
def updateusergroup():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    key = request.values.get('key', "None")
    group = request.values.get('group', None)
    number = request.values.get('number', "None")
    pending_number = request.values.get('pendingnumber', "None")
    set_default = request.values.get('setdefaultgroup', "None")

    if key not in api_keys:
        response = {"apiversion": apiversion, "code": 54, "data": {},
                    "message": "The API key defined is invalid. Please use a valid API key to use this API."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 403

    if group is None and set_default != "true":
        response = {"apiversion": apiversion, "code": 91, "data": {},
                    "message": "The group parameter was not defined. Please define a group."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 91)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 400

    if number == "None":
        response = {"apiversion": apiversion, "code": 92, "data": {},
                    "message": "The number parameter was not defined. Please define a number to make modifications."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 92)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 400

    if set_default == "true":
        group = api_defaultgroup

    if group not in api_groups:
        response = {"apiversion": apiversion, "code": 90, "data": {},
                    "message": "The group defined in the request is not a valid group."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 90)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 422

    if pending_number != "true":
        response = table.scan()
        number_registered = False
        for i in response['Items']:
            if str(i['number']) == number:
                number_registered = True

        # For security reasons, instead of saying that the user does not exist, we just say the database operation
        # was unsuccessful.

        if number_registered is False:
            response = {"apiversion": apiversion, "code": 41, "data": {},
                        "message": "The database operation was unsuccessful."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 41)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response, 503

        try:
            table.update_item(
                Key={
                    'number': number,
                },
                UpdateExpression='SET #grp = :s',
                ExpressionAttributeValues={
                    ":s": group
                },
                ExpressionAttributeNames={
                    "#grp": "group"
                },
                ReturnValues="UPDATED_NEW"
            )
            response = {"apiversion": apiversion, "code": 40, "data": {},
                        "message": "The database operation was successful."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 40)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response
        except Exception:
            response = {"apiversion": apiversion, "code": 41, "data": {},
                        "message": "The database operation was unsuccessful."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 41)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response, 503
    else:
        try:
            response = table2.scan()
            number_registered = False
            for i in response['Items']:
                if str(i['number']) == number:
                    number_registered = True

            # For security reasons, instead of saying that the user does not exist, we just say the database operation
            # was unsuccessful.

            if number_registered is False:
                response = {"apiversion": apiversion, "code": 41, "data": {},
                            "message": "The database operation was unsuccessful."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 41)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response, 503

            table2.update_item(
                Key={
                    'number': number,
                },
                UpdateExpression='SET #grp = :s',
                ExpressionAttributeValues={
                    ":s": group
                },
                ExpressionAttributeNames={
                    "#grp": "group"
                },
                ReturnValues="UPDATED_NEW"
            )
            response = {"apiversion": apiversion, "code": 40, "data": {},
                        "message": "The database operation was successful."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 40)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response
        except Exception:
            response = {"apiversion": apiversion, "code": 41, "data": {},
                        "message": "The database operation was unsuccessful."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 41)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response, 503


@app.route("/api/v2/newMessage", methods=['POST'])
@limiter.limit(api_limit_newmessage)
@shared_limiter
def postmessage():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    unix_time = int(time.time())

    messagetext = request.values.get('message', None)
    key = request.values.get('key', "None")
    usetemplate = request.values.get('usetemplate', "None")
    time_val = request.values.get('time', None)
    date = request.values.get('date', None)
    timezone_value = request.values.get('timezone', None)
    groups = request.values.get('groups', None)
    targeted_nums = request.values.get('targetednums', None)

    if usetemplate.lower() == "true":
        usetemplate = True

    if key not in api_keys:
        response = {"apiversion": apiversion, "code": 54, "data": {},
                    "message": "The API key defined is invalid. Please use a valid API key to use this API."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 403

    if usetemplate is True:
        if time is None or date is None or timezone_value is None:
            response = {"apiversion": apiversion, "code": 28, "data": {},
                        "message": "One or more required values for using a template were not defined."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 28)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response, 400

        time_raw = time_val.split(":")
        if len(time_raw) != 2:
            response = {"apiversion": apiversion, "code": 29, "data": {},
                        "message": "The time value was not defined properly. It must include a colon."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 29)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response, 400

        try:
            time_12hr_pos0 = int(time_raw[0])
            if time_12hr_pos0 < 12:
                time_12hr_denom = "AM"
            else:
                time_12hr_denom = "PM"

            if time_12hr_pos0 > 12:
                time_12hr_pos0 = time_12hr_pos0 - 12

            if time_12hr_pos0 == 0:
                time_12hr_pos0 = 12

            time_12hr = str(time_12hr_pos0) + ":" + str(time_raw[1]) + " " + str(time_12hr_denom)
        except (ValueError, TypeError):
            response = {"apiversion": apiversion, "code": 30, "data": {},
                        "message": "When processing the time defined, a value or type error occurred. Make sure that "
                                   "time is defined with integers only and a colon in between. Check the API docs "
                                   "for more info."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 30)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response, 422

        worstcase_message = "(Snow Day Predictor): New prediction " + time_12hr + " " + timezone_value + ", " + date + \
                            ": " + messagetext
        if len(worstcase_message) > 160:
            response = {"apiversion": apiversion, "code": 31, "data": {},
                        "message": "The worst-case templated message exceeded 160 characters. Please reduce the "
                                   "length of messagetext."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 31)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response, 413

    if len(messagetext) > 138:
        response = {"apiversion": apiversion, "code": 27, "data": {},
                    "message": "The message contents defined exceeded 138 characters. Reduce the message to 138 "
                               "characters or lower."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 27)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 413

    if groups is not None:
        groups = groups.split(",")
        for group in groups:
            if group not in api_groups:
                response = {"apiversion": apiversion, "code": 90, "data": {},
                            "message": "The groups defined in the request are not valid groups."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 90)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response, 422
    else:
        groups = api_groups

    raw_message_text = messagetext

    targeted_nums_avail = False

    if targeted_nums is not None:
        targeted_nums_array = targeted_nums.split(",")
        targeted_nums_avail = True

    if targeted_nums_avail is True:
        for index, val in enumerate(targeted_nums_array):
            if val[:2] != "+1":
                response = {"apiversion": apiversion, "code": 96, "data": {},
                            "message": "One of the targeted numbers did not include the +1 prefix (at index %s)" % index}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 96)
                response.headers.add("Access-Control-Allow-Origin", cors_url)
                return response, 422

            if len(val) != 12:
                response = {"apiversion": apiversion, "code": 95, "data": {},
                            "message": "One of the targeted numbers was of invalid length (at index %s)" % index}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 95)
                response.headers.add("Access-Control-Allow-Origin", cors_url)
                return response, 422

            try:
                numprefix = int(val[2:5])
            except ValueError:
                response = {"apiversion": apiversion, "code": 97, "data": {},
                            "message": "One of the targeted numbers had a bad area code that was not a number (at "
                                       "index %s)" % index}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 97)
                response.headers.add("Access-Control-Allow-Origin", cors_url)
                return response, 422

            if numprefix not in valid_codes:
                response = {"apiversion": apiversion, "code": 98, "data": {},
                            "message": "One of the targeted numbers had an invalid area code that was not a valid US "
                                       "area code (at index %s)" % index}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 98)
                response.headers.add("Access-Control-Allow-Origin", cors_url)
                return response, 422

    response = table.scan()
    for i in response['Items']:
        number = i['number']
        if targeted_nums_avail is True:
            if number not in targeted_nums_array:
                continue
        group = i['group']
        twentyfour = str(i['24_hr_prefix'])
        noprefix = str(i['no_prefix'])
        timezone = str(i['timezone'])
        messagehold = int(i['message_hold'])
        outboundNum = str(i['outboundNum'])
        if (messagehold <= unix_time) and (messagehold != 0):
            table.update_item(
                Key={
                    'number': number
                },
                UpdateExpression='SET #hld = :n',
                ExpressionAttributeValues={
                    ":n": 0
                },
                ExpressionAttributeNames={
                    "#hld": "message_hold"
                },
                ReturnValues="UPDATED_NEW"
            )
        elif messagehold > unix_time:
            continue

        if twentyfour.lower() == "true":
            twentyfour = True
        elif twentyfour.lower() == "false":
            twentyfour = False
        else:
            twentyfour = False

        if noprefix.lower() == "true":
            noprefix = True
        elif noprefix.lower() == "false":
            noprefix = False
        else:
            noprefix = False

        if timezone.lower() == "true":
            timezone = True
        elif timezone.lower() == "false":
            timezone = False
        else:
            timezone = False

        templated_message = ""
        messagetext = ""
        if usetemplate is True:
            if noprefix is False:
                templated_message = "(Snow Day Predictor): "

            templated_message = templated_message + "New prediction "

            if twentyfour is True:
                templated_message = templated_message + time
            elif twentyfour is False:
                templated_message = templated_message + time_12hr

            if timezone is True:
                templated_message = templated_message + " " + timezone_value

            templated_message = templated_message + ", " + date + ": " + raw_message_text
            messagetext = templated_message
        else:
            messagetext = "(Snow Day Predictor): " + raw_message_text

        if group in groups:
            try:
                message = client.messages.create(
                    body=messagetext,
                    from_=outboundNum,
                    status_callback=api_sms_outgoingcallback,
                    max_price=api_sms_maxprice,
                    to=number
                )
            except twilio.base.exceptions.TwilioRestException:
                continue

    response = {"apiversion": apiversion, "code": 0, "data": {},
                "message": "The action completed successfully."}
    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 0)
    response.headers.add('Access-Control-Allow-Origin', cors_url)
    return response


@app.route("/api/v2/outgoingSMSstatus", methods=['POST'])
@limiter.limit(api_limit_outgoingsmsstatus)
@shared_limiter
def messagestatus():
    unix_time = int(time.time())
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    message_to = request.values.get('To', None)
    message_status = request.values.get('SmsStatus', None)
    key = request.values.get('key', "None")
    if key not in api_keys:
        response = {"apiversion": apiversion, "code": 54, "data": {},
                    "message": "The API key defined is invalid. Please use a valid API key to use this API."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 403

    config.read("config.ini")
    try:
        dmdc_state = config.get('MAIN', 'confirmation_disabled')
        if dmdc_state not in ["True", "False"]:
            dmcd_state = "False"
    except Exception:
        dmdc_state = "False"

    if dmdc_state == "True":
        response = {"apiversion": apiversion, "code": 93, "data": {},
                    "message": "Message Delivery Confirmation has been disabled. Outgoing SMS status reports are not "
                               "being accepted at this time."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 93)
        response.headers.add("Access-Control-Allow-Origin", cors_url)
        return response, 503

    if message_status == "failed" or message_status == "undelivered":
        response = table4.scan()
        for i in response['Items']:
            if str(i['number']) == str(message_to):
                if i['errors'] >= 3:
                    table4.delete_item(
                        Key={
                            'number': message_to
                        }
                    )
                    numtable_response = table.query(
                        KeyConditionExpression='#num = :num',
                        ExpressionAttributeValues={
                            ":num": message_to
                        },
                        ExpressionAttributeNames={
                            "#num": "number"
                        }
                    )

                    outboundNum = "+18452456252"
                    outboundNum = str(numtable_response['Items'][0]['outboundNum'])

                    table.delete_item(
                        Key={
                            'number': message_to
                        }
                    )
                    try:
                        message = client.messages.create(
                            body="(Snow Day Predictor): Your number has accumulated too many delivery errors, "
                                 "so you've been unsubscribed. Resubscribe @ https://owenthe.dev/snowday",
                            from_=outboundNum,
                            to=message_to
                        )

                    except twilio.base.exceptions.TwilioRestException:
                        response = {"apiversion": apiversion, "code": 25, "data": {},
                                    "message": "The message indicating to the user that they accumulated too many "
                                               "delivery errors failed to send."}
                        response = jsonify(response)
                        response.headers.add("X-SnowDayAPI-Version", apiversion)
                        response.headers.add("X-SnowDayAPI-Code", 25)
                        response.headers.add('Access-Control-Allow-Origin', cors_url)
                        return response, 502

                    response = {"apiversion": apiversion, "code": 23, "data": {},
                                "message": "The message indicating to the user that they accumulated too many "
                                           "delivery errors successfully sent."}
                    response = jsonify(response)
                    response.headers.add("X-SnowDayAPI-Version", apiversion)
                    response.headers.add("X-SnowDayAPI-Code", 23)
                    response.headers.add('Access-Control-Allow-Origin', cors_url)
                    return response
                else:
                    if i['errors'] == 0:
                        table4.update_item(
                            Key={
                                'number': message_to,
                            },
                            UpdateExpression='SET errors = errors + :n, me1_time = :a, me1_reason = :b',
                            ExpressionAttributeValues={
                                ":n": 1,
                                ":a": unix_time,
                                ":b": message_status
                            }
                        )
                    if i['errors'] == 1:
                        table4.update_item(
                            Key={
                                'number': message_to,
                            },
                            UpdateExpression='SET errors = errors + :n, me2_time = :a, me2_reason = :b',
                            ExpressionAttributeValues={
                                ":n": 1,
                                ":a": unix_time,
                                ":b": message_status
                            }
                        )
                    if i['errors'] == 2:
                        table4.update_item(
                            Key={
                                'number': message_to,
                            },
                            UpdateExpression='SET errors = errors + :n, me3_time = :a, me3_reason = :b',
                            ExpressionAttributeValues={
                                ":n": 1,
                                ":a": unix_time,
                                ":b": message_status
                            }
                        )
                    response = {"apiversion": apiversion, "code": 30, "data": {},
                                "message": "The operation increasing the amount of errors by 1 has succeeded."}
                    response = jsonify(response)
                    response.headers.add("X-SnowDayAPI-Version", apiversion)
                    response.headers.add("X-SnowDayAPI-Code", 30)
                    response.headers.add('Access-Control-Allow-Origin', cors_url)
                    return response
    else:
        # No error, just return a 200
        response = {"apiversion": apiversion, "code": 31, "data": {},
                    "message": "The message status does not require any further action."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 31)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response


@app.route("/api/v2/disableMessageDeliveryConfirmation", methods=['POST'])
@limiter.limit(api_limit_disablemessagedeliveryconfirmation)
@shared_limiter
def dmdc():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    key = request.values.get('key', "none")
    setting = request.values.get('setting', None)

    if key not in api_keys:
        response = {"apiversion": apiversion, "code": 54, "data": {},
                    "message": "The API key defined is invalid. Please use a valid API key to use this API."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 403

    if setting is not None:
        setting = str(setting)
        if setting.lower() == "true":
            config['MAIN']['confirmation_disabled'] = "True"

            try:
                with open("config.ini", 'w') as configfile:
                    config.write(configfile)

                response = {"apiversion": apiversion, "code": 0, "data": {},
                            "message": "The operation was successful."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 0)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response
            except Exception:
                response = {"apiversion": apiversion, "code": 1, "data": {},
                            "message": "The operation was unsuccessful."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 1)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response, 500
        elif setting.lower() == "false":
            config['MAIN']['confirmation_disabled'] = "False"

            try:
                with open("config.ini", 'w') as configfile:
                    config.write(configfile)

                response = {"apiversion": apiversion, "code": 0, "data": {},
                            "message": "The operation was successful."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 0)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response
            except Exception:
                response = {"apiversion": apiversion, "code": 1, "data": {},
                            "message": "The operation was unsuccessful."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 1)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response, 500
        else:
            response = {"apiversion": apiversion, "code": 2, "data": {},
                        "message": "The setting parameter was not defined properly. No changes have been made."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 2)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response, 422
    else:
        try:
            current_setting = config.get('MAIN', 'confirmation_disabled')
        except Exception:
            response = {"apiversion": apiversion, "code": 4, "data": {},
                        "message": "Internal Server Error: There was a problem accessing the MDC setting."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 4)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response, 500

        if current_setting == "True":
            config['MAIN']['confirmation_disabled'] = "False"

            try:
                with open("config.ini", 'w') as configfile:
                    config.write(configfile)

                response = {"apiversion": apiversion, "code": 0, "data": {},
                            "message": "The operation was successful."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 0)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response
            except Exception:
                response = {"apiversion": apiversion, "code": 1, "data": {},
                            "message": "The operation was unsuccessful."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 1)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response, 500
        elif current_setting == "False":
            config['MAIN']['confirmation_disabled'] = "True"

            try:
                with open("config.ini", 'w') as configfile:
                    config.write(configfile)

                response = {"apiversion": apiversion, "code": 0, "data": {},
                            "message": "The operation was successful."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 0)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response
            except Exception:
                response = {"apiversion": apiversion, "code": 1, "data": {},
                            "message": "The operation was unsuccessful."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 1)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response, 500
        else:
            response = {"apiversion": apiversion, "code": 3, "data": {},
                        "message": "Internal Server Error: The pre-configured setting is not an acceptable value."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 3)
            response.headers.add('Access-Control-Allow-Origin', cors_url)
            return response, 500


@app.route("/api/v2/confirmMessageErrorCode", methods=['POST'])
@limiter.limit(api_limit_confirmmessageerrorcode)
@shared_limiter
def confirmmessageerrorcode():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    config.read("config.ini")
    try:
        dmdc_state = config.get('MAIN', 'confirmation_disabled')
        if dmdc_state not in ["True", "False"]:
            dmdc_state = "False"
    except Exception:
        dmdc_state = "False"

    status = request.values.get('ErrorCode', 0)
    messagestatus = request.values.get('MessageStatus', "none")
    sid = request.values.get('MessageSid', "none")
    key = request.values.get('key', "none")

    if key not in api_keys:
        response = {"apiversion": apiversion, "code": 54, "data": {},
                    "message": "The API key defined is invalid. Please use a valid API key to use this API."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 403

    if messagestatus == "queued" and dmdc_state == "True":
        response = {"apiversion": apiversion, "code": 40, "data": {},
                    "message": "The message status does not require any further action."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 40)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response
    elif messagestatus in ["sent", "queued"] and dmdc_state == "False":
        response = {"apiversion": apiversion, "code": 40, "data": {},
                    "message": "The message status does not require any further action."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 40)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response
    else:
        response = table5.scan()
        for i in response['Items']:
            if str(i['sid']) == sid:
                table5.update_item(
                    Key={
                        'sid': sid,
                    },
                    UpdateExpression='SET code = :n',
                    ExpressionAttributeValues={
                        ":n": status
                    }
                )
                response = {"apiversion": apiversion, "code": 41, "data": {},
                            "message": "The database operation was successful."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 41)
                response.headers.add('Access-Control-Allow-Origin', cors_url)
                return response

    response = {"apiversion": apiversion, "code": 40, "data": {},
                "message": "The message status does not require any further action."}
    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 40)
    response.headers.add('Access-Control-Allow-Origin', cors_url)
    return response


@app.route("/api/v2/subscribeNumber", methods=['POST'])
@limiter.limit(api_limit_subscribenumber)
@shared_limiter
def subscribe_number():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    number = request.values.get('number', None)
    key = request.values.get('key', None)
    outboundNum = request.values.get('outboundNum', None)

    if key not in api_keys:
        response = {"apiversion": apiversion, "code": 54, "data": {},
                    "message": "The API key defined is invalid. Please use a valid API key to use this API."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 403

    if len(number) != 12 or "+1" not in number:
        response = {"apiversion": apiversion, "code": 83, "data": {},
                    "message": "Number improperly formatted. The number must include +1, area code, and no extra "
                               "characters."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 83)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 422

    response = table10.query(
        KeyConditionExpression="#num = :num",
        ExpressionAttributeValues={
            ":num": number
        },
        ExpressionAttributeNames={
            "#num": "number"
        }
    )

    if len(response['Items']) == 1:
        reason = response['Items'][0]['reason']
        response = {"apiversion": apiversion, "code": 60, "data": {"reason": reason},
                    "message": "The number defined is banned from the SMS service."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 60)
        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
        return response, 401

    response = table.scan()

    if api_limitregistrations is True:
        if len(response['Items']) > api_limitregistrations_number:
            response = {"apiversion": apiversion, "code": 33, "data": {},
                        "message": "The maximum number of numbers registered has been reached, the number defined "
                                   "could not be registered. Please try again when some users have unsubscribed."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 33)
            response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
            return response, 503

    for i in response['Items']:
        if str(i['number']) == number:
            response = {"apiversion": apiversion, "code": 81, "data": {},
                        "message": "The number defined is already subscribed to the service."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 81)
            response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
            return response, 400

    response = table2.scan()

    for i in response['Items']:
        if str(i['number']) == number:
            response = {"apiversion": apiversion, "code": 82, "data": {},
                        "message": "The number defined is pending confirmation, and cannot be subscribed using "
                                   "this endpoint."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 82)
            response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
            return response, 400

    numcapacity_response = table9.scan()

    if outboundNum is not None:
        if outboundNum not in api_numbers:
            response = {"apiversion": apiversion, "code": 92, "data": {},
                        "message": "The outbound number defined in the request is not a valid number."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 92)
            response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
            return response, 422

        if api_limitregistrations is True:
            for i in numcapacity_response['Items']:
                if str(i['number']) == outboundNum:
                    if int(i['capacity']) >= api_limitregistrations_number / api_numbers_count:
                        response = {"apiversion": apiversion, "code": 93, "data": {},
                                    "message": "The outbound number defined in the request has reached capacity. "
                                               "Please choose a different number."}
                        response = jsonify(response)
                        response.headers.add("X-SnowDayAPI-Version", apiversion)
                        response.headers.add("X-SnowDayAPI-Code", 93)
                        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
                        return response, 422
    else:
        lowestcap_count = 99999999
        lowestcap_number = ""
        for i in numcapacity_response['Items']:
            if int(i['capacity']) < lowestcap_count:
                lowestcap_count = int(i['capacity'])
                lowestcap_number = str(i['number'])

        outboundNum = lowestcap_number

    try:
        unixtime = int(time.time())
        table.put_item(
            Item={
                'number': number,
                'group': 'high',
                'joindate': int(unixtime),
                'message_hold': 0,
                '24_hr_prefix': False,
                'no_prefix': False,
                'timezone': False,
                'name': number,
                'predictionde': True,
                'predictioninfo': True,
                'settingslinks': False,
                'systemstatus': True,
                'accountmanagement': True,
                'outboundNum': outboundNum

            }
        )
    except Exception:
        response = {"apiversion": apiversion, "code": 84, "data": {},
                    "message": "A database error occurred while trying to subscribe the number defined. Please try "
                               "again later. "
                    }
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 84)
        response.headers.add('Access-Control-Allow-Origin', cors_url_frontend)
        return response, 500

    try:
        message = client.messages.create(
            body="(Snow Day Predictor): Your number has been subscribed to the Snow Day SMS Service by an "
                 "Administrator. If this was made in error, text back UNSUB.",
            from_=outboundNum,
            to=number
        )

        table9.update_item(
            Key={
                'number': outboundNum
            },
            UpdateExpression='SET #cap = #cap + :n',
            ExpressionAttributeValues={
                ":n": 1
            },
            ExpressionAttributeNames={
                "#cap": "capacity"
            }
        )

        response = {"apiversion": apiversion, "code": 0, "data": {},
                    "message": "The action was completed successfully."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 0)
        response.headers.add("Access-Control-Allow-Origin", cors_url_frontend)
        return response
    except twilio.base.exceptions.TwilioRestException:
        table.delete_item(
            Key={
                "number": number
            }
        )
        response = {"apiversion": apiversion, "code": 85, "data": {},
                    "message": "The message indicating to the user that they were subscribed failed to send, "
                               "likely because of a number block. The number defined has not been subscribed to the "
                               "service."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 0)
        response.headers.add("Access-Control-Allow-Origin", cors_url)
        return response, 400


@app.route("/api/v2/unsubscribeNumber", methods=['POST'])
@limiter.limit(api_limit_unsubscribenumber)
@shared_limiter
def unsubscribe_number():
    number = request.values.get('number', None)
    key = request.values.get('key', None)
    session_id_form = request.values.get('session_id', None)
    session_id = request.cookies.get('sessionid', None)
    specialmessage = request.values.get('specialMsg', None)

    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    # Backwards compatibility with v1.5.0 code pre-SDD
    if session_id_form is not None:
        response = {"apiversion": apiversion, "code": 52, "data": {},
                    "message": "Session ID authentication must occur via. cookies. Please see API documentation for "
                               "more information."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 52)
        response.headers.add("Access-Control-Allow-Origin", cors_url)
        return response, 400

    # Determine authentication type
    if session_id is not None and key is None:
        auth_method = "cookies"
    elif session_id is None and key is not None:
        auth_method = "key"
    elif session_id is None and key is None:
        auth_method = "bad_nonespec"
    elif session_id is not None and key is not None:
        auth_method = "bad_bothspec"

    if (auth_method == "key") and (key not in api_keys):
        response = {"apiversion": apiversion, "code": 54, "data": {},
                    "message": "The API key defined is invalid. Please use a valid API key to use this API."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 403

    # If coming from the SDD, we check to see if session ID is none and key should be none
    if auth_method == "bad_nonespec":
        response = {"apiversion": apiversion, "code": 129, "data": {},
                    "message": "Session ID or API key not defined. This may be due to cookie expiration or "
                               "deletion, or due to bad form data."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 129)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    # Warn if authentication occurs with session ID & key
    if auth_method == "bad_bothspec":
        response = {"apiversion": apiversion, "code": 2, "data": {},
                    "message": "Session ID and API key were both defined. Please only define one authentication "
                               "type for this method."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 2)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    if auth_method == "cookies":
        origin = request.environ.get('HTTP_ORIGIN', None)
        if origin != cors_url_dashend:
            response = {"apiversion": apiversion, "code": 100, "data": {},
                        "message": "Bad origin."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 100)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            return response, 400

    # Scan table to see if number exists
    if auth_method == "key":
        number_exists = False
        response = table.scan()
        outboundNum = "+18452456252"
        for i in response['Items']:
            if str(i['number']) == number:
                number_exists = True
                outboundNum = str(i['outboundNum'])

        if number_exists is False:
            response = {"apiversion": apiversion, "code": 1, "data": {},
                        "message": "The action failed to complete."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 1)
            response.headers.add("Access-Control-Allow-Origin", cors_url)
            return response, 500

        try:
            table9.update_item(
                Key={
                    'number': outboundNum
                },
                UpdateExpression='SET #cap = #cap - :n',
                ExpressionAttributeValues={
                    ":n": 1
                },
                ExpressionAttributeNames={
                    "#cap": "capacity"
                }
            )
            table.delete_item(
                Key={
                    "number": number
                }
            )

            table2.delete_item(
                Key={
                    'number': number
                }
            )

            try:
                table3.delete_item(
                    Key={
                        'number': number
                    }
                )
            except Exception:
                pass

            try:
                table4.delete_item(
                    Key={
                        'number': number
                    }
                )
            except Exception:
                pass

            try:
                response = table7.scan(
                    FilterExpression="#num = :num",
                    ExpressionAttributeValues={
                        ":num": number
                    },
                    ExpressionAttributeNames={
                        "#num": "number"
                    }
                )

                for i in response['Items']:
                    table7.delete_item(
                        Key={
                            "session": str(i['session'])
                        }
                    )
            except Exception:
                pass

            try:
                response = table6.scan(
                    FilterExpression="#num = :num",
                    ExpressionAttributeValues={
                        ":num": number
                    },
                    ExpressionAttributeNames={
                        "#num": "number"
                    }
                )

                for i in response['Items']:
                    table6.delete_item(
                        Key={
                            "session": str(i['session'])
                        }
                    )
            except Exception:
                pass

            try:
                if str(specialmessage).lower() == "true":
                    message = client.messages.create(
                        body=api_unsubscribenumber_specialmsg_text,
                        from_=outboundNum,
                        to=number
                    )
                else:
                    message = client.messages.create(
                        body="(Snow Day Predictor): Your number has been unsubscribed from the Snow Day SMS Service. If "
                             "this was made in error, resubscribe @ https://owenthe.dev/snowday",
                        from_=outboundNum,
                        to=number
                    )

                response = {"apiversion": apiversion, "code": 0, "data": {},
                            "message": "The action was completed successfully."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 0)
                response.headers.add("Access-Control-Allow-Origin", cors_url)
                return response

            except twilio.base.exceptions.TwilioRestException:
                response = {"apiversion": apiversion, "code": 0, "data": {},
                            "message": "The action was completed successfully."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 0)
                response.headers.add("Access-Control-Allow-Origin", cors_url)
                return response

        except Exception:
            response = {"apiversion": apiversion, "code": 1, "data": {},
                        "message": "The action failed to complete."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 11)
            response.headers.add("Access-Control-Allow-Origin", cors_url)
            return response, 500
    elif auth_method == "cookies":
        try:
            response = table7.scan()

            for i in response['Items']:
                if str(i['session']) == session_id:
                    if int(i['expires']) <= int(time.time()):
                        table7.delete_item(
                            Key={
                                "session": session_id
                            }
                        )

                        response = {"apiversion": apiversion, "code": 128, "data": {},
                                    "message": "Session has expired. Please request a new session to unsubscribe."}
                        response = jsonify(response)
                        response.headers.add("X-SnowDayAPI-Version", apiversion)
                        response.headers.add("X-SnowDayAPI-Code", 128)
                        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                        response.headers.add("Access-Control-Allow-Credentials", "true")
                        response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
                        return response, 401

                    number = i['number']
                    response = table.query(
                        KeyConditionExpression='#num = :num',
                        ExpressionAttributeValues={
                            ":num": number
                        },
                        ExpressionAttributeNames={
                            "#num": "number"
                        }
                    )

                    outboundNum = str(response['Items'][0]['outboundNum'])

                    table9.update_item(
                        Key={
                            'number': outboundNum
                        },
                        UpdateExpression='SET #cap = #cap - :n',
                        ExpressionAttributeValues={
                            ":n": 1
                        },
                        ExpressionAttributeNames={
                            "#cap": "capacity"
                        }
                    )

                    table.delete_item(
                        Key={
                            "number": number
                        }
                    )

                    table2.delete_item(
                        Key={
                            'number': number
                        }
                    )

                    try:
                        table3.delete_item(
                            Key={
                                'number': number
                            }
                        )
                    except Exception:
                        pass

                    try:
                        table4.delete_item(
                            Key={
                                'number': number
                            }
                        )
                    except Exception:
                        pass

                    try:
                        response = table7.scan(
                            FilterExpression="#num = :num",
                            ExpressionAttributeValues={
                                ":num": number
                            },
                            ExpressionAttributeNames={
                                "#num": "number"
                            }
                        )

                        for i in response['Items']:
                            table7.delete_item(
                                Key={
                                    "session": str(i['session'])
                                }
                            )
                    except Exception:
                        pass

                    try:
                        response = table6.scan(
                            FilterExpression="#num = :num",
                            ExpressionAttributeValues={
                                ":num": number
                            },
                            ExpressionAttributeNames={
                                "#num": "number"
                            }
                        )

                        for i in response['Items']:
                            table6.delete_item(
                                Key={
                                    "session": str(i['session'])
                                }
                            )
                    except Exception:
                        pass

                    table7.delete_item(
                        Key={
                            "session": session_id
                        }
                    )

                    try:
                        message = client.messages.create(
                            body="(Snow Day Predictor): Your number has been unsubscribed from the Snow Day SMS "
                                 "Service. If this was made in error, resubscribe @ https://owenthe.dev/snowday",
                            from_=outboundNum,
                            to=number
                        )
                        response = {"apiversion": apiversion, "code": 0, "data": {},
                                    "message": "The action was completed successfully."}
                        response = jsonify(response)
                        response.headers.add("X-SnowDayAPI-Version", apiversion)
                        response.headers.add("X-SnowDayAPI-Code", 0)
                        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                        response.headers.add("Access-Control-Allow-Credentials", "true")
                        response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
                        return response

                    except twilio.base.exceptions.TwilioRestException:
                        response = {"apiversion": apiversion, "code": 0, "data": {},
                                    "message": "The action was completed successfully."}
                        response = jsonify(response)
                        response.headers.add("X-SnowDayAPI-Version", apiversion)
                        response.headers.add("X-SnowDayAPI-Code", 0)
                        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                        response.headers.add("Access-Control-Allow-Credentials", "true")
                        response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
                        return response

            response = {"apiversion": apiversion, "code": 147, "data": {},
                        "message": "Session ID could not be found. Please request a new session to unsubscribe."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 0)
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
            return response, 400
        except Exception:
            response = {"apiversion": apiversion, "code": 1, "data": {},
                        "message": "The action failed to completed successfully due to a server-side error. Please "
                                   "try again later."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 1)
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
            return response, 500


@app.route("/api/v2/terminateSession", methods=['POST'])
@limiter.limit(api_limit_terminatesession)
@shared_limiter
def terminate_session():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    origin = request.environ.get('HTTP_ORIGIN', None)
    if origin != cors_url_dashend:
        response = {"apiversion": apiversion, "code": 100, "data": {},
                    "message": "Bad origin."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 100)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    session_id = request.cookies.get("sessionid", None)

    if session_id is None:
        response = {"apiversion": apiversion, "code": 129, "data": {},
                    "message": "Session ID not defined. This may be due to cookie expiration or deletion."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 129)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    response = table7.scan()

    for i in response['Items']:
        if str(i['session']) == session_id:
            table7.delete_item(
                Key={
                    "session": session_id
                }
            )

            response = {"apiversion": apiversion, "code": 146, "data": {},
                        "message": "Session was successfully terminated."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 146)
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
            return response

    response = {"apiversion": apiversion, "code": 147, "data": {},
                "message": "Session not found. This may be due to the session already being terminated."}
    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 147)
    response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
    response.headers.add("Access-Control-Allow-Credentials", "true")
    response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
    return response, 400


@app.route("/api/v2/updateUserSettings", methods=['POST'])
@limiter.limit(api_limit_updateusersettings)
@shared_limiter
def update_user_settings():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    origin = request.environ.get('HTTP_ORIGIN', None)
    if origin != cors_url_dashend:
        response = {"apiversion": apiversion, "code": 100, "data": {},
                    "message": "Bad origin."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 100)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    unixtime = int(time.time())

    session_id = request.cookies.get("sessionid", None)
    group = request.values.get("userGroup", None)
    message_hold = request.values.get("messageHold", None)
    twentyfour_hr_prefix = request.values.get("twentyFourPrefix", None)
    no_prefix = request.values.get("noPrefix", None)
    timezone_in_msg = request.values.get("timezoneInMsg", None)
    dashName = request.values.get("dashName", None)
    pihomepage = request.values.get("pihomepage", None)
    pdehomepage = request.values.get("pdehomepage", None)
    sshomepage = request.values.get("sshomepage", None)
    sqlhomepage = request.values.get("sqlhomepage", None)
    amhomepage = request.values.get("amhomepage", None)
    outboundNum = request.values.get("outboundNum", None)

    if session_id is None:
        response = {"apiversion": apiversion, "code": 129, "data": {},
                    "message": "Session ID not defined. This may be due to cookie expiration or deletion."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 129)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    # Scan sessions table to see if session is still valid (or not)

    response = table7.scan()

    valid_response = False
    for i in response['Items']:
        if str(i['session']) == session_id:
            if int(i['expires']) <= int(time.time()):
                table7.delete_item(
                    Key={
                        "session": session_id
                    }
                )

                response = {"apiversion": apiversion, "code": 128, "data": {},
                            "message": "Session has expired. Please request a new session to update user settings."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 128)
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
                return response, 401
            else:
                assc_number = str(i['number'])
                table7.update_item(
                    Key={
                        "session": session_id
                    },
                    UpdateExpression="SET expires = :n",
                    ExpressionAttributeValues={
                        ":n": int(time.time()) + 604800
                    }
                )
                valid_response = True

    if valid_response is False:
        response = {"apiversion": apiversion, "code": 125, "data": {},
                    "message": "Session not found. This may be due to an expired session that was deleted from calling "
                               "another session-based method."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 125)
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
        return response, 400

    # Verify data types

    # TODO: Add group checking
    if group is not None:
        try:
            group = str(group)
            if group not in api_groups:
                response = {"apiversion": apiversion, "code": 153, "data": {},
                            "message": "Invalid data - Group does not exist, and is invalid."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 153)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        except ValueError:
            response = {"apiversion": apiversion, "code": 133, "data": {},
                        "message": "Invalid data type - Group must be a string."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 133)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            return response, 422

    if message_hold is not None:
        try:
            message_hold = float(message_hold)
            message_hold = int(message_hold)
        except ValueError:
            response = {"apiversion": apiversion, "code": 134, "data": {},
                        "message": "Invalid data type - Message Hold must be an integer."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 134)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            return response, 422

        if api_holdlimit is True:
            if message_hold > (unixtime + (api_holdlimit_count * 86400)):
                response = {"apiversion": apiversion, "code": 148, "data": {"messagehold_limit": api_holdlimit_count},
                            "message": "Invalid data type - Message Hold has exceeded the server-side duration limit "
                                       "for message holding."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 148)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                return response, 422

    if twentyfour_hr_prefix is not None:
        twentyfour_hr_prefix = str(twentyfour_hr_prefix)
        if twentyfour_hr_prefix == "on":
            twentyfour_hr_prefix = True
        elif twentyfour_hr_prefix == "off":
            twentyfour_hr_prefix = False
        else:
            response = {"apiversion": apiversion, "code": 135, "data": {},
                        "message": "Invalid data type - Twenty Four Hour Prefix must be a string set to on or off."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 135)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            return response, 422

    if no_prefix is not None:
        no_prefix = str(no_prefix)
        if no_prefix == "on":
            no_prefix = True
        elif no_prefix == "off":
            no_prefix = False
        else:
            response = {"apiversion": apiversion, "code": 136, "data": {},
                        "message": "Invalid data type - No Prefix must be a string set to on or off."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 136)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            return response, 422

    if timezone_in_msg is not None:
        timezone_in_msg = str(timezone_in_msg)
        if timezone_in_msg == "on":
            timezone_in_msg = True
        elif timezone_in_msg == "off":
            timezone_in_msg = False
        else:
            response = {"apiversion": apiversion, "code": 137, "data": {},
                        "message": "Invalid data type - Timezone In Message must be a string set to on or off."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 137)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            return response, 422

    if pihomepage is not None:
        pihomepage = str(pihomepage)
        if pihomepage == "on":
            pihomepage = True
        elif pihomepage == "off":
            pihomepage = False
        else:
            response = {"apiversion": apiversion, "code": 141, "data": {},
                        "message": "Invalid data type - Prediction Info on homepage must be a string set to on or off."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 141)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            return response, 422

    if pdehomepage is not None:
        pdehomepage = str(pdehomepage)
        if pdehomepage == "on":
            pdehomepage = True
        elif pdehomepage == "off":
            pdehomepage = False
        else:
            response = {"apiversion": apiversion, "code": 142, "data": {},
                        "message": "Invalid data type - Prediction Delivery Errors on homepage must be a string set "
                                   "to on or off."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 142)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            return response, 422

    if sshomepage is not None:
        sshomepage = str(sshomepage)
        if sshomepage == "on":
            sshomepage = True
        elif sshomepage == "off":
            sshomepage = False
        else:
            response = {"apiversion": apiversion, "code": 143, "data": {},
                        "message": "Invalid data type - System Status on homepage must be a string set to on or off."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 143)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            return response, 422

    if sqlhomepage is not None:
        sqlhomepage = str(sqlhomepage)
        if sqlhomepage == "on":
            sqlhomepage = True
        elif sqlhomepage == "off":
            sqlhomepage = False
        else:
            response = {"apiversion": apiversion, "code": 144, "data": {},
                        "message": "Invalid data type - Settings Quick Links on homepage must be a string set to on "
                                   "or off."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 144)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            return response, 422

    if outboundNum is not None:
        outboundNum = str(outboundNum)

        if outboundNum not in api_numbers:
            response = {"apiversion": apiversion, "code": 146, "data": {},
                        "message": "Invalid data - Outbound number defined does not exist."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 146)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            return response, 422

        numtable_response = table.query(
            KeyConditionExpression='#num = :num',
            ExpressionAttributeValues={
                ":num": assc_number
            },
            ExpressionAttributeNames={
                "#num": "number"
            }
        )
        old_outboundNum = str(numtable_response['Items'][0]['outboundNum'])

        if old_outboundNum == outboundNum:
            outboundNum = None
        else:
            if api_limitregistrations is True:
                numcapacity_response = table9.scan()
                for i in numcapacity_response['Items']:
                    if str(i['number']) == outboundNum:
                        if int(i['capacity']) >= api_limitregistrations_number / api_numbers_count:
                            response = {"apiversion": apiversion, "code": 147, "data": {},
                                        "message": "Invalid data - The number defined in the request has reached "
                                                   "capacity. Please choose a different number"}
                            response = jsonify(response)
                            response.headers.add("X-SnowDayAPI-Version", apiversion)
                            response.headers.add("X-SnowDayAPI-Code", 147)
                            response.headers.add("Access-Control-Allow-Credentials", "true")
                            response.headers.add('Access-Control-Allow-Origin', cors_url_dashend)
                            return response, 422

    if amhomepage is not None:
        amhomepage = str(amhomepage)
        if amhomepage == "on":
            amhomepage = True
        elif amhomepage == "off":
            amhomepage = False
        else:
            response = {"apiversion": apiversion, "code": 145, "data": {},
                        "message": "Invalid data type - Account Management on homepage must be a string set to on or "
                                   "off."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 145)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            return response, 422

    if dashName is not None:
        try:
            dashName = str(dashName)
            if len(dashName) == 0:
                dashName = "default_dash_name"
        except ValueError:
            response = {"apiversion": apiversion, "code": 138, "data": {},
                        "message": "Invalid data type - Dash Name must be a string."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 138)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            return response, 422

    # Check if Dash Name is 64 characters or up
    if dashName is not None:
        if len(dashName) > 64:
            response = {"apiversion": apiversion, "code": 139, "data": {},
                        "message": "Invalid data - Dash Name must be 64 characters or shorter."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 139)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            return response, 422

    # Sanitize Dash Name from HTML & JS code
    if dashName is not None:
        dashName = re.sub('[.*?^=!:<>${}()|[/]', "", dashName)
        dashName = re.sub(r"(?<=[a-z])\r?\n", "", dashName)

    # One by one go and update the DB
    if group is not None:
        table.update_item(
            Key={
                'number': assc_number
            },
            UpdateExpression='SET #grp = :s',
            ExpressionAttributeValues={
                ":s": group
            },
            ExpressionAttributeNames={
                "#grp": "group"
            },
            ReturnValues="UPDATED_NEW"
        )

    if message_hold is not None:
        table.update_item(
            Key={
                'number': assc_number
            },
            UpdateExpression='SET #hld = :n',
            ExpressionAttributeValues={
                ":n": message_hold
            },
            ExpressionAttributeNames={
                "#hld": "message_hold"
            },
            ReturnValues="UPDATED_NEW"
        )

    if twentyfour_hr_prefix is not None:
        table.update_item(
            Key={
                'number': assc_number
            },
            UpdateExpression='SET #tfp = :b',
            ExpressionAttributeValues={
                ":b": twentyfour_hr_prefix
            },
            ExpressionAttributeNames={
                "#tfp": "24_hr_prefix"
            },
            ReturnValues="UPDATED_NEW"
        )

    if no_prefix is not None:
        table.update_item(
            Key={
                'number': assc_number
            },
            UpdateExpression='SET #np = :b',
            ExpressionAttributeValues={
                ":b": no_prefix
            },
            ExpressionAttributeNames={
                "#np": "no_prefix"
            },
            ReturnValues="UPDATED_NEW"
        )

    if timezone_in_msg is not None:
        table.update_item(
            Key={
                'number': assc_number
            },
            UpdateExpression='SET #tzim = :b',
            ExpressionAttributeValues={
                ":b": timezone_in_msg
            },
            ExpressionAttributeNames={
                "#tzim": "timezone"
            },
            ReturnValues="UPDATED_NEW"
        )

    if pihomepage is not None:
        table.update_item(
            Key={
                'number': assc_number
            },
            UpdateExpression='SET #pih = :b',
            ExpressionAttributeValues={
                ":b": pihomepage
            },
            ExpressionAttributeNames={
                "#pih": "predictioninfo"
            },
            ReturnValues="UPDATED_NEW"
        )

    if pdehomepage is not None:
        table.update_item(
            Key={
                'number': assc_number
            },
            UpdateExpression='SET #pde = :b',
            ExpressionAttributeValues={
                ":b": pdehomepage
            },
            ExpressionAttributeNames={
                "#pde": "predictionde"
            },
            ReturnValues="UPDATED_NEW"
        )

    if sshomepage is not None:
        table.update_item(
            Key={
                'number': assc_number
            },
            UpdateExpression='SET #ssh = :b',
            ExpressionAttributeValues={
                ":b": sshomepage
            },
            ExpressionAttributeNames={
                "#ssh": "systemstatus"
            },
            ReturnValues="UPDATED_NEW"
        )

    if sqlhomepage is not None:
        table.update_item(
            Key={
                'number': assc_number
            },
            UpdateExpression='SET #sqlh = :b',
            ExpressionAttributeValues={
                ":b": sqlhomepage
            },
            ExpressionAttributeNames={
                "#sqlh": "settingslinks"
            },
            ReturnValues="UPDATED_NEW"
        )

    if outboundNum is not None:
        table.update_item(
            Key={
                'number': assc_number
            },
            UpdateExpression='SET #outnum = :b',
            ExpressionAttributeValues={
                ":b": outboundNum
            },
            ExpressionAttributeNames={
                "#outnum": "outboundNum"
            },
            ReturnValues="UPDATED_NEW"
        )

        table9.update_item(
            Key={
                'number': old_outboundNum
            },
            UpdateExpression='SET #cap = #cap - :n',
            ExpressionAttributeValues={
                ":n": 1
            },
            ExpressionAttributeNames={
                "#cap": "capacity"
            }
        )

        table9.update_item(
            Key={
                'number': outboundNum
            },
            UpdateExpression='SET #cap = #cap + :n',
            ExpressionAttributeValues={
                ":n": 1
            },
            ExpressionAttributeNames={
                "#cap": "capacity"
            }
        )

    if amhomepage is not None:
        table.update_item(
            Key={
                'number': assc_number
            },
            UpdateExpression='SET #amh = :b',
            ExpressionAttributeValues={
                ":b": amhomepage
            },
            ExpressionAttributeNames={
                "#amh": "accountmanagement"
            },
            ReturnValues="UPDATED_NEW"
        )

    if dashName is not None:
        table.update_item(
            Key={
                'number': assc_number
            },
            UpdateExpression='SET #dn = :s',
            ExpressionAttributeValues={
                ":s": dashName
            },
            ExpressionAttributeNames={
                "#dn": "name"
            },
            ReturnValues="UPDATED_NEW"
        )
    if (group is None) and (message_hold is None) and (twentyfour_hr_prefix is None) and (no_prefix is None) \
    and (timezone_in_msg is None) and (dashName is None) and (pihomepage is None) and (pdehomepage is None) \
    and (sshomepage is None) and (sqlhomepage is None) and (amhomepage is None) and (outboundNum is None):
        response = {"apiversion": apiversion, "code": 149, "data": {},
                    "message": "No data was requested to be updated. User settings have not been changed."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 149)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        response.set_cookie('sessionid', session_id, httponly=True, max_age=604800, path="/", domain="owenthe.dev")
        return response
    else:
        response = {"apiversion": apiversion, "code": 140, "data": {},
                    "message": "User data updated successfully."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 140)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        response.set_cookie('sessionid', session_id, httponly=True, max_age=604800, path="/", domain="owenthe.dev")
        return response


@app.route("/api/v2/getSessionStats", methods=['GET'])
@limiter.limit(api_limit_getsessionstats)
@shared_limiter
def get_session_stats():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    origin = request.environ.get('HTTP_ORIGIN', None)
    if origin != cors_url_dashend:
        response = {"apiversion": apiversion, "code": 100, "data": {},
                    "message": "Bad origin."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 100)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    session_id = request.cookies.get("sessionid", None)

    if session_id is None:
        response = {"apiversion": apiversion, "code": 129, "data": {},
                    "message": "Session ID not defined. This may be due to cookie expiration or deletion."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 129)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    response = table7.query(
        KeyConditionExpression='#ses = :sess',
        ExpressionAttributeValues={
            ":sess": session_id,
        },
        ExpressionAttributeNames={
            "#ses": "session",
        }
    )

    if len(response['Items']) == 0:
        response = {"apiversion": apiversion, "code": 190, "data": {},
                    "message": "No sessions matched the session ID defined. Try entering a valid session ID."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 190)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    assc_number = None
    for i in response['Items']:
        assc_number = i['number']
        break

    response = table7.scan(
        FilterExpression="#num = :num",
        ExpressionAttributeValues={
            ":num": assc_number
        },
        ExpressionAttributeNames={
            "#num": "number"
        }
    )

    active_sessions_count = 0
    expired_sessions_count = 0
    data_dict = {"active_count": 0, "expired_count": 0, "active_sessions": [], "expired_sessions": []}
    unix_time = int(time.time())
    for i in response['Items']:
        if int(i['expires']) <= unix_time:
            expired_sessions_count = expired_sessions_count + 1
            data_dict['expired_sessions'].append({"sessionid": str(i['session'])[:6], "browser": str(i['browser']),
                                                  "os": str(i['os']), "created": str(i['created']),
                                                  "last_activity": int(i['expires']) - 604800,
                                                  "expires": int(i['expires'])})
            table7.delete_item(
                Key={
                    "session": i['session']
                }
            )
        else:
            active_sessions_count = active_sessions_count + 1
            data_dict['active_sessions'].append({"sessionid": str(i['session'])[:6], "browser": str(i['browser']),
                                                 "os": str(i['os']), "created": str(i['created']),
                                                 "last_activity": int(i['expires']) - 604800,
                                                 "expires": int(i['expires'])})

    data_dict['active_count'] = active_sessions_count
    data_dict['expired_count'] = expired_sessions_count

    response = {"apiversion": apiversion, "code": 0, "data": data_dict,
                "message": "Data returned successfully."}
    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 0)
    response.headers.add("Access-Control-Allow-Credentials", "true")
    response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
    return response


@app.route("/api/v2/terminateAllSessions", methods=['POST'])
@limiter.limit(api_limit_terminateallsessions)
@shared_limiter
def terminate_all_sessions():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    origin = request.environ.get('HTTP_ORIGIN', None)
    if origin != cors_url_dashend:
        response = {"apiversion": apiversion, "code": 100, "data": {},
                    "message": "Bad origin."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 100)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    session_id = request.cookies.get("sessionid", None)

    if session_id is None:
        response = {"apiversion": apiversion, "code": 129, "data": {},
                    "message": "Session ID not defined. This may be due to cookie expiration or deletion."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 129)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    response = table7.query(
        KeyConditionExpression='#ses = :sess',
        ExpressionAttributeValues={
            ":sess": session_id,
        },
        ExpressionAttributeNames={
            "#ses": "session",
        }
    )

    assc_number = None
    for i in response['Items']:
        assc_number = i['number']
        break

    response = table7.scan(
        FilterExpression="#num = :num",
        ExpressionAttributeValues={
            ":num": assc_number
        },
        ExpressionAttributeNames={
            "#num": "number"
        }
    )

    if len(response['Items']) == 0:
        response = {"apiversion": apiversion, "code": 190, "data": {},
                    "message": "No sessions matched the session ID defined. Try entering a valid session ID."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 190)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    for i in response['Items']:
        table7.delete_item(
            Key={
                "session": i['session']
            }
        )

    response = {"apiversion": apiversion, "code": 0, "data": {},
                "message": "All sessions have been terminated successfully."}
    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 0)
    response.headers.add("Access-Control-Allow-Credentials", "true")
    response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
    return response


@app.route("/api/v2/predictionInfo", methods=['GET'])
@limiter.limit(api_limit_predictioninfo)
@shared_limiter
def prediction_info():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    key = request.values.get('key', None)
    detaillevel = request.values.get('detail', None)

    if detaillevel is None:
        detaillevel = 2
    elif (detaillevel < 0) or (detaillevel > 2):
        response = {"apiversion": apiversion, "code": 67, "data": {},
                    "message": "The detail level defined is not within valid range. Valid numbers include 0, 1, or 2."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 67)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 422

    if (key not in api_keys) and (key not in api_keys_predictioninfo):
        response = {"apiversion": apiversion, "code": 54, "data": {},
                    "message": "The API key defined is invalid. Please use a valid API key to use this API."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 403

    response = table8.scan()

    for i in response['Items']:
        if detaillevel == 0:
            response = {"apiversion": apiversion, "code": 0, "data": {
                "source": i['source'],
                "line1": i['pt1-info'], "line2": i['pt2-info'], "line3": i['pt3-info'],
                "lastupdated-line": i['lu-info'],
                "lastupdated-time": int(i['lastupdated'])}, "message": "The data has been returned successfully."}
        elif detaillevel == 1:
            response = {"apiversion": apiversion, "code": 0, "data": {
                "source": i['source'],
                "line1": i['pt1-info'], "line2": i['pt2-info'], "line3": i['pt3-info'], "line4": i['pt4-info'],
                "line5": i['pt5-info'], "line6": i['pt6-info'], "line7": i['pt7-info'],
                "lastupdated-line": i['lu-info'],
                "lastupdated-time": int(i['lastupdated'])}, "message": "The data has been returned successfully."}
        elif detaillevel == 2:
            response = {"apiversion": apiversion, "code": 0, "data": {
                "source": i['source'],
                "line1": i['pt1-info'], "line2": i['pt2-info'], "line3": i['pt3-info'], "line4": i['pt4-info'],
                "line5": i['pt5-info'], "line6": i['pt6-info'], "line7": i['pt7-info'], "lastupdated-line": i['lu-info'],
                "details": i['details'],
                "lastupdated-time": int(i['lastupdated'])}, "message": "The data has been returned successfully."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 0)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response


@app.route("/api/v2/messageErrors", methods=['GET'])
@limiter.limit(api_limit_messageerrors)
@shared_limiter
def message_errors():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    number = request.values.get('number', None)
    key = request.values.get('key', None)
    session_id = request.cookies.get('sessionid', None)

    # Determine authentication type
    if session_id is not None and key is None:
        auth_method = "cookies"
    elif session_id is None and key is not None:
        auth_method = "key"
    elif session_id is None and key is None:
        auth_method = "bad_nonespec"
    elif session_id is not None and key is not None:
        auth_method = "bad_bothspec"

    if (auth_method == "key") and (key not in api_keys):
        response = {"apiversion": apiversion, "code": 54, "data": {},
                    "message": "The API key defined is invalid. Please use a valid API key to use this API."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 403

    # If coming from the SDD, we check to see if session ID is none and key should be none
    if auth_method == "bad_nonespec":
        response = {"apiversion": apiversion, "code": 129, "data": {},
                    "message": "Session ID or API key not defined. This may be due to cookie expiration or "
                               "deletion, or due to bad form data."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 129)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    # Warn if authentication occurs with session ID & key
    if auth_method == "bad_bothspec":
        response = {"apiversion": apiversion, "code": 2, "data": {},
                    "message": "Session ID and API key were both defined. Please only define one authentication "
                               "type for this method."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 2)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    if auth_method == "cookies":
        origin = request.environ.get('HTTP_ORIGIN', None)
        if origin != cors_url_dashend:
            response = {"apiversion": apiversion, "code": 100, "data": {},
                        "message": "Bad origin."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 100)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            return response, 400

    if auth_method == "key":
        response = table4.scan()
        for i in response['Items']:
            if str(i['number']) == str(number):
                if i['errors'] == 0:
                    response_data = {"errors": 0, "details": []}
                elif i['errors'] == 1:
                    response_data = {"errors": 1, "details": [{"time": int(i['me1_time']), "reason": i['me1_reason']}]}
                elif i['errors'] == 2:
                    response_data = {"errors": 2, "details": [{"time": int(i['me1_time']), "reason": i['me1_reason']},
                                     {"time": int(i['me2_time']), "reason": i['me2_reason']}]}
                elif i['errors'] >= 3:
                    response_data = {"errors": int(i['errors']), "details": [{"time": int(i['me1_time']),
                                                                              "reason": i['me1_reason']},
                                     {"time": int(i['me2_time']), "reason": i['me2_reason']},
                                     {"time": int(i['me3_time']), "reason": i['me3_reason']}]}

                response = {"apiversion": apiversion, "code": 0, "data": response_data,
                            "message": "The number error retrieval was successful."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 0)
                response.headers.add("Access-Control-Allow-Origin", cors_url)
                return response

        response = {"apiversion": apiversion, "code": 79, "data": {},
                    "response": "Number invalid."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 79)
        response.headers.add("Access-Control-Allow-Origin", cors_url)
        return response, 400
    elif auth_method == "cookies":
        response = table7.scan()
        unix_time = time.time()
        assc_number = None
        for i in response['Items']:
            if i['session'] == session_id and i['expires'] > unix_time:
                assc_number = i['number']
            elif i['session'] == session_id and i['expires'] <= unix_time:
                table7.delete_item(
                    Key={
                        "session": session_id
                    }
                )

                response = {"apiversion": apiversion, "code": 139, "data": {},
                            "message": "Session ID has expired. Please request a new session before requesting "
                                       "message delivery error data."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 129)
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
                return response, 400

        if assc_number is None:
            response = {"apiversion": apiversion, "code": 116, "data": {},
                        "message": "Session not found. This may be due to an expired session that was deleted from "
                                   "calling another session-based method."}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 116)
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
            return response, 400

        response = table4.scan()
        for i in response['Items']:
            if str(i['number']) == str(assc_number):
                if i['errors'] == 0:
                    response_data = {"errors": 0, "details": []}
                elif i['errors'] == 1:
                    response_data = {"errors": 1, "details": [{"time": int(i['me1_time']), "reason": i['me1_reason']}]}
                elif i['errors'] == 2:
                    response_data = {"errors": 2, "details": [{"time": int(i['me1_time']), "reason": i['me1_reason']},
                                                              {"time": int(i['me2_time']), "reason": i['me2_reason']}]}
                elif i['errors'] >= 3:
                    response_data = {"errors": int(i['errors']),
                                     "details": [{"time": int(i['me1_time']), "reason": i['me1_reason']},
                                                 {"time": int(i['me2_time']), "reason": i['me2_reason']},
                                                 {"time": int(i['me3_time']), "reason": i['me3_reason']}]}

                response = {"apiversion": apiversion, "code": 0, "data": response_data,
                            "message": "The number error retrieval was successful."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 0)
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                return response


@app.route("/api/v2/getUserSettings", methods=['GET'])
@limiter.limit(api_limit_getusersettings)
@shared_limiter
def get_user_settings():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    origin = request.environ.get('HTTP_ORIGIN', None)
    if origin != cors_url_dashend:
        response = {"apiversion": apiversion, "code": 100, "data": {},
                    "message": "Bad origin."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 100)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    session_id = request.cookies.get("sessionid", None)

    if session_id is None:
        response = {"apiversion": apiversion, "code": 129, "data": {},
                    "message": "Session ID not specified. This may be due to cookie expiration or deletion."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 129)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    response = table7.scan()

    valid_response = False
    for i in response['Items']:
        if str(i['session']) == session_id:
            if int(i['expires']) <= int(time.time()):
                table7.delete_item(
                    Key={
                        "session": session_id
                    }
                )

                response = {"apiversion": apiversion, "code": 128, "data": {},
                            "message": "Session has expired. Please request a new session to get user settings."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 128)
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
                return response, 401
            else:
                assc_number = str(i['number'])
                table7.update_item(
                    Key={
                        "session": session_id
                    },
                    UpdateExpression="SET expires = :n",
                    ExpressionAttributeValues={
                        ":n": int(time.time()) + 604800
                    }
                )
                valid_response = True

    if valid_response is False:
        response = {"apiversion": apiversion, "code": 125, "data": {},
                    "message": "Session not found. This may be due to an expired session that was deleted from calling "
                               "another session-based method."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 125)
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
        return response, 400

    # This code will be improved as to not use scan and rather query for better performance.
    response = table.scan()

    for i in response['Items']:
        if str(i['number']) == assc_number:
            twentyfour_prefix = str(i['24_hr_prefix'])
            noprefix = str(i['no_prefix'])
            tzInMsg = str(i['timezone'])
            dashName = str(i['name'])
            pde = str(i['predictionde'])
            predinfo = str(i['predictioninfo'])
            sysstatus = str(i['systemstatus'])
            settinglinks = str(i['settingslinks'])
            accountmanagement = str(i['accountmanagement'])
            dashboxes_bit = 0
            # For the above four boxes we do bit math to represent if they are off or on
            # Bit 0 is for settings links, 1 for sys status, 2 for prediction info, 3 for delivery errors
            # For instance 110 translates to settings links off, sys status on, prediction info on, delivery errors off
            if twentyfour_prefix.lower() == "true":
                twentyfour_prefix = True
            elif twentyfour_prefix.lower() == "false":
                twentyfour_prefix = False

            if noprefix.lower() == "true":
                noprefix = True
            elif noprefix.lower() == "false":
                noprefix = False

            if tzInMsg.lower() == "true":
                tzInMsg = True
            elif tzInMsg.lower() == "false":
                tzInMsg = False

            if pde.lower() == "true":
                pde = True
                dashboxes_bit = dashboxes_bit + 1
            elif pde.lower() == "false":
                pde = False

            if predinfo.lower() == "true":
                predinfo = True
                dashboxes_bit = dashboxes_bit + 10
            elif predinfo.lower() == "false":
                predinfo = False

            if sysstatus.lower() == "true":
                sysstatus = True
                dashboxes_bit = dashboxes_bit + 100
            elif sysstatus.lower() == "false":
                sysstatus = False

            if settinglinks.lower() == "true":
                settinglinks = True
                dashboxes_bit = dashboxes_bit + 1000
            elif settinglinks.lower() == "false":
                settinglinks = False

            if accountmanagement.lower() == "true":
                accountmanagement = True
                dashboxes_bit = dashboxes_bit + 10000
            elif accountmanagement.lower() == "false":
                accountmanagement = True

            if dashName == "default_dash_name":
                dashName = str(i['number'])
                table.update_item(
                    Key={
                        'number': dashName
                    },
                    UpdateExpression='SET #nm = :n',
                    ExpressionAttributeValues={
                        ":n": dashName
                    },
                    ExpressionAttributeNames={
                        "#nm": "name"
                    }
                )

            response = {"apiversion": apiversion, "code": 130,
                        "message": "Data retrival successful.",
                        "data": {"group": str(i['group']), "message_hold": int(i['message_hold']),
                                 "24_hr_prefix": twentyfour_prefix, "no_prefix": noprefix,
                                 "timezone_in_msg": tzInMsg, "dash_name": dashName, "pde": pde,
                                 "predictioninfo": predinfo, "sysstatus": sysstatus, "settinglinks": settinglinks,
                                 "accountmanagement": accountmanagement, "outboundNum": str(i['outboundNum']),
                                 "dashbit": dashboxes_bit}}
            response = jsonify(response)
            response.headers.add("X-SnowDayAPI-Version", apiversion)
            response.headers.add("X-SnowDayAPI-Code", 130)
            response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
            response.headers.add("Access-Control-Allow-Credentials", "true")
            response.set_cookie('sessionid', session_id, httponly=True, max_age=604800, path="/", domain="owenthe.dev")
            return response

    response = {"apiversion": apiversion, "code": "131", "data": {},
                "message": "Internal Server Error: Could not find number associated with session."}
    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 131)
    response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
    response.headers.add("Access-Control-Allow-Credentials", "true")
    response.set_cookie('sessionid', session_id, httponly=True, max_age=604800, path="/", domain="owenthe.dev")
    return response, 500


@app.route("/api/v2/verifyPreAuthSessionStatus", methods=['POST'])
@limiter.limit(api_limit_verifypreauthsessionstatus)
@shared_limiter
def verify_pre_auth_session_status():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    origin = request.environ.get('HTTP_ORIGIN', None)
    if origin != cors_url_dashend:
        response = {"apiversion": apiversion, "code": 100, "data": {},
                    "message": "Bad origin."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 100)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    preauth_sessionid = request.cookies.get("preauth-sessionid", None)

    if preauth_sessionid is None or preauth_sessionid == "":
        response = {"apiversion": apiversion, "code": 129, "data": {},
                    "message": "Pre-Auth session ID not defined. This may be due to cookie expiration or deletion."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 129)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    response = table6.scan()

    for i in response['Items']:
        if str(i['session']) == preauth_sessionid:
            if int(i['expires']) <= int(time.time()):
                table6.delete_item(
                    Key={
                        "session": preauth_sessionid
                    }
                )

                response = {"apiversion": apiversion, "code": 128, "data": {},
                            "message": "Pre-Auth session has expired."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 128)
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                response.set_cookie('preauth-sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
                return response
            else:
                response = {"apiversion": apiversion, "code": 127, "data": {},
                            "message": "Pre-Auth session is valid."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 127)
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                return response

    response = {"apiversion": apiversion, "code": 125, "data": {},
                "message": "Pre-Auth session not found. This may be due to an expired session that was deleted from "
                           "calling another session-based method."}
    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 125)
    response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
    response.headers.add("Access-Control-Allow-Credentials", "true")
    response.set_cookie('preauth-sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
    return response, 400


@app.route("/api/v2/verifySessionStatus", methods=['POST'])
@limiter.limit(api_limit_verifysessionstatus)
@shared_limiter
def verify_session_status():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    origin = request.environ.get('HTTP_ORIGIN', None)
    if origin != cors_url_dashend:
        response = {"apiversion": apiversion, "code": 100, "data": {},
                    "message": "Bad origin."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 100)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    session_id = request.cookies.get("sessionid", None)

    if session_id is None or session_id == "":
        response = {"apiversion": apiversion, "code": 129, "data": {},
                    "message": "Session ID not defined. This may be due to cookie expiration or deletion."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 129)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    response = table7.scan()

    for i in response['Items']:
        if str(i['session']) == session_id:
            if int(i['expires']) <= int(time.time()):
                table7.delete_item(
                    Key={
                        "session": session_id
                    }
                )

                response = {"apiversion": apiversion, "code": 128, "data": {},
                            "message": "Session has expired."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 128)
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
                return response
            else:
                table7.update_item(
                    Key={
                        "session": session_id
                    },
                    UpdateExpression="SET expires = :n",
                    ExpressionAttributeValues={
                        ":n": int(time.time()) + 604800
                    }
                )

                response = {"apiversion": apiversion, "code": 127, "data": {},
                            "message": "Session is valid, and expiry time has been set to 7 days from now."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 127)
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                response.set_cookie('sessionid', session_id, httponly=True, max_age=604800, path="/",
                                    domain="owenthe.dev")
                return response

    response = {"apiversion": apiversion, "code": 125, "data": {},
                "message": "Session not found. This may be due to an expired session that was deleted from calling "
                           "another session-based method."}
    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 125)
    response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
    response.headers.add("Access-Control-Allow-Credentials", "true")
    response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
    return response, 400


@app.route("/api/v2/verifyNewSession", methods=['POST'])
@limiter.limit(api_limit_verifynewsession)
@shared_limiter
def verify_new_session():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    ua = request.headers.get('User-Agent')

    user_agent = user_agent_parser.Parse(ua)

    ua_os_family = user_agent['os']['family']
    ua_os_ver_major = user_agent['os']['major']
    ua_os_ver_minor = user_agent['os']['minor']
    ua_os_ver_patch = user_agent['os']['patch']

    ua_browser_family = user_agent['user_agent']['family']
    ua_browser_ver_major = user_agent['user_agent']['major']
    ua_browser_ver_minor = user_agent['user_agent']['minor']
    ua_browser_ver_patch = user_agent['user_agent']['patch']

    if ua_os_family is None:
        ua_os = "null"
    elif (ua_os_family is not None) and (ua_os_ver_major is None):
        ua_os = ua_os_family
    elif (ua_os_family is not None) and (ua_os_ver_major is not None) and (ua_os_ver_minor is None):
        ua_os = ua_os_family + " " + ua_os_ver_major
    elif (ua_os_family is not None) and (ua_os_ver_major is not None) and (ua_os_ver_minor is not None) \
            and (ua_os_ver_patch is None):
        ua_os = ua_os_family + " " + ua_os_ver_major + "." + ua_os_ver_minor
    else:
        ua_os = ua_os_family + " " + ua_os_ver_major + "." + ua_os_ver_minor + "." + ua_os_ver_patch

    if ua_os == "Android":
        ua_os = "Android 9 or higher"

    if ua_os == "Firefox OS":
        ua_os = "Firefox OS or KaiOS"

    if ua_browser_family is None:
        ua_browser = "null"
    elif (ua_browser_family is not None) and (ua_browser_ver_major is None):
        ua_browser = ua_browser_family
    elif (ua_browser_family is not None) and (ua_browser_ver_major is not None) and (ua_browser_ver_minor is None):
        ua_browser = ua_browser_family + " " + ua_browser_ver_major
    elif (ua_browser_family is not None) and (ua_browser_ver_major is not None) and (ua_browser_ver_minor is not None) \
            and (ua_browser_ver_patch is None):
        ua_browser = ua_browser_family + " " + ua_browser_ver_major + "." + ua_browser_ver_minor
    else:
        ua_browser = ua_browser_family + " " + ua_browser_ver_major + "." + ua_browser_ver_minor + "." + \
                     ua_browser_ver_patch

    origin = request.environ.get('HTTP_ORIGIN', None)
    if origin != cors_url_dashend:
        response = {"apiversion": apiversion, "code": 100, "data": {},
                    "message": "Bad origin."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 100)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    code = request.values.get("code", None)
    preauth_id = request.cookies.get("preauth-sessionid", None)

    if code is None or code == "":
        response = {"apiversion": apiversion, "code": 109, "data": {},
                    "message": "Code field was not defined."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 109)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    if preauth_id is None or preauth_id == "":
        response = {"apiversion": apiversion, "code": 114, "data": {},
                    "message": "Preauth session ID not defined. This may be due to cookie expiration or deletion."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 114)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    response = table6.scan()

    for i in response['Items']:
        if str(i['session']) == preauth_id:
            if int(i['expires']) <= int(time.time()):
                table6.delete_item(
                    Key={
                        "session": preauth_id
                    }
                )
                response = {"apiversion": apiversion, "code": 113, "data": {},
                            "message": "Pre-auth session has expired. Please request a new pre-auth session."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 113)
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                response.set_cookie('sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
                return response, 401

            if str(i['code']) == code:
                number = str(i['number'])
                table6.delete_item(
                    Key={
                        "session": preauth_id
                    }
                )

                new_session_id = uuid.uuid4().hex.replace("-", "")

                table7.put_item(
                    Item={
                        "session": new_session_id,
                        "number": number,
                        "created": int(time.time()),
                        "expires": int(time.time()) + 604800,
                        "browser": ua_browser,
                        "os": ua_os
                    }
                )

                response = {"apiversion": apiversion, "code": 110, "data": {},
                            "message": "Pre-authorization successful."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 110)
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                response.set_cookie('preauth-sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
                response.set_cookie('sessionid', new_session_id, httponly=True, max_age=604800, path="/",
                                    domain="owenthe.dev")
                return response
            else:
                response = {"apiversion": apiversion, "code": 111, "data": {},
                            "message": "Incorrect code. Please try again."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 111)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                return response, 422

    response = {"apiversion": apiversion, "code": 115, "data": {},
                "message": "Pre-Auth Session not found. This may be due to an expired session that was deleted from "
                           "calling another session-based method."}
    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 115)
    response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
    response.headers.add("Access-Control-Allow-Credentials", "true")
    response.set_cookie('preauth-sessionid', "", httponly=True, expires=0, path="/", domain="owenthe.dev")
    return response, 400


@app.route("/api/v2/requestNewAuthCode", methods=['POST'])
@limiter.limit(api_limit_requestnewauthcode)
@shared_limiter
def request_new_auth_code():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    unixtime = int(time.time())

    origin = request.environ.get('HTTP_ORIGIN', None)
    if origin != cors_url_dashend:
        response = {"apiversion": apiversion, "code": 100, "data": {},
                    "message": "Bad origin."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 100)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    preauth_id = request.cookies.get("preauth-sessionid", None)

    if preauth_id is None:
        response = {"apiversion": apiversion, "code": 109, "data": {},
                    "message": "Preauth session ID not defined. This may be due to cookie expiration or deletion."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 109)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    response = table6.scan()

    # TODO: Add preauth-session verification

    for i in response['Items']:
        if str(i['session']) == preauth_id:
            if int(i['expires']) <= int(time.time()):
                table6.delete_item(
                    Key={
                        "session": preauth_id
                    }
                )

                response = {"apiversion": apiversion, "code": 108, "data": {},
                            "message": "Pre-auth session has expired."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 108)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                return response, 401

            if int(i['timeuntilnextcode']) >= int(time.time()):
                response = {"apiversion": apiversion, "code": 112, "data":
                            {"time_to_wait": int(int(i['timeuntilnextcode']) - int(time.time()))},
                            "message": "Please wait another %s seconds before requesting a new verification code."
                            % str(int(i['timeuntilnextcode']) - int(time.time()))}

                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 112)
                time_to_wait = int(int(i['timeuntilnextcode']) - int(time.time()))
                response.headers.add("Retry-After", time_to_wait)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                return response, 429
            else:
                new_code = random.randint(100000, 999999)
                number = str(i['number'])
                outboundNum = str(i['outboundNum'])
                try:
                    message = client.messages.create(
                        body="%s is your Snow Day Dashboard code." % new_code,
                        from_=outboundNum,
                        to=number
                    )
                except twilio.base.exceptions.TwilioRestException:
                    response = {"apiversion": apiversion, "code": 107, "data": {},
                                "message": "Failed to send verification code due to a Twilio error."}
                    response = jsonify(response)
                    response.headers.add("X-SnowDayAPI-Version", apiversion)
                    response.headers.add("X-SnowDayAPI-Code", 107)
                    response.headers.add("Access-Control-Allow-Credentials", "true")
                    response.headers.add('Access-Control-Allow-Origin', cors_url_dashend)
                    return response, 400

                table6.update_item(
                    Key={
                        'session': preauth_id,
                    },
                    UpdateExpression='SET code = :n',
                    ExpressionAttributeValues={
                        ":n": new_code
                    }
                )

                table6.update_item(
                    Key={
                        'session': preauth_id,
                    },
                    UpdateExpression='SET timeuntilnextcode = :n',
                    ExpressionAttributeValues={
                        ":n": int(time.time()) + 300
                    }
                )

                response = {"apiversion": apiversion, "code": 119, "data": {},
                            "message": "New confirmation code successfully sent."}
                response = jsonify(response)
                response.headers.add("X-SnowDayAPI-Version", apiversion)
                response.headers.add("X-SnowDayAPI-Code", 119)
                response.headers.add("Access-Control-Allow-Credentials", "true")
                response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
                # TODO: Add cookie
                return response

    response = {"apiversion": apiversion, "code": 115, "data": {},
                "message": "Session not found. This may be due to an expired session that was deleted from calling "
                           "another session-based method."}
    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 115)
    response.headers.add("Access-Control-Allow-Credentials", "true")
    response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
    # TODO: Add cookie
    return response, 400


@app.route("/api/v2/requestNewSession", methods=['POST'])
@limiter.limit(api_limit_requestnewsession)
@shared_limiter
def request_new_session():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    unixtime = int(time.time())

    key = request.values.get("key", None)

    if (key not in api_keys) and (key not in api_keys_requestnewsession):
        response = {"apiversion": apiversion, "code": 54, "data": {},
                    "message": "The API key defined is invalid. Please use a valid API key to use this API."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 403

    origin = request.environ.get('HTTP_ORIGIN', None)
    if origin != cors_url_dashend:
        response = {"apiversion": apiversion, "code": 100, "data": {},
                    "message": "Bad origin."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 100)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    number = request.values.get("number", None)
    rawnumber = number

    if number is None or number == "":
        response = {"apiversion": apiversion, "code": 109, "data": {},
                    "message": "Number field was not defined."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 109)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    number = number.replace("-", "").replace("(", "").replace(")", "").replace(" ", "").replace("+", "")
    number = number.replace('\n', "").replace('\r', "").replace('\t', "").replace("  ", "")

    if number[0] == "1":
        number = number[1:]

    number = "+1" + number

    if len(number) != 12:
        response = {"apiversion": apiversion, "code": 5, "data": {},
                    "message": "The number defined is not of proper length. Make sure an area code is included."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 5)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add('Access-Control-Allow-Origin', cors_url_dashend)
        return response, 422

    tempdata = number[:5]
    tempdata = tempdata[2:]

    try:
        tempdata = int(tempdata)
    except ValueError:
        response = {"apiversion": apiversion, "code": 159, "data": {},
                    "message": "The number defined is invalid. Make sure that the phone number defined has "
                               "actual numbers, other than (, ), -, a space, and +."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 159)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add('Access-Control-Allow-Origin', cors_url_dashend)
        return response, 422

    if tempdata not in valid_codes:
        response = {"apiversion": apiversion, "code": 162, "data": {},
                    "message": "The number defined is invalid as it does not have an area code based in the "
                               "mainland US, Alaska, or Hawaii."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 162)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add('Access-Control-Allow-Origin', cors_url_dashend)
        return response, 422

    response = table.scan()
    number_in_table = False
    outboundNum = "+18452456252"
    for i in response['Items']:
        if str(i['number']) == number:
            number_in_table = True
            outboundNum = str(i['outboundNum'])

    if number_in_table is False:
        response = {"apiversion": apiversion, "code": 105, "data": {},
                    "message": "Number is invalid."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 105)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 400

    response = table6.scan(
        FilterExpression="#num = :num",
        ExpressionAttributeNames={
            "#num": "number"
        },
        ExpressionAttributeValues={
            ":num": number
        }
    )

    sessions_count = 0

    for i in response['Items']:
        if int(i['expires']) > unixtime:
            sessions_count = sessions_count + 1

    if sessions_count >= 10:
        response = {"apiversion": apiversion, "code": 106, "data": {},
                    "message": "The number defined has reached the maximum amount of pre-auth sessions. Please wait "
                               "for a pre-auth session to expire, or authenticate a pre-auth session."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 106)
        response.headers.add("Retry-After", 3600)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
        return response, 429

    session_id = uuid.uuid4().hex.replace("-", "")

    code = random.randint(100000, 999999)

    try:
        message = client.messages.create(
            body="%s is your Snow Day Dashboard code." % code,
            from_=outboundNum,
            to=number
        )
    except twilio.base.exceptions.TwilioRestException:
        response = {"apiversion": apiversion, "code": 107, "data": {},
                    "message": "Failed to send verification code due to a Twilio error."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 107)
        response.headers.add("Access-Control-Allow-Credentials", "true")
        response.headers.add('Access-Control-Allow-Origin', cors_url_dashend)
        return response, 400

    table6.put_item(
        Item={
            'session': session_id,
            'number': number,
            'expires': int(time.time()) + 1800,
            'timeuntilnextcode': int(time.time()) + 300,
            'code': code,
            'outboundNum': outboundNum
        }
    )

    response = {"apiversion": apiversion, "code": 104, "data": {},
                "message": "New session requested successfully."}
    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 104)
    response.headers.add("Access-Control-Allow-Credentials", "true")
    response.headers.add("Access-Control-Allow-Origin", cors_url_dashend)
    response.set_cookie('preauth-sessionid', value=session_id, httponly=True, max_age=604800, domain="owenthe.dev")
    return response


@app.route("/api/v2/numberCapacity", methods=['GET'])
@limiter.limit(api_limit_numbercapacity)
@shared_limiter
def numbercapacity():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    key = request.values.get('key', None)
    if (key not in api_keys) and (key not in api_keys_numbercapacity):
        response = {"apiversion": apiversion, "code": 54, "data": {},
                    "message": "The API key defined is invalid. Please use a valid API key to use this API."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 403

    response = {"apiversion": apiversion, "code": 0, "data": {}, "message": "Data returned successfully."}
    numcap_response = table9.scan()

    for i in numcap_response['Items']:
        capacity = int(i['capacity'])
        if api_limitregistrations is True:
            max = int(api_limitregistrations_number / api_numbers_count)
            percent = float((capacity / max) * 100)
        else:
            max = 9999999
            percent = float((capacity / max) * 100)

        response['data']['%s' % str(i['number'])] = {"capacity": capacity, "max": max, "percent": percent,
                                                     "percent_rounded": round(percent, 2)}

    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 0)
    response.headers.add('Access-Control-Allow-Origin', cors_url)
    return response


@app.route("/api/v2/banNumber", methods=['POST'])
@limiter.limit(api_limit_bannumber)
@shared_limiter
def bannumber():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    key = request.values.get('key', None)
    number = request.values.get('number', None)
    reason = request.values.get('reason', None)

    if key not in api_keys_bannumber:
        response = {"apiversion": apiversion, "code": 54, "data": {},
                    "message": "The API key defined is invalid. Please use a valid API key to use this API."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 403

    if number is None:
        response = {"apiversion": apiversion, "code": 40, "data": {},
                    "message": "Number is not defined. A number must be defined and formatted properly as a +1 number "
                               "with area code and no extra characters."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 40)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 400

    if (len(str(number)) != 12) and ("+" not in number):
        response = {"apiversion": apiversion, "code": 41, "data": {},
                    "message": "The number defined is invalid. Make sure it is formatted properly as a +1 number "
                               "with area code and no extra characters."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 41)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 422

    if reason is None:
        response = {"apiversion": apiversion, "code": 42, "data": {},
                    "message": "Reason is not defined. A reason must be defined and between 1 and 64 characters long."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 42)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 400

    if reason == "":
        response = {"apiversion": apiversion, "code": 43, "data": {},
                    "message": "Reason defined is 0 characters long. A reason must be defined and between 1 and 64 "
                               "characters long."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 43)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 422

    if len(reason) > 64:
        response = {"apiversion": apiversion, "code": 44, "data": {},
                    "message": "Reason defined is over 64 characters long. Reason must be between 1 and 64 characters."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 44)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 422

    response = table10.query(
        KeyConditionExpression="#num = :num",
        ExpressionAttributeValues={
            ":num": number
        },
        ExpressionAttributeNames={
            "#num": "number"
        }
    )

    if len(response['Items']) >= 1:
        response = {"apiversion": apiversion, "code": 45, "data": {},
                    "message": "The number defined has been already banned."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 45)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 400

    response_num = table.query(
        KeyConditionExpression="#num = :num",
        ExpressionAttributeValues={
            ":num": number
        },
        ExpressionAttributeNames={
            "#num": "number"
        }
    )

    print(response_num['Items'])

    table.delete_item(
        Key={
            "number": number
        }
    )

    table2.delete_item(
        Key={
            'number': number
        }
    )

    try:
        table3.delete_item(
            Key={
                'number': number
            }
        )
    except Exception:
        pass

    try:
        table4.delete_item(
            Key={
                'number': number
            }
        )
    except Exception:
        pass

    try:
        response = table7.scan(
            FilterExpression="#num = :num",
            ExpressionAttributeValues={
                ":num": number
            },
            ExpressionAttributeNames={
                "#num": "number"
            }
        )

        for i in response['Items']:
            table7.delete_item(
                Key={
                    "session": str(i['session'])
                }
            )
    except Exception:
        pass

    try:
        response = table6.scan(
            FilterExpression="#num = :num",
            ExpressionAttributeValues={
                ":num": number
            },
            ExpressionAttributeNames={
                "#num": "number"
            }
        )

        for i in response['Items']:
            table6.delete_item(
                Key={
                    "session": str(i['session'])
                }
            )
    except Exception:
        pass

    if len(response_num['Items']) == 1:
        outboundNum = str(response_num['Items'][0]['outboundNum'])
        table9.update_item(
            Key={
                'number': outboundNum
            },
            UpdateExpression='SET #cap = #cap - :n',
            ExpressionAttributeValues={
                ":n": 1
            },
            ExpressionAttributeNames={
                "#cap": "capacity"
            }
        )

        table10.put_item(
            Item={
                "number": number,
                "reason": reason
            }
        )

        try:
            message = client.messages.create(
                body="(Snow Day Predictor): Your number has been banned from receiving prediction texts. Reason: %s"
                     % reason,
                from_=outboundNum,
                to=number
            )
        except twilio.base.exceptions.TwilioRestException:
            pass
    else:
        table10.put_item(
            Item={
                "number": number,
                "reason": reason
            }
        )

    response = {"apiversion": apiversion, "code": 0, "data": {},
                "message": "The number defined has been banned successfully."}
    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 0)
    response.headers.add('Access-Control-Allow-Origin', cors_url)
    return response


@app.route("/api/v2/unbanNumber", methods=['POST'])
@limiter.limit(api_limit_unbannumber)
@shared_limiter
def unbannumber():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    key = request.values.get('key', None)
    number = request.values.get('number', None)

    if key not in api_keys_unbannumber:
        response = {"apiversion": apiversion, "code": 54, "data": {},
                    "message": "The API key defined is invalid. Please use a valid API key to use this API."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 54)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 403

    if number is None:
        response = {"apiversion": apiversion, "code": 40, "data": {},
                    "message": "Number is not defined. A number must be defined and formatted properly as a +1 number "
                               "with area code and no extra characters."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 40)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 400

    if (len(str(number)) != 12) and ("+" not in number):
        response = {"apiversion": apiversion, "code": 42, "data": {},
                    "message": "The number defined is invalid. Make sure it is formatted properly as a +1 number "
                               "with area code and no extra characters."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 42)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 422

    response = table10.query(
        KeyConditionExpression="#num = :num",
        ExpressionAttributeValues={
            ":num": number
        },
        ExpressionAttributeNames={
            "#num": "number"
        }
    )

    if len(response['Items']) >= 1:
        table10.delete_item(
            Key={
                "number": number
            }
        )

        response = {"apiversion": apiversion, "code": 0, "data": {},
                    "message": "The number defined has been successfully unbanned."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 0)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response
    else:
        response = {"apiversion": apiversion, "code": 41, "data": {},
                    "message": "The number defined is not banned."}
        response = jsonify(response)
        response.headers.add("X-SnowDayAPI-Version", apiversion)
        response.headers.add("X-SnowDayAPI-Code", 41)
        response.headers.add('Access-Control-Allow-Origin', cors_url)
        return response, 400


@app.route("/api/v2/ping", methods=['GET'])
@limiter.limit(api_limit_ping)
@shared_limiter
def pingpong():
    if apiclosed is True:
        response = apiclosed_check()
        return response, 503

    response = {"apiversion": apiversion, "code": 0, "data": {},
                "message": "Pong!"}
    response = jsonify(response)
    response.headers.add("X-SnowDayAPI-Version", apiversion)
    response.headers.add("X-SnowDayAPI-Code", 0)
    return response


@app.route("/docs", methods=['GET'])
@limiter.limit(api_limit_docs)
def docsresponse():
    return render_template('docs.html')


@app.route("/", methods=['GET'])
@limiter.limit(api_limit_index)
def defaultresponse():
    if apiclosed is False:
        return render_template('homepage.html', apiver=apiversion_nov, docs_url=api_homepage_docsurl,
                               status_url=api_homepage_statusurl, source_url=api_homepage_sourceurl)
    elif apiclosed is True:
        return render_template('homepage_maintenance.html', apiver=apiversion_nov, docs_url=api_homepage_docsurl,
                               status_url=api_homepage_statusurl, source_url=api_homepage_sourceurl)


if __name__ == "__main__":
    app.run(threaded=True)
