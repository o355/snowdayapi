# Snow Day API - Predictor Page Parser
# (c) 2019-2020 o355
# This code is licensed under the MIT License. Please see LICENSE for more information.

# This code relies on the config file. Make sure that config.ini is present in the relative directory where
# sdparser.py is being stored in.

import requests
from bs4 import BeautifulSoup
import sys
import boto3
import time
import configparser

config = configparser.ConfigParser()
config.read("config.ini")

access_key_aws = config.get('KEYS', 'aws_access')
secret_key_aws = config.get('KEYS', 'aws_secret')
region_aws = config.get('KEYS', 'aws_region')

# Configure the URL that the parser script should get for parsing. This should be a fully formatted URL path, ending in
# .html. It shoild include https:// at the start as well.
url = ""

unixtime = int(time.time())

session = boto3.Session(aws_access_key_id=access_key_aws, aws_secret_access_key=secret_key_aws, region_name=region_aws)
db = session.resource('dynamodb')
table = db.Table('PredictionInfo')

try:
    webpage = requests.get(url)
except:
    print("Failed to get webpage, try again later")
    sys.exit()

webpage = webpage.text

# Parse file with BS4

soup = BeautifulSoup(webpage, 'html.parser')

# Start replacing text

percenttext1_element = soup.find(id="predictiontext-1")
pt1str = percenttext1_element.string

percenttext2_element = soup.find(id="predictiontext-2")
pt2str = percenttext2_element.string

percenttext3_element = soup.find(id="predictiontext-3")
pt3str = percenttext3_element.string

percenttext4_element = soup.find(id="predictiontext-4")
pt4str = percenttext4_element.string

percenttext5_element = soup.find(id="predictiontext-5")
pt5str = percenttext5_element.string

percenttext6_element = soup.find(id="predictiontext-6")
pt6str = percenttext6_element.string

percenttext7_element = soup.find(id="predictiontext-7")
pt7str = percenttext7_element.string

percenttext8_element = soup.find(id="lastupdated")
pt8str = percenttext8_element.string

details_element = soup.find(id="details")
detailsstr = details_element.text
detailsstr = detailsstr.replace("            ", "")
print(detailsstr)

table.put_item(
    Item={
        'pt1-info': str(pt1str),
        'pt2-info': str(pt2str),
        'pt3-info': str(pt3str),
        'pt4-info': str(pt4str),
        'pt5-info': str(pt5str),
        'pt6-info': str(pt6str),
        'pt7-info': str(pt7str),
        'source': str(url),
        'lu-info': str(pt8str),
        'lastupdated': unixtime,
        'details': str(detailsstr)
    }
)