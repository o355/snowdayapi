# Snow Day API Session Pruner
# (c) 2019-2020 o355
# This code is licensed under the MIT License. Please see LICENSE for more information.

import boto3
import time
import logging
from logging.handlers import RotatingFileHandler
import configparser

config = configparser.ConfigParser()
config.read("config.ini")

# Configure where the prunelog should be stored here. This script uses the configuration file, so make sure that this
# script has config.ini in its relative path.
# Something like /root/prunelog.log is an acceptable path for this variable.
# The end of this string should always have prunelog.log.
prunelog_path = ""

access_key_aws = config.get('KEYS', 'aws_access')
secret_key_aws = config.get('KEYS', 'aws_secret')
region_aws = config.get('KEYS', 'aws_region')

logger = logging.getLogger("SessionPruner")
logger.setLevel(logging.DEBUG)
logging.basicConfig(format='%(asctime)s %(message)s')
logger.propagate = False

handler = RotatingFileHandler(prunelog_path, backupCount=10)
handler.setLevel(logging.DEBUG)
handler.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
logger.addHandler(handler)

handler.doRollover()

session = boto3.Session(aws_access_key_id=access_key_aws, aws_secret_access_key=secret_key_aws, region_name=region_aws)

db = session.resource('dynamodb')
table = db.Table('Sessions_Prod')
table2 = db.Table('SessionsActual_Prod')
table3 = db.Table('PendingNumbers_Prod')

unixtime = int(time.time())

response = table.scan()

pruned_preauth = 0
pruned_sessions = 0
pruned_confnumbers = 0

for i in response['Items']:
    if int(i['expires']) + 604800 <= unixtime:
        table.delete_item(
            Key={
                "session": str(i['session'])
            }
        )
        pruned_preauth = pruned_preauth + 1

logger.info("Pruned %s preauth sessions" % pruned_preauth)

response = table2.scan()

for i in response['Items']:
    if int(i['expires']) + 604800 <= unixtime:
        table2.delete_item(
            Key={
                "session": str(i['session'])
            }
        )
        pruned_sessions = pruned_sessions + 1

logger.info("Pruned %s sessions" % pruned_sessions)

response = table3.scan()

for i in response['Items']:
    if int(i['created']) + 259200 <= unixtime:
        table3.delete_item(
            Key={
                "number": str(i['number'])
            }
        )
        pruned_confnumbers = pruned_confnumbers + 1

logger.debug("Pruned %s pending confirmation numbers" % pruned_confnumbers)
logger.info("Script complete.")