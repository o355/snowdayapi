# Snow Day API - Key Generator
# (c) 2019-2020 o355
# This code is licensed under the MIT License. Please see LICENSE for more information.

try:
    from secrets import token_hex
    secrets_avail = True
except ImportError:
    import uuid
    secrets_avail = False
    print("Unable to use secrets library to generate keys, defaulting on UUID library. Please upgrade to Python 3.6 "
          "for more securely generated keys.")

import sys

print("Welcome to the API key generator for the Snow Day API. This script generates 25-byte hex keys for use with the "
      "Snow Day API.")
print("If you'd like to generate keys that are less than 25 bytes in length please modify the source code.")
print("How many keys would you like to generate (up to 32)?")
number = input("Input here: ")

try:
    number = int(number)
except ValueError:
    print("You didn't input a valid integer. Please try again.")
    sys.exit()

if number <= 0 or number > 32:
    print("Please choose a number of keys to generate that is greater than 0, and less than or equal to 32.")
    sys.exit()

keys = []
for i in range(1, number + 1):
    if secrets_avail is True:
        keys.append(token_hex(25))
    else:
        key = uuid.uuid4().hex + uuid.uuid4().hex
        key = key[:50]

keys = str(keys)
keys = keys.replace("[", "").replace("]", "").replace("'", "")
print("Done! Copy & paste the keys below into a key configuration variable in config.ini.")
print("If you already have more than 1 key, make sure to append a comma between the last key in the array and first "
      "new key.")
print("")
print(keys)
sys.exit()
