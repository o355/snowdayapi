# Snow Day API - Resubscribe Script
# (c) 2019-2020 o355
# This code is licensed under the MIT License. Please see LICENSE for more information.

# This code reads from a repository that had all the numbers of people who wanted to carry over subscriptions from
# one year to the next (Numbers2020_Prod). If you want to use this script to pragmatically subscribe people, make a
# database with the name Numbers2020_Prod, primary key 'numbers' (type String), no sort key, and put numbers that you
# want to subscribe in this format: +15554441234 (country code, area code, full number with no extra characters)

# Make sure you put this script where config.ini is in the same relative directory as this script. It relies on
# config.ini for reading the AWS Access/Secret Keys

import boto3
import requests
import configparser
import time

# Configures if a dry run should be completed. If dry run is on, then the script will fully run (good for testing), but
# not actually unsubscribe anyone.
dryrun = False

# Configures the API key used to register numbers.
apikey = ""

# Configures if the special message query value will be set to True or not.
specialmsg_val = False

# Configures the URL for the path to /api/v2/registerNumber. Include the full hostname of your server when doing this.
# The path should have https:// included at the front
registernumber_path = ""

config = configparser.ConfigParser()
config.read("config.ini")

access_key_aws = config.get('KEYS', 'aws_access')
secret_key_aws = config.get('KEYS', 'aws_secret')
region_aws = config.get('KEYS', 'aws_region')

session = boto3.Session(aws_access_key_id=access_key_aws, aws_secret_access_key=secret_key_aws, region_name=region_aws)
db = session.resource('dynamodb')
table = db.Table('Numbers2020_Prod')

response = table.scan()

responselength = len(response['Items'])
counter = 0
if dryrun is True:
    print("- - - - - - - - - - - - - - - -")
    print("DRY RUN ENABLED. NO REQUESTS TO THE API WILL BE MADE.")
    time.sleep(2)
print("- - - - - - - - - - - - - - - -")
for i in response['Items']:
    counter = counter + 1
    print("Subscribing number %s/%s - %s" % (counter, responselength, str(i['number'])))
    if str(i['userresp']) == "Y":
        # If you want to resubscribe people from a specific outbound number (useful with 2+ numbers)
        # add "outboundNum" to the payload, with one of your Twilio numbers as a String for the value.
        payload = {"key": apikey,
                   "number": str(i['number']),
                   "specialMsg": specialmsg_val}
        print("Sending request...")
        if dryrun is False:
            requestresponse = requests.post(registernumber_path, data=payload)
            try:
                jsonreturn = requestresponse.json()
                if str(jsonreturn['code']) == "0":
                    print("Request successful. Error Code: 0. Message was: %s" % str(jsonreturn['message']))
                    print("Continuing onto next number.")
                    print("- - - - - - - - - - - - - - - -")
                else:
                    print("Request unsuccessful. Error Code: %s. Message was: %s" % (str(jsonreturn['code']),
                                                                                     str(jsonreturn['message'])))
                    print("Continuing onto next number.")
                    print("- - - - - - - - - - - - - - - -")
            except ValueError:
                print("Request unsuccessful. No JSON was returned, response was: %s" % requestresponse.text)
                print("Continuing onto next number.")
                print("- - - - - - - - - - - - - - - -")
        elif dryrun is True:
            time.sleep(1.5)
            print("Request 'successful'. Error Code: N/A. Message was: N/A")
            print("Continuing onto next number.")
            print("- - - - - - - - - - - - - - - -")
    elif str(i['userresp']) == "N":
        print("User responded No to resubscribing. Continuing onto next number.")
        print("- - - - - - - - - - - - - - - -")
    else:
        print("Unknown response from user. Continuing onto next number.")
        print("- - - - - - - - - - - - - - - -")

print("No more numbers to subscribe. Script complete.")
