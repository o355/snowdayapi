# Snow Day API

Python/Flask-based API to serve the Snow Day SMS Service & Dashboard. Integrates with Twilio for SMS & DynamoDB for storage.

This is the most complicated of the Snow Day series of projects, so I will be open sourcing the repository in small steps.

# Spaghet-o-meter
Projects made before late 2020 generally include more spaghetti code, and less object-oriented and efficient code. This is a flaw from how I wrote code - usually focusing on development speed rather than future maintainability. Each project I have now gets a rating from 1 to 10 about the spaghettiness of the code.

Snow Day API Spaghet-o-meter: **9.5/10**

The Snow Day API contains a lot of spaghetti code. Modularity is present in the config file and in the separate scripts for pruning sessions & parsing web information, but that's as far as it goes. All code is wrapped into one massive serve.py file, instead of being divided into separate files to handle individual API requests. Every method has copypasta code for authentication - which should have been achieved via wrappers. Finalizing responses also contains a massive amount of copypasta, which definitely should have been dealt with using global functions inside of separate files to construct a response. Writing/deleting to DynamoDB should have also been handled by a global function for better readability & less copypasta. Table variable names are non-descriptive.

Code maintainability is all around poor, with any major changes to low-level API functions requiring edits to dozens of copypasta code. 

# Important Note
The Snow Day API was built in the US, and was designed to work with +1 numbers. If you are using this API for numbers that don't have a +1 prefix, you will need to make relatively extensive source code modifications!

# Getting started
To get started with the Snow Day API, you'll need:
* A computer
* An installation of Python 3
* A web server that has the ability to host WSGI (Python) modules. You can also use the internal Flask server, but for production purposes this is not recommended.
* A Twilio account (Trial mode can be on), with at least one phone number that has support for SMS & Calling
* An AWS account (it can be free tier) that has access to creating DynamoDB repositories
* These Python 3 libraries: flask, flask_limiter, twilio, boto3, beautifulsoup4, and ua_parser. You can install them by running `pip3 install flask flask-limiter boto3 twilio ua-parser beautifulsoup4`
* A Redis server for the rate limiting side of things. It should be a localhost server.
* Some sort of Cron system that will allow you to run scripts at certain times of the day

To get started, download this entire repository. You can use Git Clone or download with the download button as a .zip.

If you are using a separate web server, make sure that you unzip the directories into where the web server will host files. At that point, you'll need to configure your web server (or a virtual host on a web server) to host the Snow Day API. Otherwise, you can use Flask's built in server if you're tinkering with the project. 

If you are tinkering with the API, you'll want to temporarily port forward your instance of the API via your router. Do it on a randomly generated port in the 30000-40000 part range, and check if there are preexisting conflicts. You can also spin up a temporary VPS as well. **DO NOT** use ngrok! It will work, but ngrok has numerous security issues, and can compromise your machine.

serve.py is the main script for running the Snow Day API. It is a Flask web application. This is the script that you want to initially focus on.

If you run a web server with the Snow Day API, make sure you run it as a daemon (with application group and everything). Not doing this will result in API response times of up to 1 second on slower machines, compared to ~100ms when running in daemon mode (except for the first API call).

At this point, you'll need to set up Twilio & AWS DynamoDB for the SMS number, and for data storage.

When setting up Twilio, set up an account, and get a number that you can use. If you can, turn off trial mode. Take note of your Twilio SID & Token, you will need these later.

Head into the phone numbers section of the console, and click on the phone number (or the first phone number that you have if you have multiple). Keep this tab open.

For AWS DynamoDB configuration, you'll need to configure ten databases. To save space in the readme, details are available in the wiki.

Once DynamoDB configuration is complete, take note of your AWS Access & Secret tokens, along with your AWS region code (such as `us-east-2`). Information about which region codes correspond with certain regions can be found online.

You'll now want to head into the Wiki, and read the entirety of Configuration Variables, and configure config.ini while reading this. Instructions about generating API keys are embedded in the Configuration Variables document, and in a separate page called "Generating API keys".

Once that's all done, you'll need to configure the webhook URLs for Twilio to point to your server. For inbound voice calls, you should have Twilio hook into `https://yourservername.com:portnumberifitsnot443/api/v2/inboundVoice?key=a_key_in_APIKEYS/KEYS_in_the_config_file` using HTTP POST.

(use http:// if you're on a local server. Twilio doesn't accept self-signed HTTPS as far as I know)

For instance, an inbound webhook for voice using the server example.com should be:

`https://example.com/api/v2/inboundVoice?key=1234abcd`

The same process repeats for inbound SMS messages. Have your server hook into the same URL as above, replacing `Voice` with `SMS`. The key can stay the same.

For instance, an inbound webhook for SMS using the server example.com should be:

`https://example.com/api/v2/inboundSMS?key=1234abcd`

Once the webhooking is done, you should now be able to text or call the number you provided, and the API should be able to return something.

# Further Setup
There are plenty of further steps to take to get the Snow Day API fully up and running.

## Setting up a session pruner
The Snow Day API includes a session pruner in the repository, which is meant to be executed once per day. In my case, it was executed every day at 3:30 AM ET.

`prunesessions.py` is meant to prune pre-authentication and normal sessions that are older than 7 days for the Snow Day Dashboard, and pending numbers that have had no action taken after 3+ days.

To get it working, you'll need to put in your AWS Access & Secret Keys, along with defining where to store the prunesessions log, since it does log how many pre-auth sessions, normal sessions, and pending numbers it prunes.

Run `prunesessions.py` to make sure the script works as intended. Once that's done, set the script on a daily cron timer (or any other system that can pragmatically run scripts).

## Setting up the Snow Day Parser
At this point, you'll need to have a server running the snow day predictor from https://gitlab.com/o355/snowday so that the /api/v2/predictionInfo endpoint works properly.

`sdparser.py` is the script that will handle automatically collecting data from the predictor page, and pushing it to DynamoDB.

In `sdparser.py`, you'll want to configure your AWS access & secret keys, along with the URL that the script should fetch. Once that's complete, running sdparser.py should populate the PredictionInfo database.

Make sure you put `sdparser.py` on a cron timer (or any other system that can pragmatically run scripts), set to 5-15 minutes between script runs for best results. Generally, the longer between when the script runs, the higher the potential longest delay for when the API takes note of the latest update to the website.

## Setting up your copy of the Snow Day Predictor to accept signups from your copy of the API
You'll also likely want to set up the predictor homepage to accept signups to the SMS Service. You can find the source code repository at https://gitlab.com/o355/snowday. You'll need to configure some variables in /js/regnumber.js, such as including the URL of the API, and an API Key from APIKEYS/keys_registernumber.

## Setting up your own copy of the Snow Day Dashboard
You'll also probably want to get a copy of the Snow Day Dashboard running as well. For that, you'll want to download the Snow Day Dashboard repository at https://gitlab.com/o355/snowdaydash. In each of the .html files, there are URLs to configure for the Snow Day API, along with inputting API keys, and other misc. information.

## Setting up the Snow Day Tools repository
If you'd like, you can also set up a copy of the Snow Day Tools repository, which are web-based tools that can help to manage the repository. You can find the repo for this at https://gitlab.com/o355/snowdaytools.

# Issues? Bugs?
This documentation has flaws, and probably isn't written super well. I wrote the documentation with the expectation that you know what you're doing, and can Google problems that you might encounter. If you come across any issues during the setup process, or if documentation needs to be clarified, please feel free to submit an issue, or make a PR that corrects the documentation. I'll be more than happy to help!

If there are any issues, or if you have any questions about the documentation in the Wiki, feel free to submit an issue.

# License
The Snow Day API is licensed under the MIT License.
