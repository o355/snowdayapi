This is an old version of the API documentation, and should not be referenced. Please see the updated API documentation in /docs for full information. It is here for historical preservation purposes.

----------

**Last updated: 6/11/2019 | For API version: v1.6.0**

The Snow Day API is a mostly RESTful API that can be run on any server supporting Python 3 & WSGI. It is up to you whether you want your API to be public or not. In reality, you can use this API for a lot more than sending out Snow Day Predictions, this is a lightweight and minimal system for sending out SMS through Twilio.

Data is communicated to the API via. web form parameters.

For all the examples below, the URL of the API is `https://snowdayapi.owenthe.ninja`. This is where the master copy of the API is stored.

## Prelude: Response codes
All returns from the API starting with v2.0.0 will return JSON data with an error code. The only exception is where the API is replying to a message using TwiML, and these special responses are noted in the documentation.

Standard JSON API response:

| JSON key | Type | Description |
|----------|------|-------------|
| code | int | The error code of the action. Regardless of configured settings an API error code will always be sent back. |
| apiversion | string | The version of the Snow Day API that is running on the server you requested. You may not see this response depending on if this option is turned off or not. |
| documentation | string | The URL of the documentation for the API. By default, it's set to this page, but it may be set to a different page. |
| message | string | An English message describing what happened with a request. These messages are usually quite short, more detailed error code information is available in the API Error Codes & Responses page. |
| data | JSON object | A JSON data object for some methods that need to return data. To maintain compatibility, if a method doesn't need to send back data, `{}` is sent back as the data object. See the method-specific documentation for more info.


## /api/initAPI
Methods: `POST`

Description: Initializes the the API for the first time. v3.0.0 and above only.

No form parameters are expected.

This method initializes the API for the first time, generating keys & other important credentials. This process can only be done once.

Initializing the API will create a config file for the API, which you can configure later. Once you initialize the API, it's recommended you check the config file for the auto-generated API keys & other important credentials. More documentation on the config file is available in the Wiki page titled "Configuration File".

Example for requesting via curl:

`curl https://snowdayapi.owenthe.dev/api/initAPI -X POST`

## /api/registerNumber
Methods: `POST`

Description: Handles registering numbers with the API.

Expected form parameters:

| Field | Type | Description |
|-------|------|-------------|
| key | string | Your API key (ex. `1234abcd`) |
| number | int | The number you are trying to register. This number is assumed to be a +1 number, and must be 12 digits long. Area code & local number must be included. (ex. `5554441234`) If you don't format the number correctly, you'll get an error code 5 (you had additional characters) or error code 3 (invalid length)|
| group (optional) | string | The name of the pre-defined group you'd like the number to be registered under. (e.x. `group1`) If not specified, the API will automatically use the pre-defined default group for the number to be registered under. Make sure that the group specified is pre-defined, if it's not then error code 90 will be returned (invalid group)|

If your number is successfully registered, you'll receive an error code 0 (success).

Your program should be able to wait for up to a minute for the API to confirm the status of the confirmation message.

You can receive many error codes from this API method, check the API Error Codes & Responses page for more info.

This method returns no data, and returns no cookies.

Example for requesting via curl:

`curl https://snowdayapi.owenthe.dev/api/registerNumber -F "key=1234abc" -F "number=5554441234" -F "group=group1" -X POST`

## /api/inboundSMS
Methods: `GET, POST`

Description: Handles all inbound SMS message into the API.

This API method is meant for Twilio's system, but you can manually manipulate the inbound SMS system.

Expected form parameters:

| Field | Type | Description |
|-------|------|-------------|
| key | string | Your API key (ex. `1234abcd`) |
| Body | string | The message body. Recommended to be under 140 characters, must be a string. (ex. `Y`) |
| From | string | The phone number of who the message is coming from. Must be a formatted phone number with an international prefix, area code, and local number. No dashes or spaces. (ex. `+15554441234`) |

TwiML-compatible XML is sent back every time - Error Code & API Version information is encoded in the headers of every request.

More information on error codes is available on the API Error Codes & Responses wiki page.

This method returns no data, and returns no cookies.

Example for requesting via curl:

`curl https://snowdayapi.owenthe.dev/api/inboundSMS -F "key=1234abc" -F "Body=Hello world!" -F "From=+15554441234" -X POST`

## /api/updateUserGroup
Methods: `POST`

Description: Updates the group a registered number is under.

As the description states, this method updates the group for a user (number). There are many potential combinations of form parameters for this API method, so please read carefully.

| Field | Type | Description |
|-------|------|-------------|
| key | string | Your API key (ex. `1234abc`)
| number | string | The number that you'd like the group info for. Must be formatted properly as a +1 number. (ex. `+15554441234`) |
| group (mostly required) | string | The name of the group that you'd like to change the number to. (ex. `group1`). The group parameter is required **UNLESS** the `setdefaultgroup` parameter is set to `true`. In this case, the group parameter is not necessary. |
| pendingnumber (mostly optional) | string | Specifies that the number's group data to be changed is still pending confirmation. Must be set to `true` as a string when you'd like to change the group data for a pending number. Otherwise, you don't need to specify this parameter. |
| setdefaultgroup (optional) | string | Specifies that the number specified should be set back to the default group. Must be set to `true` as a string when you'd like to set the group to the default group. Otherwise, you don't need to specify this parameter. |

This method returns no data, and returns no cookies.

Examples for requesting via. curl for different combinations:

| Combination | CURL command |
|-------------|--------------|
| Number is not pending confirmation, group should be set to `group1`. | `curl https://snowdayapi.owenthe.dev/api/updateUserGroup -F "key=1234abc" -F "number=+15554441234" -F "group=group1" -X POST` |
| Number is not pending confirmation, group should be set to default. | `curl https://snowdayapi.owenthe.dev/api/updateUserGroup -F "key=1234abc" -F "number=+15554441234" -F "setdefaultgroup=true" -X POST` |
| Number is pending confirmation, group should be set to `group1`. | `curl https://snowdayapi.owenthe.dev/api/updateUserGroup -F "key=1234abc" -F "number=+15554441234" -F "group=group1" -F "pendingnumber=true" -X POST` |
| Number is pending confirmation, group should be set to default. | `curl https://snowdayapi.owenthe.dev/api/updateUserGroup -F "key=1234abc" -F "number=+15554441234" -F "setdefaultgroup=true" -F "pendingnumber=true" -X POST` |

## /api/disableMessageDeliveryConfirmation
Methods: `POST`

Description: Disables various message delivery confirmation systems across the API.

This method will disable message delivery confirmation systems across the Snow Day API when there are issues with message confirmation, usually due to a carrier or Twilio issue. 

Disabling message delivery confirmation will disable `/api/outboundSMSstatus` entirely. Additionally, disabling message delivery confirmation will also cause false positives with errors on registering numbers, as when MDC is enabled by default, we don't care about the queued & sent states, but with MDC disabled, only queued is not cared for. This basically means that a user registering their number will get back a success, when they message may not have sent.

It is up to the administrator of the API to enable & disable MDC as needed. There are no systems in place to turn MDC back on after some time.

Please also note that across the API, it's mostly indicating if MDC is **DISABLED**, not the state of MDC. (True = MDC disabled, False = MDC enabled). I understand that it can be slightly confusing, so **be careful!**. 


| Field | Type | Description |
|-------|------|-------------|
| key | string | Your API key (e.x. `1234abc`) |
| setting (optional) | string or boolean | The setting (state) for if message delivery confirmation is disabled across the API. If you'd like to manually specify the state, use the form data to specify this state. Acceptable strings are `true` or `false` (case-insensitive), or as the True or False booleans (for best results, capitalize the T and F, as the Snow Day API is coded in Python). If this is not specified, then the message delivery confirmation disabled state will be set to the opposite of what it was (if the state is True, calling without the setting field means the state switches to False, and vice versa). For API security the new state will not be returned, but a method to get the current state will be added in v1.5.0. |

This method returns no data, and returns no cookies.

Example for requesting via. curl with the setting field:

`curl https://snowdayapi.owenthe.dev/api/disableMessageDeliveryConfirmation -F "key=1234abc" -F "setting=true" -X POST`

Example for requesting via. curl without the setting field:

`curl https://snowdayapi.owenthe.dev/api/disableMessageDeliveryConfirmation -F "key=1234abc" -X POST`

## /api/newMessage
Methods: `POST`

Description: Requests a new message to be sent out to all subscribed numbers.

For this to work, you'll need to make sure that your program can wait for about 1 second per 15 subscribed users at minimum - Performance has been vastly improved but with large setups you will be waiting a while.

Expected form parameters:

| Field | Type | Description |
|-------|------|-------------|
| key | string | Your API key for new messages (ex. `1234abc`) |
| message | string | The message you want to send out to everyone who is subscribed. You will get back an error code of 26 for any messages that would exceed 160 characters with the message & "prefix". For v2.0.0 and below, messages are restricted to 138 characters and under, in v3.0.0 this number is dynamically updated. (ex. `New prediction 6:00 PM, 1/1: 95% chance of snow day tomorrow`) |
| groups (optional) | string | A list separated string of groups to send the message out to. If the groups field is not specified, then the message will be sent out to all numbers. If a group is specified that does not exist, error code 90 will be returned. (e.x. `group1,group2,group3` for multi group or `group1` for single group)

A good tool for figuring out of your messages will be under 160 characters is here: https://chadselph.github.io/smssplit/

A prefix is this text in any message coming from the API:
`(Snow Day Predictor): ` (note the extra space after the `:`

Make sure that when you do calculations for message length you include the prefix with the extra space.

If your broadcast was sent successfully from the API side to Twilio, you'll get back a code 0. However, it will not report on if any individual messages aren't sent out - Check your errors AWS table and/or Twilio console to get detailed info.

**Please note: The code in newMessage doesn't account for the extra characters appended by Twilio to every SMS message if your account is in Trial Mode.**

This method returns no data, and returns no cookies.

Example for requesting via. curl: 

`curl https://snowdayapi.owenthe.dev/api/newMessage -F "key=1234abc" -F "message=New prediction 6:00 PM, 1/1: 95% chance of snow day tomorrow" -X POST`

## /api/outboundSMSstatus
Methods: `POST`

Description: Determines if any individual broadcast messages failed to deliver to their destination.

The outboundSMSstatus method is mostly for internal usage, and really shouldn't be tooled around with, unless you enjoy screwing up the API.

Expected form parameters:

| Field | Type | Description |
|-------|------|-------------|
| key | string | The API key for outgoing SMS status (ex. `1234abc`) |
| To | string | Who the outbound message was to when it was broadcasted. Should be a fully formatted number with an international prefix, area code and local number. No spaces/dashes. (ex. `+15554441234`) |
| SmsStatus | string | An SMS status string that Twilio would provide. "failed" and "undelivered" will trigger some action, anything else will just return a code 0 (v2.0.0 and up) or Success (v1.2.0 and below). (ex. `success`) |

This method returns no data, and returns no cookies.

Example for requesting via curl:

`curl https://snowdayapi.owenthe.dev/api/outboundSMSstatus -F "key=1234abc" -F "To=+15554441234" -F "SmsStatus=sent" -X POST`

## /api/requestNewSession
Methods: `POST`

Description: Allows a user to request a new session for the Snow Day Dash for pre-authentication.

When calling this method, a 30-minute session is created for pre-auth, in which a 6-digit code is sent to the number entered for the new session. A user has to verify their number for the pre-auth session to be successful.

As of v1.6.0, this is a keyless method. v1.6.1 will include rate limiting on this method to prevent abuse.

Expected form parameters:

| Field | Type | Description |
|-------|------|-------------|
| number | str | The number that should be tied to a new session for pre-authentication. The API will intentionally strip out `(`, `)`, `-`, a space, and `+` from any response. Additionally, if the first character is `1` (an international code was placed) after stripping characters, the `1` will be stripped. (e.x. `+15554441234`, `(555) 444-1234`, etc) |

This method returns no data.

This method returns cookies if the request for a new session is successful. Please see the table below for cookies returned.

| Name | Type | Description |
|------|------|-------------|
| preauth-sessionid | string | A session ID for pre-authentication (verification). These sessions last 30 minutes, and are only extended when a user requests a new verification code. |

This method employs origin protection - To request via curl you'll need to attach an Origin header.

Example for requesting via curl:

`curl https://snowdayapi.owenthe.dev/api/requestNewSession -F "number=5554441234" -H "Origin: https://owenthe.dev" -X POST`

## /api/verifyNewSession
Methods: `POST`

Description: Verifies a pre-auth session.

This method will verify a pre-authentication session for the Snow Day Dash, if the session ID & code match up. This method requires cookies to be sent along with the response.

Expected form parameters:

| Field | Type | Description |
|-------|------|-------------|
| code | int | The six-digit code associated with the pre-auth session to verify a new session. Authentication codes range from `100000` to `999999`. (e.x. `123456`) |

Expected cookies:

| Name | Type | Description |
|------|------|-------------|
| preauth-sessionid | string | The pre-auth session ID for the session that is being verified. |

This method returns no data.

This method returns cookies, depending on if verification was successful or not. Please see the table below for returned cookies.

| Name | Type | Description |
|------|------|-------------|
| sessionid | string | The session ID, if verification is successful. If the verification is unsuccessful due to an expired pre-auth session, or successful, this will be set to nothing with a UNIX expiry time 0. Additionally, this cookie will be set to nothing with a UNIX expiry time of 0 for an unsuccessful new code request due to an expired pre-auth session. This cookie will be set with an extended expiry time to the same preauth-sessionid specified in a request for a new auth code when the request is successful. Got that? |
| preauth-sessionid | string | If authentication is successful, preauth-sessionid is set to nothing with an expiry time of UNIX time 0 (Jan 1, 1970).|

This method employs origin protection - To request via curl you'll need to attach an Origin header.

Example for requesting via curl:

`curl https://snowdayapi.owenthe.dev/api/verifyNewSession --cookie "preauth-sessionid=1234abc" -H "Origin: https://owenthe.dev" -X POST`


## /api/requestNewAuthCode
Methods: `POST`

Description: Requests a new auth code for pre-auth sessions, if the code did not come through.

This method has intentional rate limiting so that there is a 5-minute timeout to request a new auth code.

No form parameters are expected with this method.

Expected cookies:

| Name | Type | Description |
|------|------|-------------|
| preauth-sessionid | string | A pre-auth session ID for the session that is being verified. |

This method returns data if we run into a timeout error, if the session validation was successful. See the sample data return, along with an explanation of the data keys.

```
{"apiversion": "v1.6.0",
 "code": 112,
 "data": {"time_to_wait": 200},
 "message": "Please wiat another 200 seconds before requesting a new verification code."
```

| Key | Type | Description |
|-----|------|-------------|
| time_to_wait | integer | The amount in seconds that you have to wait before requesting a new verification code. |

This method returns cookies, depending on the state of the request. Please see the table below for more information.

| Name | Type | Description |
|------|------|-------------|
| preauth-sessionid | string | A pre-auth session ID for the session that is being verified. If the verification code failed to send, or if there was a timeout error, no cookie will be returned. If the new code was sent successfully, this cookie will be returned with what was entered with an expiry time of 90 days. If the session entered was expired or not found in the database, this cookie will be set to nothing with a UNIX expiry time of 0. |

This method employs origin protection - To request via curl you'll need to attach an Origin header.

Example for requesting via. curl:

`curl https://snowdayapi.owenthe.dev/api/requestNewAuthCode --cookie "preauth-sessionid=1234abc" -H "Origin: https://owenthe.dev" -X POST`

## /api/unsubscribeNumber
Methods: `POST`

Description: Allows a number to be unsubscribed via the API. This method is mainly intended to be used by the Snow Day Dashboard, but can be used if you cannot directly access the numbers database. 

In v1.5.0 of the Snow Day API, this method can only be authenticated via API key. Authenticating via. Session ID will always return an error. In v1.6.0 of the Snow Day API, this method can be authenticated via API key or Session ID.

When calling this method via. session ID, the session ID (and any active session IDs tied with the number of the Session ID being called) are immediately deleted if the action was successful.

When a user is unsubscribed using this method, the user will be sent a message saying that they were unsubscribed.

`/api/unsubscribeNumber` must be called either via form or cookie method, you cannot mix and match.

Expected form parameters (form method):

| Field | Type | Description |
|-------|------|-------------|
| key | string | Your API key (ex. `1234abc`). For v1.5.0, this parameter is always expected. For v1.6.0, this parameter is optional **IF** you define an active session ID. |
| number | string | The number you'd like to unsubscribe. When this method is called using an API key, you **must** define the number to be unsubscribed in a valid +1 with area code format. When this method is called using a session ID, the number parameter will be ignored, and the number tied with the session ID will be unsubscribed. |

Expected cookie parameters:

| Name | Type | Description |
|------|------|-------------|
| sessionid | string | Session ID for the Snow Day Dashboard. This ID can be for an active or inactive session, but for this method to do anything this must be active. |

This form does not return any data.

If you request via cookies, this method returns cookies. See the table below for more information.

| Name | Type | Description |
|------|------|-------------|
| sessionid | string | Session ID for the Snow Day Dashboard. If the number unsubscribe action was successful, this cookie will be set to nothing with a UNIX expiry time of 0. |

This method employs origin protection - To request via curl you'll need to attach an Origin header.

Example for requesting via curl via the form method:

`curl https://snowdayapi.owenthe.dev/api/unsubscribeNumber -F "key=1234abc" -F "number=+15554441234" -H "Origin: https://owenthe.dev -X POST`

Example for requesting via curl for the cookie method:

`curl https://snowdayapi.owenthe.dev/api/unsubscribeNumber --cookie="sessionid=1234abc" -H "Origin: https://owenthe.dev -X POST`

## /api/confirmMessageErrorCode
Methods: `POST`

Description: Updates the error code for a confirmation message via. unique SID code.

This method is mainly for internal usage, and you'll likely get no use out of it in the large scheme of things. This method provides detailed error information for a confirmation message.

Expected form parameters:

| Field | Type | Description |
|-------|------|-------------|
| key | string | Your API key (ex. `1234abc`) |
| ErrorCode | int | An integer number meant to be supplied by Twilio as an error code. See Twilio's documentation on error codes for more info. In the entire scheme of the API we only care for 3000x error codes, those being SMS errors. (ex. `30003`) |
| MessageStatus | string | A human readable status of the message code. Having "sent" or "queued" will trigger no response from the API, anything else will trigger an API response. (ex. `sent`) |
| MessageSid | string | The unique SID for the message string. See Twilio's documentation on message SIDs for more information. (ex. `SMabcd1234`) |
 
As with the outboundSMSstatus method, you'll likely get no usage from this method, as it's just for internal usage.

Example for requesting via curl:

`curl https://snowdayapi.owenthe.dev/api/confirmMessageErrorCode -F "key=1234abc" -F "ErrorCode=30003" -F "MessageStatus=failed" -F "MessageSid=SMabcd1234" -X POST`

## /api/getUserSettings
Methods: `GET`

Description: Gets user settings for a session for the Snow Day Dash. This method returns settings for a number associated with a session.

No form parameters are expected.

Expected cookies:

| Name | Type | Description |
|------|------|-------------|
| sessionid | string | A session ID for the Snow Day Dashboard. This session ID can be active or inactive. (e.x. `1234abc`) |

This method returns data. A sample of returned data is provided below, along with an explanation for each of the data keys.

```
{"apiversion": "v1.6.0",
 "code": 130,
 "data": {"group": "group1",
          "messagehold": 123456789,
          "24_hr_prefix": false,
          "no_prefix": true,
          "timezone_in_msg": false,
          "dash_name": "name1"
          },
 "message": "Data retrieval successful."
}
```

| Key | Type | Description |
|-----|------|-------------|
| group | string | The name of the group that the user is in (e.x. `group1`) |
| messagehold | integer | The UNIX time that a user is holding their messages to. A value of `0` indicates no message hold. (e.x. `123456789`) |
| 24_hr_prefix | boolean | Boolean indicating if the user has the time in a new prediction message. True means that the user is using 24-hour time, False means that the user is using 12-hour time. (e.x. `true`) |
| no_prefix | boolean | Boolean indicating if the user does not have the standard prefix at the start of a new message (prediction messages). True means that there is no standard prefix, False means that there is a standard prefix (e.x. `false`) |
| timezone_in_msg | boolean | Boolean indicating that if the local timezone of the predictions (ET) is included in the prediction time. True means that there is the timezone indicator in a user's messages, False means that there is no timezone indicator in the user's messages. |
| dash_name | string | String indicating the user's dash name. By default, this is set to the user's phone number. (e.x. `name1`) |

This method returns cookies, depending on the state of the request. Please see the table below for more information.

| Name | Type | Description |
|------|------|-------------|
| sessionid | string | A session ID for the Snow Day Dashboard. If getting the user settings was successful, or if the session was validated as active but couldn't retrieve number data, this cookie will be set to what was entered with an expiry time of 90 days. If we find that the session was expired or if the session was not found in the database, this cookie will be set to nothing with a UNIX expiry time of 0. |

This method employs origin protection - To request via curl you'll need to attach an Origin header.

Example for requesting via curl:

`curl https://snowdayapi.owenthe.dev/api/getUserSettings --cookie "sessionid=1234abc" -H "Origin: https://owenthe.dev" -X GET`


## /api/updateUserSettings
Methods: `POST`

Description: Updates user settings for a session (generally through the Snow Day Dash).

This method can update up to 6 settings at once.

Expected form parameters:

| Field | Type | Description |
|-------|------|-------------|
| userGroup | string | The user group that the user should be put into. It must be a valid group for the data to be accepted. (e.x. `group1`) |
| messageHold | integer, float, integer as a string, float as a string | The UNIX time that the user would like to hold messages until. This method accepts integers - but will accept a float, just rounded down. You can also put an integer or float as a string. Sending back `0` means no message hold. (e.x. `123456789`) |
| twentyFourPrefix | boolean, boolean as a string | A boolean indicating if a user wants to have a 24-hour time as the "New prediction: (Time) (Date)" in a new message. Can be a raw boolean, or as a boolean as a string. (e.x. `true`) |
| noPrefix | boolean, boolean as a string | A boolean indicating if a user wants to have the standard prefix in a new message. If enabled, a standard prefix will still show for informational messages, like confirmation codes, etc. (e.x. `true`) |
| timezoneInMsg | boolean, boolean as a string | A boolean indicating if a user wants to have `ET` appended to the end of a prediction time, in the event that someone is getting predictions in a different timezone. (e.x. `true`) |
| dashName | string, limited to 63 characters or less | The user's dash name for the Snow Day Dashboard. dashName gets filtered for any characters that could result in XSS, etc. (e.x. `name1`). |

Expected cookies:

| Name | Type | Description |
|------|------|-------------|
| sessionid | string | A session ID for the Snow Day Dashboard. This session ID can be active or inactive - but for data to update the session must be active. (e.x. `1234abc`) |

This method returns no data.

This method returns cookies, depending on the state of the request. Please see the table below for more information.

| Name | Type | Description |
|------|------|-------------|
| sessionid | string | A session ID for the Snow Day Dashboard. If updating user settings was successful, this cookie will come back as entered with an expiry time of 90 days. If the session entered expired or wasn't in the database, this cookie will be set to nothing with a UNIX expiry time of 0. |

This method employs origin protection - To request via curl you'll need to attach an Origin header.

Example for requesting via curl:

`curl https://snowdayapi.owenthe.dev/api/updateUserSettings --cookie "sessionid=1234abc" -F "userGroup=group1" -F "messageHold=123456789" -F "twentyFourPrefix=false" -F "noPrefix=frue" -F "timezoneInMsg=false" -F "dashName=name1" -H "Origin: https://owenthe.dev" -X POST`

## /api/terminateSession
Methods: `POST`

Description: Terminates a session for the Snow Day API.

/api/terminateSession does what it says - terminates a session, regardless if it's expired or not. This method removes a specified session from the database.

No form parameters are expected with this method.

Expected cookies:

| Name | Type | Description |
|------|------|-------------|
| sessionid | string | A session ID for the Snow Day Dashboard. This session ID can be active or inactive. (e.x. `1234abc`) |

This method returns no data.

This method returns cookies. Please see the table below for more information.

| Name | Type | Description |
|------|------|-------------|
| sessionid | string | A session ID for the Snow Day Dashboard. Unless the session cookie was not specified, this cookie will always be set to nothing with a UNIX expiry time of 0. |

This method employs origin protection - To request via curl you'll need to attach an Origin header.

Example for requesting via. curl:

`curl https://snowdayapi.owenthe.dev/api/terminateSession --cookie "sessionid=1234abc" -H "Origin: https://owenthe.dev" -X POST`

## /api/verifyPreAuthSessionStatus
Methods: `POST`

Description: Verifies if a pre-auth session is still active.

This method is used on the Snow Day Dash verification page to ensure that a pre-auth session is still active if someone renavigates to the page.

No form parameters are expected with this method.

Expected cookies: 

| Name | Type | Description |
|------|------|-------------|
| preauth-sessionid | string | A pre-auth session ID for the Snow Day Dashboard. This pre-auth session ID can be active or inactive.

This method returns no data.

This method returns a cookie, depending on the state of the request. Please see the table below for more information.

| Name | Type | Description |
|------|------|-------------|
| preauth-sessionid | string | A pre-auth session ID for the Snow Day Dashboard. If the pre-auth session is valid, this cookie will not return. However, if the pre-auth session could not be found or expired, then the cookie will be set to nothing with a UNIX expiry time of 0. |

This method employs origin protection - To request via curl you'll need to attach an Origin header.

Example for requesting via. curl:

`curl https://snowdayapi.owenthe.dev/api/verifyPreAuthSessionStatus --cookie "preauth-sessionid=1234abc" -H "Origin: https://owenthe.dev" -X POST`

## /api/verifySessionStatus
Methods: `POST`

Description: Verifies if a session is still active. If the session is active, the session expiry is extended to 30 minutes past request time. If the session has expired, the session will be deleted from the database.

This method is generally used on the Snow Day Dash to make sure a session is active when navigating to a new page.

No form parameters are expected.

Expected cookies:

| Name | Type | Description |
|------|------|-------------|
| sessionid | string | A session ID for the Snow Day Dashboard. This session ID can be active or inactive. (e.x. `1234abc`) |

This method returns no data.

This method returns cookies depending on the state of the request. Please see the table below for more information.

| Name | Type | Description |
|------|------|-------------|
| sessionid | string | A session ID for the Snow Day Dashboard. If verifying the session status is successful, and the session is valid, this cookie will be set to whatever came in, with an expiry time of 90 days. If the session is expired, or the session could not be found, this cookie will be set to nothing with a UNIX expiry time of 0. |

This method employs origin protection - To request via curl you'll need to attach an Origin header.

Example for requesting via curl:

`curl https://snowdayapi.owenthe.dev/api/verifySessionStatus --cookie "sessionid=1234abc" -H "Origin: https://owenthe.dev" -X POST`

## /
Methods: `GET`

Description: The base URL for the API. Returns an HTML page.

If you end up requesting the base URL for the API, you'll always get an HTML page in response, with some basic information.

These pages are customized on if the API is closed or not.

No form parameters are expected.