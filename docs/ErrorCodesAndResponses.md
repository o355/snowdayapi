This is an oudated document describing API error codes. Some information is applicable to the API, but there are many more error codes that aren't referenced in this document. Please see the updated documentation. This document is being retained for historical preservation purposes only.

----

**Last updated: 5/26/2019 | For API version 1.4.0**

Beginning with v1.4.0 of the Snow Day API, when a request is made, a response is sent back with a specific error code, and a standard HTTP response.

v1.3.0 and earlier of the API will **only** send back specific error codes. Even if an operation fails, a 200 OK is sent with a JSON-encoded payload as its response. 

For all methods except `/api/inboundSMS`, a JSON-encoded response will be sent back with error code and API information like this:

```
{"apiversion": "v2.0.0",
 "code": 0,
 "message": "The action was successfully completed."}
```

All responses, including `/api/inboundSMS` will return this information in the response headers. This headers are as follows:

```
X-SnowDayAPI-Version - API version as a string (e.g. v1.3.0)
X-SnowDayAPI-Code - Status code returned from the API as an integer (e.g. 0)
```

The English translation of the error code and documentation URL is NOT sent in the headers, only the API version and status code.

Depending on what API method you've requested, these error codes will mean entirely different things.

Please note: Because the API is not on version 2.0.0, error codes are not yet standard! This is future documentation and reference for developing the API.

## Standard error codes across the API

### Error Code `54`
The API key you entered was invalid. Please enter a valid API key for the method you are requesting.

Error Code 54 is returned when an invalid API key is entered. API keys don't have any spaces, and are all lowercase. Additionally, they are **key-sensitive**.

Error Code 54 is always returned with HTTP status code `403`.

### Error Code `55`
The API is closed for an unknown reason, and is not accepting any requests.  Contact the server owner for more information.

Error Code 55 is returned when the server administrator has closed down requests.

Error Code 55 is always returned with HTTP status code `503`.

### Error Code `57`
The API is closed for an extended period of time, and is not accepting requests. Visit the homepage of the API for more information, or contact the server owner.

Error Code 57 is returned when the server administrator has closed down requests for a period of time. Server admins should put he re-opening date on the homepage, so be sure to check the homepage for more info.

Error Code 57 is always returned with HTTP status code `503`.

### Error Code `59`
The API is closed down for maintenance, and is not accepting any requests. Visit the homepage of the API for more information, or contact the server owner.

Error Code 59 is returned when the server administrator has temporarily closed down requests to perform maintenance. Maintenance mode should only be enabled for 96-120 hours at most.

Error Code 59 is always returned with HTTP status code `503`.

### Error Code `72`
The API has not yet been initialized. Make sure you initialize the API before usage.

Error Code 72 is returned when the API has not yet been initialized. Make sure you call /api/initAPI to get things started.

**Please note: Error Code 72 is returned across every API method EXCEPT /api/initAPI, if the API is not yet initialized.**

Error Code 72 is always returned with HTTP status code `503`.

## Standardized HTTP codes across the API

### HTTP Code `200`
OK

200 codes are sent back when an operation was successfully completed with no errors.

### HTTP code `400`
Bad request

HTTP code 400 is sent back from a bad request. It should be noted that code 400 will **only** come back requests that are bad (e.g. number is already subscribed, etc). Additionally, code 400 will come back when a mandatory parameter for a request is not sent along.

### HTTP Code `403`
Forbidden

HTTP code 403 is returned when the request was unauthorized due to an invalid API key.

### HTTP Code `405`
Method Not Allowed

HTTP code 405 is returned when a method is known by the API, but isn't implemented in the current version of the API, but will be implemented soon.

### HTTP Code `408`
Request timeout

HTTP code 408 is returned when an external operation times out, and as a result, the request has timed out. For now, this occurs only in `/api/registerNumber` if the validation timed out, but will be expanded to include database time outs as well.

### HTTP Code `422`
Unprocessable Entity

HTTP code 422 is returned when the form data of a request is understood, but the API could not process the data. For example, if you wanted to send out a message to many groups, but one of the groups did not exist, a 422 would be sent back. 

### HTTP Code `429`
Too many requests

HTTP code 429 is returned when too many requests have been made to the API, and you are being rate limited.

### HTTP Code `500`
Internal Server Error

HTTP code 500 is returned when an unexpected error occurs. This can be returned intentionally, like when the configuration file failed to load, or when a Python exception occurred.

### HTTP Code `502`
Gateway Error

HTTP code 502 is returned when a gateway error occurs with Twilio, and when the request should not be made again. For now, HTTP status code 502 is only returned with `/api/outgoingSMSstatus` when a user reaches the error limit, and the message to the user did not send properly.

### HTTP Code `503`
Service temporarily unavailable

HTTP code 503 is returned when an issue with an external service being used is unavailable, and as a result, the service is unavailable. For now, 503s are returned for database issues with DynamoDB, and if the API is closed down.

### HTTP Code `504`
Gateway timeout

HTTP code 504 is sent back in specific cases where a timeout occurs with an external service. For now, 504s are not used in production, but will be soon.

## /api/initAPI

### Important Note
`/api/initAPI` has not been implemented into the API. As such, these error codes are not in use, and calling `/api/initAPI` will always return HTTP 405 (Method Not Allowed)

### Status Code `405` & Error Code `99405`
initAPI is not yet available in this version of the copy of the API.

When calling a copy of the API that is running v1.3.0 or higher, but doesn't have the initAPI functionality built-in, status code 405 & error code 99405 is returned.

Error Code 99405 is always returned with HTTP status code `405`.

### Error Code `0`
The API was successfully initialized. Check the config.cfg file on the machine where the API is stored for more information.

Error Code 0 is only returned once on initAPI. No information about API keys, etc is returned, check the server for the newly created config file.

Error Code 0 is always returned with HTTP status code `200`.

### Error Code `71`
The API has already been initialized.

Error Code 71 is returned when the API has already been initialized. Check the server for the newly created config file.

Error Code 71 is always returned with HTTP status code `400`.

## /api/registerNumber

### Error Code `0`
The number inputted was successfully registered. Check for a confirmation text from the Twilio number set up for use to start using the service.

Error Code 0 is always returned with HTTP status code `200`.

### Error Code `3`
The number inputted has an invalid length. The number should be 12 characters long, without any dashes or spaces. (ex. `5554441234`)

If the length of the number in the form is not 12 characters, Error Code 3 is always returned.

Error Code 3 is always returned with HTTP status code `422`.

### Error Code `4`
The number inputted has already been subscribed to the service.

If Error Code 4 is being returned and you've tried to unsubscribe the number you've inputted, check your Twilio console for any errors, or delete the number manually from the Numbers database.

Error Code 4 is always returned with HTTP status code `400`.

### Error Code `5`
The number inputted has additional characters, like parenthesis, or dashes.

Inputting this will trigger error code 5: `(555) 444-1234`

Inputting this will also trigger error code 5: `555-444-1234`

But inputting this won't trigger error code 5: `5554441234`.

Make sure that the number input is checked for extra characters - Even an accidental space may fool the system.

Error Code 5 is always returned with HTTP status code `422`.

### Error Code `6`
When attempting to send out the confirmation code to the inputted number, Twilio encountered a fatal error. This may be due to that your Twilio account hasn't been upgraded from Trial Mode, invalid API key(s), invalid originating number, or an internet connection issue. 

Additionally, numbers who did a full unsubscribe by sending `UNSUBSCRIBE`, or `STOP` to the Twilio number configured for use will return error code 6 if the number is attempted to be registered. I call this a "Twilio-side block", but generally it's Twilio's system saying that you can't send messages to this number, it requested an unsubscribe on our end.

Error Code 6 is always returned with HTTP status code `400`.

### Error Code `8`
The number inputted is pending verification. Check for a text from the Twilio number set up for use to start using the API, or contact the server owner for more information.

Error Code 8 will only be triggered if the confirmation text was seen as successfully sent to the inputted number. However, any API is never 100% reliable, so it's possible that Twilio thought it sent the message successfully but in reality it didn't.

Error Code 8 is always returned with HTTP status code `400`.

### Error Code `13`
The number inputted is not reachable, and returned a Twilio error 30003. Make sure the number is reachable.

See Twilio's error code documentation on Error 30003 for more information: https://www.twilio.com/docs/api/errors/30003

Error Code 13 is always returned with HTTP status code `400`.

### Error Code `14`
The number inputted blocked the confirmation message, and is not reachable, and returned a Twilio error 30004. Make sure that the number inputted is not blocked.

Error Code 14 will usually occur if a user has not done a Twilio-side block, but instead blocked the number on their own phone or through the carrier.

See Twilio's error code documentation on Error 30004 for more information: https://www.twilio.com/docs/api/errors/30004

Error Code 14 is always returned with HTTP status code `400`.

### Error Code `15`
The number inputted resulted in an unknown destination error.

Error Code 15 is a general error code. It can result from SMS not being available on the number inputted, the number not being reachable, or other reasons.

See Twilio's error code documentation on Error 30005 for more information: https://www.twilio.com/docs/api/errors/30005

Error Code 15 is always returned with HTTP status code `400`.

### Error Code `16`
The number inputted is a landline number, and cannot receive SMS messages.

Error Code 16 is pretty self-explanatory, it means you entered a landline number.

See Twilio's error code documentation on Error 30006: https://www.twilio.com/docs/api/errors/30006

Error Code 16 is always returned with HTTP status code `400`.

### Error Code `17`
The number inputted cannot receive the confirmation text due to carrier spam filtering.

Error Code 17 occurs when a message can't be sent to a carrier. Possible reasons for Error Code 17 is that you've been sending too many messages to phone numbers on a certain carrier, or that the confirmation text was flagged as spam by the carrier.

The best course of action with Error Code 17 is to wait about 24-48 hours before requesting another confirmation, or to limit the amount of messages being broadcasted.

See Twilio's error code documentation on Error 30007: https://www.twilio.com/docs/api/errors/30007

Error Code 17 is always returned with HTTP status code `400`.

### Error Code `18`
A generic unknown error occurred.

Error Code 18 occurs when Twilio returns an unknown error. There are many reasons why you might be getting Error Code 18, like a number not being able to receive SMS messages, etc.

See Twilio's error code documentation on Error 30008: https://www.twilio.com/docs/api/errors/30008

Error Code 18 is always returned with HTTP status code `400`.

### Error Code `19`
An unknown error occurred.

Error Code 19 occurs when we get back a non 3000x error code from Twilio. See your Twilio logs to see full error details.

Error Code 19 is always returned with HTTP status code `500`.

### Error Code `22`
A timeout error occurred when attempting to validate if the confirmation message sent.

Error Code 22 will show up after 60 seconds if the confirmation text verification times out. Possible solutions include emptying the SIDStatus database, emptying the PendingNumbers database, and restarting the phone you're trying to confirm.

Error Code 22 is always returned with HTTP status code `408`.

### Error Code `33`
The maximum number of numbers registered has been reached, your number could not be registered. Please try again when some users have unsubscribed.

Error Code 33 is only returned when the owner of the copy of the API has turned on a limit for number registrations.

Error Code 33 is always returned with HTTP status code `503`.

### Error Code `37`
A database error has prevented confirmation of the number inputted.

Error Code 37 occurs when the API encounters a critical error when attempting to confirm the number that was inputted. This may be due to a variety of reasons - DynamoDB being down, invalid keys, missing databases, corrupt or incorrectly formatted databases, etc. If code 37 starts returning and DynamoDB isn't down, make sure API keys are right, databases are set up properly, etc.

Error Code 37 is always returned with HTTP status code `503`.

### Error Code `90`
The group specified in the request is not a valid group.

Error Code 90 is returned when the optional group parameter is specified, and the group that was specified is not a valid group.

Error Code 90 is always returned with HTTP status code `422`.

## /api/inboundSMS

### Important Note
inboundSMS responses are always in XML that is TwiML compatible. Please read more on how TwiML is formatted for responses. Error Code information is stored in the headers as described in the prelude of this page.

### Error Code `0`
The action was completed successfully.

Error Code 0 is always returned for commands that don't require external operations, and is returned when external operations for a command are successful.

Error Code 0 is always returned with HTTP status code `200`.

### Error Code `24`
No response to the inbound message is necessary. 

Error Code 24 is used as a replacement for "Success" or a 204 code for when no response is needed for an inbound SMS message.

Error Code 24 is always returned with HTTP status code `200`.

### Error Code `26`
When attempting to complete a database operation associated with the command received, a database error occurred.

Error Code 26 is returned when a command is received to do something to the API (e.g. unsubscribe a user, check errors, etc), and the operation is unsuccessful. Code 26 may be returned due to bad API keys, non-existent databases, etc.

Error Code 26 is always returned with HTTP status code `503`.

### Error Code `29`
No response to the non-command message is necessary, because the number has already responded with a non-command message before.

Error Code 29 occurs when an inbound SMS message does not contain a known command, and if the number specified in "To" already received notice that the API has already responded that it cannot accept non-command messages.

Error Code 29 is always returned with HTTP status code `200`.

## /api/updateUserGroup

### Error Code `40`
The database operation was successful.

Error Code 40 is returned when the operation was successful.

Error Code 40 is always returned with HTTP status code `200`.

### Error Code `41`
The database operation was unsuccessful.

Error Code 41 is returned when the operation was unsuccessful.

Error Code 41 is returned in a variety of situations to preserve security of the API. As a result, Error Code 41 is returned when a number could not be found, or a database error occurred.

Error Code 41 is always returned with HTTP status code `503`.

### Error Code `90`
The group specified in the request is not a valid group.

Error Code 90 is returned when the specified group in the group parameter is not a valid group.

Error Code 90 is always returned with HTTP status code `422`.

### Error Code `91`
The group parameter was not specified.

Error Code 91 is returned when the group parameter is not specified when required.

Error Code 91 is always returned with HTTP status code `400`.

### Error Code `92`
The number parameter was not specified.

Error Code 92 is returned when the number parameter is not specified, since it is always required.

Error Code 92 is always returned with HTTP status code `400`.

## /api/newMessage

### Error Code `27`
The message you entered exceeded the maximum character limit.

Error Code 27 will return if the message you send exceeded the message segment limit set in config.cfg.

Generally, here's a formula to figure out the character limit.

`(160 * message segment limit) - prefix length)`

Prefix length counts an extra space after the `:` character.

Error Code 27 is always returned with HTTP status code `413`.

### Error Code `90`
The groups specified in the request are not valid groups.

Error Code 90 is returned when the specified group in the optional groups parameter is not a valid group, or one of the groups is invalid (if multiple groups was specified). Additionally, Error Code 90 may also be returned if the group parameter was malformed, especially when specifying multiple groups.

Error Code 90 is always returned with HTTP status code `422`.

## /api/outboundSMSstatus

### Error Code `23`
The message indicating to the user that they accumulated too many delivery errors failed to send.

Error Code 23 is always returned with HTTP status code `200`.

### Error Code `25`
The message indicating to the user that they accumulated too many delivery errors failed to send.

If you get back Error Code 25, this means that there was a Gateway Error, and that the request should not be repeated. As such, Error Code 25 is always returned with HTTP status code `502`.

Error Code 25 is similar to Error Code 14 in /api/inboundSMS, which occurs when a user has applied a non-Twilio block.

### Error Code `2502`
The message indicating to the user that they accumulated too many delivery errors failed to send, and the database operation to delete the number from the ErrorNumbers database failed.

Error Code 2502 occurs when a multi-error occurs. If you get Error Code 2502, make sure to check your database settings.

**Please note: Error Code 2502 has not been implemented into the API. It is planned to be implemented in v1.5.0.**

### Error Code `2503`
The message indicating to the user that they accumulated too many delivery errors failed to send, and the database operation to delete the number from the Numbers database failed.

Error Code 2503 occurs when a multi-error occurs. If you get Error Code 2503, make sure to check your database settings.

**Please note: Error Code 2503 has not been implemented into the API. It is planned to be implemented in v1.5.0.**

### Error Code `2504`
The message indicating to the user that they accumulated too many delivery errors failed to send, and the database operation to delete the number from the Numbers database failed, and the database operation to delete the number from the ErrorNumbers database failed.

Error Code 2504 occurs when a multi-error occurs. If you get Error Code 2504, make sure to check your database settings.

**Please note: Error Code 2504 has not been implemented into the API. It is planned to be implemented in v1.5.0.**

### Error Code `30`
The operation increasing the amount of errors by 1 has succeeded.

Error Code 30 indicates to you if the database operation was successful to increment the error count for the number by one.

Error Code 30 is always returned with HTTP status code `200`.

### Error Code `31`
The message status does not require any further action.

Unless `failure` or `undelivered` is specified as the message status in the headers, you'll get back error code 31 meaning no further action is required.

Error Code 31 is always returned with HTTP status code `200`.

### Error Code `32`
The operating increasing the amount of errors by 1 has not succeeded.

Error Code 32 occurs when a database error occurs, and the amount of errors can't be increased.

**Please note: Error Code 32 has not been implemented into the API. It is planned to be implemented in v1.5.0.**

### Error Code `93`
Message Delivery Confirmation has been disabled. Outgoing SMS status reports are not being accepted at this time.

Error Code 93 occurs when the Message Delivery Confirmation system has been disabled by the API administrator, (CONFIG/confirmation_disabled is True). As a result, no outgoing SMS statuses are being accepted, as if they were, users could accumulate delivery errors unintentionally.

Error Code 93 is always returned with HTTP status code `503`.

## /api/disableMessageDeliveryConfirmation

### Error Code `0`
The operation was successful.

Error Code 0 is returned when the operation to change the state of message delivery confirmation was successful.

Error Code 0 is always returned with HTTP status code `200`.

### Error Code `1`
The operation was unsuccessful.

Error Code 1 is returned when the operation to change the state of message delivery confirmation was unsuccessful. This may be due to bad permissions on the config file for storing these changes, or the config file not existing.

Error Code 1 is always returned with HTTP status code `500`.

### Error Code `2`
The setting parameter was not specified properly. No changes have been made.

Error Code 2 is returned when the desired state in the form data to change was invalid. See the API documentation for how to properly specify the state.

Error Code 2 is always returned with HTTP status code `422`.

### Error Code `3`
Internal Server Error: The pre-configured setting is not an acceptable value.

Error Code 3 is returned when the desired state is not set in the form data, and when the state in the config file is not a valid state. If this error occurs, make sure the confirmation_disabled key in the config file is set to either "True" or "False".

Error Code 3 is always returned with HTTP status code `500`.

### Error Code `4`
Internal Server Error: There was a problem accessing the MDC setting.

Error Code 4 is returned when we cannot get the current MDC setting from the config file. This may be due to bad config file location setting, or if the config file cannot be accessed due to a permissions error.

Error Code 4 is always returned with HTTP status code `500`.

## /api/confirmMessageErrorCode

### Error Code `40`
The message status does not require any further action.

Unless `failure` or `undelivered` is specified as the message status in the headers, you'll get back error code 40 meaning no further action is required.

Error Code 40 is always returned with HTTP status code `200`.

### Error Code `41`
The database operation was successful.

Error Code 41 is returned when the SID status is properly updated in the database.

Error Code 41 is always returned with HTTP status code `200`.

### Error Code `42`
The database operation was unsuccessful.

Error Code 42 is returned when the SID status wasn't properly updated in the database. If you get back Error Code 42, check your database settings.

**Please note: Error Code 42 has not been implemented into the API. It is planned to be implemented in v1.4.0.**

## /api/unsubscribeNumber

Full error code documentation coming after retrofit

## /api/terminateSession

### Error Code `100`
Bad origin.

Error Code 100 is returned when the origin in the HTTP headers does not match the whitelisted origins for API access.

Error COde 100 is always returned with HTTP status code `400`.

### Error Code `129`
Session ID not specified. This may be due to cookie expiration or deletion.

Error Code 129 is returned when the session ID cookie is not specified to terminate the session. Error Code 129 is returned if you didn't specify a cookie via. curl, or if the cookie in the browser isn't specified.

Error Code 129 also gets returned when the cookie is set to nothing.

Error Code 129 is always returned with HTTP status code `400`.

### Error Code `146`
Session was successfully terminated.

Error Code 146 is returned when the session is properly terminated and deleted from the database.

Error Code 146 is always returned with HTTP status code `200`.

### Error Code `147`
Session not found. This may be due to the session already being terminated.

Error Code 147 is returned when a session is specified, but the session could not be found in the database table.

The Snow Day API automatically purges sessions that have been expired for more than 72 hours at 3 AM local time every night, so if you specified a previously-valid session that was last accessed 72.5 hours or later, you'll get Error Code 147.

Additionally, specifying a generally incorrect session ID will also trigger Error Code 147.

Error Code 147 is always returned with HTTP status code `400`.

### /api/updateUserSettings

### Error Code `100`
Bad origin.

Error Code 100 is returned when the origin in the HTTP headers does not match the whitelisted origins for API access.

Error Code 100 is always returned with HTTP status code `400`.

### Error Code `125`
Session not found. This may be due to an expired session that was deleted from calling another session-based method.

Error Code 125 is returned when a Session ID is specified, but the session ID specified could not be found in the database. Similar to Error Code 147 in /api/terminateSession, this could be due to a previously valid session being purged from the database, or the specified session not actually existing.

Error Code 125 is always returned with HTTP status code `400`.

### Error Code `128`
Session has expired. Please request a new session to update user settings.

Error Code 128 is returned when the session ID specified was a previously valid session, but has expired.

Error Code 128 is always returned with HTTP status code `401`.

### Error Code `129`
Session ID not specified. This may be due to cookie expiration or deletion.

Error Code 129 is returned when the session ID cookie is not specified. Error Code 129 is returned if you didn't specify a cookie via. curl, or if the cookie in the browser isn't specified.

Error Code 129 also gets returned when the cookie is set to nothing.

Error Code 129 is always returned with HTTP status code `400`.

### Error Code `133`
Invalid data type

Error Code 133 is returned when data for a certain user setting to be updated is of an invalid type. Specific data can be seen in the message, and will also be encoded in error codes in v1.7.0.

Error Code 133 is always returned with HTTP status code `422`.

### Error Code `135`
User data updated successfully.

Error Code 135 is returned when the requested data to be updated was successful. For now, Error Code 135 is returned even when no changes are requested, but support for this type of response will come in v1.7.0.

Error Code 135 is always returned with HTTP status code `200`.

### Error Code `138`
Invalid data - Dash Name must be 63 characters or less.

Error Code 138 is returned when the specified string to update the existing dash name is 64 characters or longer **BEFORE SANITIZATION**. 

Error Code 138 is always returned with HTTP status code `422`.

## /api/getUserSettings

### Error Code `100`
Bad origin.

Error Code 100 is returned when the origin in the HTTP headers does not match the whitelisted origins for API access.

Error Code 100 is always returned with HTTP status code `400`.

### Error Code `125`
Session not found. This may be due to an expired session that was deleted from calling another session-based method.

Error Code 125 is returned when a Session ID is specified, but the session ID specified could not be found in the database. Similar to Error Code 147 in /api/terminateSession, this could be due to a previously valid session being purged from the database, or the specified session not actually existing.

Error Code 125 is always returned with HTTP status code `400`.

### Error Code `128`
Session has expired. Please request a new session to get user settings.

Error Code 128 is returned when the session ID specified was a previously valid session, but has expired.

Error Code 128 is always returned with HTTP status code `401`.

### Error Code `129`
Session ID not specified. This may be due to cookie expiration or deletion.

Error Code 129 is returned when the session ID cookie is not specified. Error Code 129 is returned if you didn't specify a cookie via. curl, or if the cookie in the browser isn't specified.

Error Code 129 also gets returned when the cookie is set to nothing.

Error Code 129 is always returned with HTTP status code `400`.

### Error Code `130`
Data retrival successful.

Error Code 130 is returned when the data was returned successfully.

Error Code 130 is always returned with HTTP status code `200`.

### Error Code `131`
Internal Server Error: Could not find number associated with session.

Error Code 131 really should be never returned, but is here in the event that somehow a number associated with a session no longer exists.

The only feasible situation where Error Code 131 could be returned is when a new session is requested, and then the user unsubscribes, then manually tries to get data.

Error Code 131 is always returned with HTTP status code `500`.

## /api/verifyPreAuthSessionStatus

### Error Code `100`
Bad origin.

Error Code 100 is returned when the origin in the HTTP headers does not match the whitelisted origins for API access.

Error Code 100 is always returned with HTTP status code `400`.

### Error Code `127`
Pre-Auth session is valid.

Error Code 127 is returned when the pre-auth session ID specified is valid.

Error Code 127 is always returned with HTTP status code `200`.

### Error Code `128`
Pre-Auth session has expired.

Error Code 128 is returned when the pre-auth session ID specified was a previously valid session, but has expired.

Error Code 128 is always returned with HTTP status code `401`.

### Error Code `129`
Pre-Auth session ID not specified. This may be due to cookie expiration or deletion.

Error Code 129 is returned when the pre-auth session ID cookie is not specified. Error Code 129 is returned if you didn't specify a cookie via. curl, or if the cookie in the browser isn't specified.

Error Code 129 also gets returned when the cookie is set to nothing.

Error Code 129 is always returned with HTTP status code `400`.

## /api/verifySessionStatus

### Error Code `100`
Bad origin.

Error Code 100 is returned when the origin in the HTTP headers does not match the whitelisted origins for API access.

Error Code 100 is always returned with HTTP status code `400`.

### Error Code `125`
Session not found. This may be due to an expired session that was deleted from calling another session-based method.

Error Code 125 is returned when a Session ID is specified, but the session ID specified could not be found in the database. Similar to Error Code 147 in /api/terminateSession, this could be due to a previously valid session being purged from the database, or the specified session not actually existing.

Error Code 125 is always returned with HTTP status code `400`.

### Error Code `127`
Session is valid, and expiry time has been set to 30 minutes from now.

Error Code 127 is returned when the session ID specified is valid. Additionally, when this method is called and Error Code 127 is returned, the session expiry time is set to 30 minutes past when this method is called.

Error Code 127 is always returned with HTTP status code `200`.

### Error Code `128`
Session has expired.

Error Code 128 is returned when the session ID specified was a previously valid session, but has expired.

Error Code 128 is always returned with HTTP status code `401`.

### Error Code `129`
Session ID not specified. This may be due to cookie expiration or deletion.

Error Code 129 is returned when the session ID cookie is not specified. Error Code 129 is returned if you didn't specify a cookie via. curl, or if the cookie in the browser isn't specified.

Error Code 129 also gets returned when the cookie is set to nothing.

Error Code 129 is always returned with HTTP status code `400`.

## /api/verifyNewSession

## /api/requestNewAuthCode

## /api/requestNewSession