This is an outdated development timeline of the Snow Day API. This document is being retained for historical preservation purposes, and is not up-to-date.

------

**Last updated: 5/26/2019 | For API version: v1.4.0**

**This development timeline will be updated soon to reflect recent changes.**

The Snow Day API has a lot of development ideas and a big future ahead. This wiki page outlines the timeline of versions upcoming soon, and the primary focuses of each version.

## API v1.2.0, Dash N/A, Database v1.0.0
Version 1.2.0 is a general fix, improving support for API keys, along with allowing users to check how many delivery errors they've accumulated.

Released on April 10, 2019.

## API v1.2.1, Dash N/A, Database v1.0.0
Version 1.2.1 builds on the foundation of v1.2.0. v1.2.1 will remove the option to optionally send back the API version & response status (now ALWAYS required), switches all methods to POST-only, along with better coded responses.

## API v1.3.0, Dash N/A, Database v1.1.0
Version 1.3.0 of the API will add methods for group management. In the program configuration, you'll have the option to configure an array of user groups, and select a default group. For a new user signup, the user will be added to the default group. 

Two new API methods will be added:

`/api/updateUserGroup`, which updates the group that a specified number is in. This method takes three parameters - `key`, `number` (no dashes & fully formatted +1 number), and the `group` - This can either be the name of the group, or the index of the group name in the global array.

The response will contain not only the usual parameters, but a response confirming that the user was put into a certain group index or name. An error will also be returned if the specified group was not found, indicating that the operation was unsuccessful.

`/api/listUserGroup`, which will return back a list of user group names in an array.

One API method will be updated to contain additional parameters

`/api/newMessage` will now have the option to send to specific groups, using the `groups` form parameter. The list of groups is separated by commas, no spaces (ex: `group1,group2,group3`). If no groups parameter is specified, the message will be sent to all groups.

### API v2.0.0, Dash 1.0.0, Database v1.2.0
Version 2.0.0 of the API will add support for the Snow Day Dash, a brand new platform for managing user preferences.

Because of the amount of functionality coming to the API, many methods will be added.

`/api/requestDashLogon`, which will request for a temporary code to be sent out to a subscribed user from the dash for a logon. This method is POST only, and will only return if the code was sent out or not - it will NOT send back the actual logon code. It will however return a session ID cookie that is computer unique that is meant to be stored as an additional security layer to prevent colliding, with a by-default 30 minute expiry time.

`/api/authenticateDashLogon`, which will authenticate if a dash user is allowed to be authenticated with the temporary code. This method is POST only, but needs parameters for number, session ID, and the actual code the user entered.

Errors will be returned if logon failed, session expired, etc etc.

`/api/getUserSettings`, which will get back user settings from the API for the dash. Form parameters will always include session ID, number, and what settings should be returned, with the `settings` form param. If this parameter is not included, all settings will be sent back. Every time this request is made, the session duration is reset to 30 minutes, if it hasn't expired.

Additionally, errors will be sent back if a database error occurred, or if the session timed out.

`/api/putUserSettings`, which will update user settings from the dash to the API. Form parameters will always include which settings to update, session ID, and number. Errors will be sent back if a database error occurred, or the session timed out. Every time this request is made, the session duration is reset to 30 minutes, if it hasn't expired.

`/api/authenticateDashLogout`, which will authenticate a dash logout. This means that regardless of session expiry, the session entry will be deleted from the database.

Other API methods will also be updated:

`/api/registerNumber` will be updated to return a Unix timestamp of an expiry date for the number being confirmed for when it can respond.

`/api/inboundSMS` will be updated so that if a user responds to a confirmation code Y, and if their number has been either pruned from the pendingNumbers DB or if their entry was pruned, that a response is sent back requiring reconfirmation. N will also be updated to say that while the user wasn't subscribed, the session timed out.


v2.0.0 will also introduce new Python-based cron scripts to help manage databases. They are intended to be run each night at 3:30 AM local time.

`prune-sessions.py` is intended to prune user sessions that have an expiry time of 3:29:55 AM or earlier. It isn't intended to prune sessions that are actively happening at 3:30 in the morning.

`prune-pendingnumbers.py` is intended to prune pending numbers with an expiry date that is older than 2 days from when the script is run.

## v2.1.0 API, v1.1.0 Dash, v1.2.0 Database
v2.1.0 of the API is intended to make template-based messages a thing, making it easier to send multi-language messages.