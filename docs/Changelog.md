This is an old version of the changelog, which details changes up to v1.4.0 and "future" plans up to v1.7.0. This document is not current, and is being retained for historical preservation purposes.

------

**Last updated: 5/26/2019 | For API version: v1.4.0**

This page contains past & upcoming changes to the API, along with maintenance windows.

## Version 1.7.0 - Releasing in November 2019
* Adds new management methods: /api/PendingNumber (POST for adding and DELETE for removing)
* Adds new method: /api/MDCdisabledstate (GET to get it)
* Adds full support for config file with API keys and all that jazz (plus Docs)
* Adds full support for form data missing errors
* Adds ability for segmented API key permissions
* Improves responses for inbound SMS
* Adds more specific error codes for invalid data types in /api/updateUserSettings
* Adds specific error code for no data requested to update in /api/updateUserSettings
* Support Snow Day Dash v1.2.0 with announcements

## Version 1.6.2 - Releasing in September 2019
* Remove ability to close API

## Version 1.6.1 - Releasing in September 2019
* Do a lot of project cleanup and make sure docs are all up-to-date
* Add code comments

## Version 1.6.0  - Releasing in September 2019
* Adds too many methods
* Fixes bugs

## Version 1.5.0 - Releasing on May 31, 2019
* Adds management method: /api/unsubscribeNumber

## Version 1.4.0 - Released on May 26, 2019
* Adds the ability to add a number to a group
* Adds new methods for group management
* Adds new methods for disabling message confirmation deliveries when needed
* Improves homepage templates
* Adds support for partial config file functionality, mainly to store on-the-fly variables
* Adds script to generate API keys
* Add HTTP response codes across the API
* Improve documentation for new HTTP status codes, new methods, and improved CURL examples
* Copy documentation into local files
* CORS now mandated across the API, with `*` as default CORS url

## Version 1.3.0 - Released in May 2019
* Standardizes inboundSMS to only return TwiML
* API version info & status code now encoded as an XML comment for inboundSMS responses
* Adds the ability to stop registrations from a high amount of numbers

## Version 1.2.2 - Released in April 2019
* Switches all API methods to POST-only
* Eradicates all "Success" post-backs to status coded messages (along with temporary 204s)
* Eradicates all try blocks from TwiML responses in inboundSMS
* Remove option to not send back API version and status message
* Improved error code documentation
* If a timeout error occurs for a confirmation message SID, we will now delete the SID from the database to prevent collisions in the future
* Adds options to disable CORS entirely
* Commands command will have newline support

## Version 1.2.1 was basically skipped. No idea why, it just happened.

## Version 1.2.0 - Released April 10, 2019
* Adds a global CORS variable to prevent redefines
* Adds a global callback URL variable to prevent redefines
* Adds additional layers of verification across the API system to ensure security
* Adds multiple hardcoded keys for different endpoints
* Adds a command to allow users to see the amount of errors for their number
* Improved welcome message
* Adds an API shutoff command which will return all requests with a code 54
* Improved command message with new features
* Improved support for STOP/START commands - The API will now unsubscribe you with a STOP/UNSUBSCRIBE command (and not send anything back to avoid a Twilio error)

Important notices:
* Sometimes the API will return back absolutely nothing with a status code of 204. This is a temporary solution and will be permanently fixed in v1.2.1.

## Version 1.1.0 - Released February 15, 2019
* Adds additional error code detection for new number registration
* Adds code to detect if new broadcast messages are being undelivered, and if a message can't be delivered 3x the number is automatically unsubscribed
* Adds additional commands, like SUPPORT and ABOUT.
* Adds a command to list commands, COMMANDS.
* Adds a generic response message, saying that the system is a bot and can't accept replies.

## Version 1.0.0
* Initial version